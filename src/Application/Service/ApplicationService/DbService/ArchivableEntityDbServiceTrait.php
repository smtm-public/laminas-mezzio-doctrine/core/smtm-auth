<?php

declare(strict_types=1);

namespace Smtm\Auth\Application\Service\ApplicationService\DbService;

use Smtm\Auth\Authentication\Application\Service\AuthenticationService;
use Smtm\Auth\Domain\ArchivedByAwareEntityInterface;
use Smtm\Auth\Domain\UnarchivedByAwareEntityInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait as BaseArchivableEntityDbService2Trait;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Doctrine\ORM\QueryBuilder;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivableEntityDbServiceTrait
{

    use BaseArchivableEntityDbService2Trait {
        archiveFunc as baseArchiveFunc;
        unarchiveFunc as baseUnarchiveFunc;
    }

    public function archiveFunc(
        NotArchivedAwareEntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        if ($this instanceof ArchivedByAuthUserAwareEntityDbServiceInterface) {
            /** @var AuthenticationService $rootAuthenticationService */
            $rootAuthenticationService = $this->applicationServicePluginManager->get(
                AuthenticationService::class
            );
            $authenticatedUser = $rootAuthenticationService->getAuthenticatedUser();
            //$authenticatedUser = $this->attach($authenticatedUser);
            $authenticatedUser = $this->getRepository()->getEntityManager()->find(
                get_class($authenticatedUser),
                $authenticatedUser->getId()
            );
            /** @var ArchivedByAwareEntityInterface $entity */
            $entity->setArchivedBy($authenticatedUser);
        }

        $this->baseArchiveFunc($entity, $options);
    }

    public function unarchiveFunc(
        NotArchivedAwareEntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): NotArchivedAwareEntityInterface {
        if ($this instanceof UnarchivedByAuthUserAwareEntityDbServiceInterface) {
            /** @var AuthenticationService $rootAuthenticationService */
            $rootAuthenticationService = $this->applicationServicePluginManager->get(
                AuthenticationService::class
            );
            $authenticatedUser = $rootAuthenticationService->getAuthenticatedUser();
            $authenticatedUser = $this->getRepository()->getEntityManager()->find(
                get_class($authenticatedUser),
                $authenticatedUser->getId()
            );
            /** @var UnarchivedByAwareEntityInterface $entity */
            $entity->setUnarchivedBy($authenticatedUser);
        }

        return $this->baseUnarchiveFunc($entity, $options);
    }

    protected function prepareBulkArchiveById(
        array $id,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): QueryBuilder {
        $qb = $this->basePrepareBulkArchiveById($id, $options);

        if ($this instanceof ArchivedByAuthUserAwareEntityDbServiceInterface) {
            /** @var AuthenticationService $rootAuthenticationService */
            $rootAuthenticationService = $this->applicationServicePluginManager->get(
                AuthenticationService::class
            );
            $authenticatedUser = $rootAuthenticationService->getAuthenticatedUser();
            /*
            $authenticatedUser = $this->getRepository()->getEntityManager()->find(
                get_class($authenticatedUser),
                $authenticatedUser->getId()
            );
            */
            $qb->set('o.archivedBy', ':archivedBy');
            $qb->setParameter('archivedBy', $authenticatedUser->getId());
        }

        return $qb;
    }
}
