<?php

declare(strict_types=1);

namespace Smtm\Auth\Application\Service\ApplicationService\DbService;

use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AbstractUuidAwareEntityDbService extends AbstractDbService
    implements UuidAwareEntityDbServiceInterface
{
    use UuidAwareEntityDbServiceTrait;
}
