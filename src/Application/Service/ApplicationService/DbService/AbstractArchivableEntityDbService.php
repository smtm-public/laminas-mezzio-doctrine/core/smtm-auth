<?php

declare(strict_types=1);

namespace Smtm\Auth\Application\Service\ApplicationService;

use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AbstractArchivableEntityDbService extends AbstractDbService
    implements ArchivableEntityDbServiceInterface
{
    use ArchivableEntityDbServiceTrait;
}
