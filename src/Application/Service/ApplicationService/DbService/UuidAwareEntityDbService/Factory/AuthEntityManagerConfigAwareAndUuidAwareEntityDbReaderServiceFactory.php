<?php

declare(strict_types=1);

namespace Smtm\Auth\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\Factory;

use Smtm\Auth\Context\Permission\Application\Service\PermissionReaderServiceInterface;
use Smtm\Auth\Context\RoleCodePermission\Application\Service\RoleCodePermissionReaderServiceInterface;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthEntityManagerConfigAwareAndUuidAwareEntityDbReaderServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config')['auth'];

        if (
            in_array(
                $requestedName,
                [
                    PermissionReaderServiceInterface::class,
                    RoleCodePermissionReaderServiceInterface::class,
                ],
                true
            )
        ) {
            $config['readerEntityManagerName'] = $config['readerRolePermissionEntityManagerName'];
        }

        return new $requestedName(
            applicationServicePluginManager: $container->get(ApplicationServicePluginManager::class),
            infrastructureServicePluginManager: $container->get(InfrastructureServicePluginManager::class),
            hydratorPluginManager: $container->get(HydratorPluginManager::class),
            entityManagerRegistry: $container
                ->get(InfrastructureServicePluginManager::class)
                ->get(ManagerRegistryInterface::class),
            config: $config
        );
    }
}
