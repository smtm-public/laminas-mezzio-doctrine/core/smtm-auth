<?php

declare(strict_types=1);

namespace Smtm\Auth\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\Factory;

use Smtm\Auth\Context\Permission\Application\Service\PermissionServiceInterface;
use Smtm\Auth\Context\RoleCodePermission\Application\Service\RoleCodePermissionServiceInterface;
use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthEntityManagerConfigAwareAndUuidAwareEntityDbServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config')['auth'];

        if (
            in_array(
                $requestedName,
                [
                    PermissionServiceInterface::class,
                    RoleCodePermissionServiceInterface::class,
                ],
                true
            )
        ) {
            $config['entityManagerName'] = $config['rolePermissionEntityManagerName'];
        }

        return new $requestedName(
            applicationServicePluginManager: $container->get(ApplicationServicePluginManager::class),
            infrastructureServicePluginManager: $container->get(InfrastructureServicePluginManager::class),
            hydratorPluginManager: $container->get(HydratorPluginManager::class),
            extractorPluginManager: $container->get(ExtractorPluginManager::class),
            baseHydratorPluginManager: $container->get(\Laminas\Hydrator\HydratorPluginManager::class),
            entityManagerRegistry: $container
                ->get(InfrastructureServicePluginManager::class)
                ->get(ManagerRegistryInterface::class),
            config: $config
        );
    }
}
