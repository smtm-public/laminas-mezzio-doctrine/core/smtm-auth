<?php

declare(strict_types=1);

namespace Smtm\Auth\Application\Service\ApplicationService;

use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\ApplicationService\DbService\EntityManagerConfigAwareDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AbstractEntityManagerConfigAwareDbService extends AbstractDbService
    implements EntityManagerConfigAwareDbServiceInterface
{
    #[Pure] public function __construct(
        ApplicationServicePluginManager $applicationServicePluginManager,
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        HydratorPluginManager $hydratorPluginManager,
        ExtractorPluginManager $extractorPluginManager,
        \Laminas\Hydrator\HydratorPluginManager $baseHydratorPluginManager,
        ManagerRegistryInterface $entityManagerRegistry,
        protected array $config
    ) {
        parent::__construct(
            $applicationServicePluginManager,
            $infrastructureServicePluginManager,
            $hydratorPluginManager,
            $extractorPluginManager,
            $baseHydratorPluginManager,
            $entityManagerRegistry
        );

        $this->entityManagerName = $config['entityManagerName'];
    }
}
