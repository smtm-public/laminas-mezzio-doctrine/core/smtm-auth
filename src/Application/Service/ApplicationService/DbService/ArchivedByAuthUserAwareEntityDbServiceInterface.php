<?php

declare(strict_types=1);

namespace Smtm\Auth\Application\Service\ApplicationService\DbService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ArchivedByAuthUserAwareEntityDbServiceInterface
{

}
