<?php

declare(strict_types=1);

namespace Smtm\Auth\Application\Service\ApplicationService;

use Smtm\Auth\Application\Service\ApplicationService\DbService\CreatedByAuthUserAwareEntityDbServiceInterface;
use Smtm\Auth\Application\Service\ApplicationService\DbService\ModifiedByAuthUserAwareEntityDbServiceInterface;
use Smtm\Auth\Authentication\Application\Service\AuthenticationService;
use Smtm\Auth\Context\User\Application\Service\UserService;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Domain\DomainObjectInterface;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Domain\IdAwareEntityInterface;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * @template-covariant T
 * TODO: MD - Add generic at extends \Smtm\Base\Application\Service\ApplicationService\AbstractDbService<DomainObjectInterface>
 */
class AbstractDbService extends \Smtm\Base\Application\Service\ApplicationService\AbstractDbService
{
    public function createFunc(
        array $data,
        ?EntityInterface $entity = null,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        if ($this instanceof CreatedByAuthUserAwareEntityDbServiceInterface) {
            if (empty($data['createdBy'])) {
                /** @var AuthenticationService $rootAuthenticationService */
                $rootAuthenticationService = $this->applicationServicePluginManager->get(
                    AuthenticationService::class
                );
                $authenticatedUser = $rootAuthenticationService->getAuthenticatedUser();
                $authenticatedUser = $this->getRepository()->getEntityManager()->find(
                    get_class($authenticatedUser),
                    $authenticatedUser->getId()
                );
                $data['createdBy'] = $authenticatedUser;
            }
        }

        return parent::createFunc($data, $entity, $options);
    }

    public function updateFunc(
        EntityInterface $entity,
        array $data,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        if ($this instanceof ModifiedByAuthUserAwareEntityDbServiceInterface) {
            if (empty($data['modifiedBy'])) {
                /** @var AuthenticationService $rootAuthenticationService */
                $rootAuthenticationService = $this->applicationServicePluginManager->get(
                    AuthenticationService::class
                );
                $authenticatedUser = $rootAuthenticationService->getAuthenticatedUser();
                $authenticatedUser = $this->getRepository()->getEntityManager()->find(
                    get_class($authenticatedUser),
                    $authenticatedUser->getId()
                );
                $data['modifiedBy'] = $authenticatedUser;
            }
        }

        return parent::updateFunc($entity, $data, $options);
    }

    public function saveFunc(
        ?EntityInterface $entity = null,
        array $data = [],
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        if ($this instanceof CreatedByAuthUserAwareEntityDbServiceInterface
            && (
                $entity === null
                || ($entity instanceof IdAwareEntityInterface && !ReflectionHelper::isPropertyInitialized('id', $entity))
                || !ReflectionHelper::isPropertyInitialized('createdBy', $entity)
            )
        ) {
            if (isset($data['createdBy'])) {
                /** @var UserService $userService */
                $userService = $this->applicationServicePluginManager->get(UserService::class);

                if (is_array($data['createdBy'])) {
                    $data['createdBy'] = $userService->hydrateDomainObject($data['createdBy']);
                } elseif (is_int($data['createdBy'])) {
                    $data['createdBy'] = $userService->getById($data['createdBy']);
                } elseif (is_string($data['createdBy'])) {
                    $data['createdBy'] = $userService->getOneByUuid($data['createdBy']);
                }
            } else {
                $data['createdBy'] = $this->getAuthenticatedUser();
            }
        } elseif ($this instanceof ModifiedByAuthUserAwareEntityDbServiceInterface) {
            if (isset($data['modifiedBy'])) {
                /** @var UserService $userService */
                $userService = $this->applicationServicePluginManager->get(UserService::class);

                if (is_array($data['modifiedBy'])) {
                    $data['modifiedBy'] = $userService->hydrateDomainObject($data['modifiedBy']);
                } elseif (is_int($data['modifiedBy'])) {
                    $data['modifiedBy'] = $userService->getById($data['modifiedBy']);
                } elseif (is_string($data['modifiedBy'])) {
                    $data['modifiedBy'] = $userService->getOneByUuid($data['modifiedBy']);
                }
            } else {
                $data['modifiedBy'] = $this->getAuthenticatedUser();
            }
        }

        return parent::saveFunc($entity, $data, $options);
    }

    protected function getAuthenticatedUser(): UserInterface
    {
        /** @var AuthenticationService $rootAuthenticationService */
        $rootAuthenticationService = $this->applicationServicePluginManager->get(
            AuthenticationService::class
        );
        $authenticatedUser = $rootAuthenticationService->getAuthenticatedUser();
        $authenticatedUser = $this->getRepository()->getEntityManager()->find(
            get_class($authenticatedUser),
            $authenticatedUser->getId()
        );

        return $authenticatedUser;
    }
}
