<?php

declare(strict_types=1);

namespace Smtm\Auth\Command;

use Smtm\Auth\Authentication\Application\Service\AuthenticationService;
use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManagerAwareInterface;
use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManagerAwareTrait;
use Smtm\Auth\Authentication\Application\Service\Cli\CliAuthenticationServiceInterface;
use Smtm\Base\Infrastructure\Doctrine\Orm\Mapping\EntityListenerResolver;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Command extends \Smtm\Base\Command\Command implements AuthenticationServicePluginManagerAwareInterface
{

    use AuthenticationServicePluginManagerAwareTrait;

    protected int $userClientId = 1;

    public function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        /** @var AuthenticationService $rootAuthenticationService */
        $rootAuthenticationService = $this->applicationServicePluginManager->get(AuthenticationService::class);
        /** @var CliAuthenticationServiceInterface $cliAuthenticationService */
        $cliAuthenticationService = $this->authenticationServicePluginManager->get(
            CliAuthenticationServiceInterface::class
        );
        $cliAuthenticationService->authenticateUserClient($this->userClientId);
        $rootAuthenticationService->setAuthenticationService($cliAuthenticationService);

        /** @var EntityListenerResolver $entityListenerResolver */
        $entityListenerResolver = $this->infrastructureServicePluginManager->get(EntityListenerResolver::class);
        $entityListenerResolver->setAuthenticatedUserId(
            $rootAuthenticationService->getAuthenticationService()->getAuthenticatedUser()->getId()
        );
    }
}
