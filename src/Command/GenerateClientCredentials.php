<?php

declare(strict_types=1);

namespace Smtm\Auth\Command;

use Smtm\Base\Infrastructure\Service\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Service\Log\LoggerAwareTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Example:
 * vendor/bin/symfony-console auth:generate-client-credentials -vvv
 * vendor/bin/symfony-console auth:generate-client-credentials -vvv --client-id-length=16 --client-secret-length=64
 */
class GenerateClientCredentials extends Command implements LoggerAwareInterface
{

    use LoggerAwareTrait;

    public const DEFAULT_CLIENT_ID_LENGTH = 16;
    public const DEFAULT_CLIENT_SECRET_LENGTH = 64;

    protected function configure()
    {
        $this
            ->setName('auth:generate-client-credentials')
            ->setDescription(
                'Smtm - Auth - Generate Client Credentials'
            )
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')

            ->addOption(
                'client-id-length',
                'i',
                InputOption::VALUE_REQUIRED,
                'The number of characters of the Client ID' . "\n"
            )
            ->addOption(
                'client-secret-length',
                's',
                InputOption::VALUE_REQUIRED,
                'The number of characters of the Client Secret.' . "\n"
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $optionClientIdLength =
            $input->getOption('client-id-length')
                ? (int) $input->getOption('client-id-length')
                : static::DEFAULT_CLIENT_ID_LENGTH;
        $optionClientSecretLength =
            $input->getOption('client-secret-length')
                ? (int) $input->getOption('client-secret-length')
                : static::DEFAULT_CLIENT_SECRET_LENGTH;

        $clientId = bin2hex(random_bytes($optionClientIdLength));
        $clientSecret = bin2hex(random_bytes($optionClientSecretLength));
        $clientSecretHash = password_hash($clientSecret, PASSWORD_DEFAULT);
        $this->logger->info('Client ID: ' . $clientId);
        $this->logger->info('Client Secret: ' . $clientSecret);
        $this->logger->info('Client Secret Hash: ' . $clientSecretHash);

        return Command::SUCCESS;
    }
}
