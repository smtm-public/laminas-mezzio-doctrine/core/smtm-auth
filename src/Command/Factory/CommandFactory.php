<?php

declare(strict_types=1);

namespace Smtm\Auth\Command\Factory;

use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManager;
use Smtm\Auth\Command\Command;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CommandFactory extends \Smtm\Base\Command\Factory\CommandFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var Command $command */
        $command = parent::__invoke($container, $requestedName, $options);
        $command->setAuthenticationServicePluginManager($container->get(AuthenticationServicePluginManager::class));

        return $command;
    }
}
