<?php

declare(strict_types=1);

namespace Smtm\Auth\Command;

use Smtm\Base\Infrastructure\Helper\OAuth2Helper;
use Smtm\Base\Infrastructure\Service\Log\LoggerAwareInterface;
use Smtm\Base\Infrastructure\Service\Log\LoggerAwareTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Example:
 * vendor/bin/symfony-console auth:generate-pkce -vvv
 * vendor/bin/symfony-console auth:generate-pkce -vvv --code-verifier-length=128
 */
class GeneratePkce extends Command implements LoggerAwareInterface
{

    use LoggerAwareTrait;

    public const DEFAULT_CODE_VERIFIER_LENGTH = 128;

    protected function configure()
    {
        $this
            ->setName('auth:generate-pkce')
            ->setDescription(
                'Smtm - Auth - Generate PKCE'
            )
            // the full command description shown when running the command with
            // the "--help" option
            //->setHelp('Could write some useful text here')

            ->addOption(
                'code-verifier-length',
                'l',
                InputOption::VALUE_REQUIRED,
                'The number of characters of the Code Verifier.' . "\n"
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $optionCodeVerifierLength =
            $input->getOption('code-verifier-length')
                ? (int) $input->getOption('code-verifier-length')
                : static::DEFAULT_CODE_VERIFIER_LENGTH;

        $codeVerifier = OAuth2Helper::generateCodeVerifier($optionCodeVerifierLength);
        $codeChallenge = OAuth2Helper::createS256CodeChallenge($codeVerifier);
        $codeChallengeUrlSafe = OAuth2Helper::createS256CodeChallenge($codeVerifier, true);
        $this->logger->info('Code Verifier: ' . $codeVerifier);
        $this->logger->info('Code Challenge: ' . $codeChallenge);
        $this->logger->info('Code Challenge (URL-safe): ' . $codeChallengeUrlSafe);

        return Command::SUCCESS;
    }
}
