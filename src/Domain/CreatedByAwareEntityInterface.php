<?php

declare(strict_types=1);

namespace Smtm\Auth\Domain;

use Smtm\Auth\Context\User\Domain\UserInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface CreatedByAwareEntityInterface
{
    public function getCreatedBy(): UserInterface;
    public function setCreatedBy(UserInterface $createdBy): static;
}
