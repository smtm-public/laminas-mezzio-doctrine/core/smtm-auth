<?php

declare(strict_types=1);

namespace Smtm\Auth\Domain;

use Smtm\Auth\Context\User\Domain\UserInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UnarchivedByAwareEntityInterface
{
    public function getUnarchivedBy(): ?UserInterface;
    public function setUnarchivedBy(UserInterface $unarchivedBy): static;
}
