<?php

declare(strict_types=1);

namespace Smtm\Auth\Domain;

use Smtm\Auth\Context\User\Domain\UserInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait UnarchivedByAwareEntityTrait
{
    protected ?UserInterface $unarchivedBy = null;

    public function getUnarchivedBy(): ?UserInterface
    {
        return $this->unarchivedBy;
    }

    public function setUnarchivedBy(UserInterface $unarchivedBy): static
    {
        $this->unarchivedBy = $unarchivedBy;

        return $this;
    }
}
