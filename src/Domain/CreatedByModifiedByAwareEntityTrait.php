<?php

declare(strict_types=1);

namespace Smtm\Auth\Domain;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait CreatedByModifiedByAwareEntityTrait
{
    use CreatedByAwareEntityTrait, ModifiedByAwareEntityTrait;
}
