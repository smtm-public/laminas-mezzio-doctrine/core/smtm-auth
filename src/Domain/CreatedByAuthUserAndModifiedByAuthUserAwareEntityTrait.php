<?php

declare(strict_types=1);

namespace Smtm\Auth\Domain;

use Smtm\Auth\Context\User\Domain\UserInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait CreatedByAuthUserAndModifiedByAuthUserAwareEntityTrait
{
    protected UserInterface $createdBy;
    protected ?UserInterface $modifiedBy = null;

    public function getCreatedBy(): UserInterface
    {
        return $this->createdBy;
    }

    public function setCreatedBy(UserInterface $createdBy): static
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?UserInterface
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?UserInterface $modifiedBy): static
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }
}
