<?php

declare(strict_types=1);

namespace Smtm\Auth\Domain;

use Smtm\Auth\Context\User\Domain\UserInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ArchivedByAwareEntityInterface
{
    public function getArchivedBy(): ?UserInterface;
    public function setArchivedBy(UserInterface $archivedBy): static;
}
