<?php

declare(strict_types=1);

namespace Smtm\Auth\Domain;

use Smtm\Auth\Context\User\Domain\UserInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ModifiedByAwareEntityInterface
{
    public function getModifiedBy(): ?UserInterface;
    public function setModifiedBy(UserInterface $modifiedBy): static;
}
