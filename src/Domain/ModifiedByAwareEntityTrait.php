<?php

declare(strict_types=1);

namespace Smtm\Auth\Domain;

use Smtm\Auth\Context\User\Domain\UserInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ModifiedByAwareEntityTrait
{
    protected ?UserInterface $modifiedBy = null;

    public function getModifiedBy(): ?UserInterface
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(UserInterface $modifiedBy): static
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }
}
