<?php

declare(strict_types=1);

namespace Smtm\Auth\Domain;

use Smtm\Auth\Context\User\Domain\UserInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait ArchivedByAwareEntityTrait
{
    protected ?UserInterface $archivedBy = null;

    public function getArchivedBy(): ?UserInterface
    {
        return $this->archivedBy;
    }

    public function setArchivedBy(UserInterface $archivedBy): static
    {
        $this->archivedBy = $archivedBy;

        return $this;
    }
}
