<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Http\Middleware;

use Smtm\Auth\Authentication\Application\Service\AuthenticationService;
use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManager;
use Smtm\Auth\Authentication\Application\Service\Basic\BasicAuthenticationServiceInterface;
use Smtm\Auth\Authentication\Application\Service\Basic\Exception\InvalidPasswordException;
use Smtm\Auth\Authentication\Application\Service\Bearer\BearerAuthenticationServiceInterface;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\ExpiredTokenException;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\InvalidIdTokenException;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\InvalidJwtSignatureException;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\InvalidUserAuthTypeException;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\TokenNotFoundException;
use Smtm\Auth\Authentication\Application\Service\Cli\CliSystemUserAuthenticationService;
use Smtm\Auth\Context\User\Exception\UserNotFoundException;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Http\Exception\UnauthorizedException;
use Smtm\Base\Http\Middleware\Exception\InvalidArgumentException;
use Smtm\Base\Infrastructure\Doctrine\Orm\Mapping\EntityListenerResolver;
use Smtm\Base\Infrastructure\Exception\InvalidArgumentException as InfrastructureInvalidArgumentException;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Mezzio\Router\RouteResult;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthenticationMiddleware implements MiddlewareInterface, ConfigAwareInterface
{

    use ConfigAwareTrait;

    public const REQUEST_ATTRIBUTE_NAME_AUTHENTICATION_SERVICE = 'authenticationService';
    public const REQUEST_ATTRIBUTE_NAME_AUTHENTICATED_USER = 'authenticatedUser';

    public function __construct(
        protected ApplicationServicePluginManager $applicationServicePluginManager,
        protected AuthenticationServicePluginManager $authenticationServicePluginManager,
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager
    ) {
    }

    /**
     * @inheritdoc
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->config['enableAuthenticationAndAuthorization'] && $this->config['enableAuthentication']) {
            /** @var AuthenticationService $rootAuthenticationService */
            $rootAuthenticationService = $this->applicationServicePluginManager->get(
                AuthenticationService::class
            );

            if (PHP_SAPI === 'cli' && (($request->getServerParams()['SCRIPT_NAME'] ?? '') !== 'Codeception')) {
                /** @var CliSystemUserAuthenticationService $authenticationService */
                $authenticationService = $this->authenticationServicePluginManager->get(
                    CliSystemUserAuthenticationService::class
                );
                $rootAuthenticationService->setAuthenticationService($authenticationService);
                $authenticationService->authenticateUserClient();
            } else {
                /** @var RouteResult $routeResult */
                $routeResult = $request->getAttribute(RouteResult::class);

                if (!$routeResult || !$routeResult->isSuccess()) {
                    return $handler->handle($request);
                }

                $matchedRoute = $routeResult->getMatchedRoute();
                $routeOptions = $matchedRoute->getOptions();

                if (!array_key_exists('authentication', $routeOptions)) {
                    throw new InvalidArgumentException('Missing \'authentication\' route option.');
                }

                if (!isset($routeOptions['authentication']) || !isset($routeOptions['authentication']['services'])) {
                    return $handler->handle($request);
                }

                $authorizationHeaderValues = $request->getHeader('authorization');

                try {
                    $httpAuthentication = HttpHelper::parseAuthenticationHeaderValues($authorizationHeaderValues);
                } catch (InfrastructureInvalidArgumentException $e) {
                    throw new UnauthorizedException($e->getMessage());
                }

                if ($httpAuthentication === null && ($routeOptions['authentication']['options']['required'] ?? true) === true) {
                    throw new UnauthorizedException('Missing \'authorization\' header.');
                }

                switch ($httpAuthentication['type']) {
                    case HttpHelper::AUTHENTICATION_BASIC:
                        if (!array_key_exists(
                            HttpHelper::AUTHENTICATION_BASIC,
                            $routeOptions['authentication']['services']
                        )) {
                            throw new UnauthorizedException('Basic authentication is not allowed for this route.');
                        }

                        $authenticationService = $this->authenticationServicePluginManager->get(
                            $routeOptions['authentication']['services'][HttpHelper::AUTHENTICATION_BASIC]['name'],
                            $routeOptions['authentication']['services'][HttpHelper::AUTHENTICATION_BASIC]['options'] ?? null
                        );
                        $rootAuthenticationService->setAuthenticationService($authenticationService);
                        $request = $request->withAttribute(
                            self::REQUEST_ATTRIBUTE_NAME_AUTHENTICATION_SERVICE,
                            $authenticationService
                        );

                        if (!$authenticationService instanceof BasicAuthenticationServiceInterface) {
                            throw new InvalidArgumentException(
                                'Expected service of type \'' . BasicAuthenticationServiceInterface::class
                                . '\' got \'' . get_class($authenticationService) . '\'.'
                            );
                        }

                        try {
                            $authenticationService->authenticateWithUsernameAndPassword(
                                $httpAuthentication['credentials']['username'],
                                $httpAuthentication['credentials']['password']
                            );
                        } catch (UserNotFoundException|InvalidPasswordException $e) {
                            throw new UnauthorizedException('', 0, $e);
                        }

                        break;
                    case HttpHelper::AUTHENTICATION_TOKEN:
                        if (!array_key_exists(
                            HttpHelper::AUTHENTICATION_TOKEN,
                            $routeOptions['authentication']['services']
                        )) {
                            throw new UnauthorizedException(
                                'Token authentication is not allowed for this route.'
                            );
                        }

                        $authenticationService = $this->authenticationServicePluginManager->get(
                            $routeOptions['authentication']['services'][HttpHelper::AUTHENTICATION_TOKEN]['name'],
                            $routeOptions['authentication']['services'][HttpHelper::AUTHENTICATION_TOKEN]['options'] ?? null
                        );
                        $rootAuthenticationService->setAuthenticationService($authenticationService);

                        if (!$authenticationService instanceof BearerAuthenticationServiceInterface) {
                            throw new InvalidArgumentException(
                                'Expected service of type \'' . BearerAuthenticationServiceInterface::class
                                . '\' got \'' . get_class($authenticationService) . '\'.'
                            );
                        }

                        try {
                            $authenticationService->authenticateWithToken(
                                $httpAuthentication['credentials']['token']
                            );
                        } catch (ExpiredTokenException
                        |InvalidJwtSignatureException
                        |InvalidUserAuthTypeException
                        |TokenNotFoundException $e
                        ) {
                            throw new UnauthorizedException('', 0, $e);
                        }

                        break;
                    case HttpHelper::AUTHENTICATION_BEARER:
                        if (!array_key_exists(
                            HttpHelper::AUTHENTICATION_BEARER,
                            $routeOptions['authentication']['services']
                        )) {
                            throw new UnauthorizedException(
                                'Bearer token authentication is not allowed for this route.'
                            );
                        }

                        $authenticationService = $this->authenticationServicePluginManager->get(
                            $routeOptions['authentication']['services'][HttpHelper::AUTHENTICATION_BEARER]['name'],
                            $routeOptions['authentication']['services'][HttpHelper::AUTHENTICATION_BEARER]['options'] ?? null
                        );
                        $rootAuthenticationService->setAuthenticationService($authenticationService);

                        if (!$authenticationService instanceof BearerAuthenticationServiceInterface) {
                            throw new InvalidArgumentException(
                                'Expected service of type \'' . BearerAuthenticationServiceInterface::class
                                . '\' got \'' . get_class($authenticationService) . '\'.'
                            );
                        }

                        try {
                            $authenticationService->authenticateWithToken(
                                $httpAuthentication['credentials']['token']
                            );
                        } catch (ExpiredTokenException
                        |InvalidJwtSignatureException
                        |InvalidUserAuthTypeException
                        |TokenNotFoundException
                        |InvalidIdTokenException $e
                        ) {
                            throw new UnauthorizedException('', 0, $e);
                        }

                        break;
                }
            }

            $request = $request->withAttribute(
                static::REQUEST_ATTRIBUTE_NAME_AUTHENTICATION_SERVICE,
                $rootAuthenticationService->getAuthenticationService()
            );
            $request = $request->withAttribute(
                static::REQUEST_ATTRIBUTE_NAME_AUTHENTICATED_USER,
                $rootAuthenticationService->getAuthenticationService()->getAuthenticatedUser()
            );
            /** @var EntityListenerResolver $entityListenerResolver */
            $entityListenerResolver = $this->infrastructureServicePluginManager->get(EntityListenerResolver::class);
            $entityListenerResolver->setAuthenticatedUserId(
                $rootAuthenticationService->getAuthenticationService()->getAuthenticatedUser()->getId()
            );
        }

        return $handler->handle($request);
    }
}
