<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Factory;

use Smtm\Auth\Authentication\Application\Service\AuthenticationServiceInterface;
use Smtm\Base\Application\Extractor\ExtractorPluginManager;
use Smtm\Base\Application\Hydrator\HydratorPluginManager;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthenticationServiceAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(ApplicationServicePluginManager::class),
            $container->get(InfrastructureServicePluginManager::class),
            $container->get(HydratorPluginManager::class),
            $container->get(ExtractorPluginManager::class),
            $container->get(\Laminas\Hydrator\HydratorPluginManager::class),
            $container->get(AuthenticationServiceAbstractFactory::class)
        );
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AuthenticationServiceInterface::class);
    }
}
