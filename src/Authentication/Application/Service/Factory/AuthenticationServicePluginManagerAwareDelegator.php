<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Factory;

use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManager;
use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManagerAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

class AuthenticationServicePluginManagerAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var AuthenticationServicePluginManagerAwareInterface $object */
        $object = $callback();

        $object->setAuthenticationServicePluginManager($container->get(AuthenticationServicePluginManager::class));

        return $object;
    }
}
