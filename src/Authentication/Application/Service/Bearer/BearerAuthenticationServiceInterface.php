<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Bearer;

use Smtm\Auth\Authentication\Application\Service\AuthenticationServiceInterface;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\UserClient\Domain\UserClientInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface BearerAuthenticationServiceInterface extends AuthenticationServiceInterface
{
    public function authenticateWithToken(
        string $tokenData
    ): void;

    public function getAuthenticatedUserClient(): UserClientInterface;
    public function getAuthenticatedUser(): UserInterface;
    public function getRoleCodeCollection(): array;
}
