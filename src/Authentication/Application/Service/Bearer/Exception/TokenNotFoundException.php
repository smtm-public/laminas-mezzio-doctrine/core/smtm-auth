<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Bearer\Exception;

use Smtm\Base\Application\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenNotFoundException extends RuntimeException
{
    protected $message = 'Token not found';
}
