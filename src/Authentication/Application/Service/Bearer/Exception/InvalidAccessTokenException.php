<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Bearer\Exception;

use Smtm\Base\Application\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidAccessTokenException extends RuntimeException
{
    protected $message = 'Invalid access token';
}
