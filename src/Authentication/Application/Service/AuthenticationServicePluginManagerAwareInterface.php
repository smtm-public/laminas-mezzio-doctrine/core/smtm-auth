<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface AuthenticationServicePluginManagerAwareInterface
{
    public function getAuthenticationServicePluginManager(): AuthenticationServicePluginManager;
    public function setAuthenticationServicePluginManager(
        AuthenticationServicePluginManager $authenticationServicePluginManager
    ): static;
}
