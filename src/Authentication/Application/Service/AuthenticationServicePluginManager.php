<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service;

use Laminas\ServiceManager\AbstractPluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthenticationServicePluginManager extends AbstractPluginManager
{
    protected $instanceOf = AuthenticationServiceInterface::class;
}
