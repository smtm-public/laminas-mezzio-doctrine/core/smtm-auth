<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service;

use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Base\Application\Service\AbstractApplicationService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthenticationService extends AbstractApplicationService implements AuthenticationServiceInterface
{
    protected ?AuthenticationServiceInterface $authenticationService = null;

    public function getAuthenticatedUser(): UserInterface
    {
        return $this->authenticationService->getAuthenticatedUser();
    }

    public function getRoleCodeCollection(): ?array
    {
        if ($this->authenticationService === null) {
            return null;
        }

        return $this->authenticationService->getRoleCodeCollection();
    }

    public function getAuthenticationService(): ?AuthenticationServiceInterface
    {
        return $this->authenticationService;
    }

    public function setAuthenticationService(
        ?AuthenticationServiceInterface $authenticationService
    ): static {
        $this->authenticationService = $authenticationService;

        return $this;
    }
}
