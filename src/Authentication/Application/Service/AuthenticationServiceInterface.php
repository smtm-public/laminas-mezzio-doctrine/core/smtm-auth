<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service;

use Smtm\Auth\Context\User\Domain\UserInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface AuthenticationServiceInterface
{
    public function getAuthenticatedUser(): UserInterface;
    public function getRoleCodeCollection(): ?array;
}
