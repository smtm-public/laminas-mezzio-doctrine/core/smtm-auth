<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Token;

use Smtm\Auth\Authentication\Application\Service\AuthenticationServiceInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface TokenAuthenticationServiceInterface extends AuthenticationServiceInterface
{
    public function authenticateWithToken(
        string $token
    ): void;
}
