<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Token\Exception;

use Smtm\Base\Application\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExpiredTokenException extends RuntimeException
{
    protected $message = 'The token has expired';
}
