<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Token;

use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\InvalidJwtSignatureException;
use Smtm\Auth\Authentication\Application\Service\Bearer\Exception\TokenNotFoundException;
use Smtm\Auth\Authentication\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\Jwt\Application\Service\JwtService;
use Smtm\Auth\Context\Jwt\Application\Service\JwtServiceInterface;
use Smtm\Auth\Context\Jwt\Domain\Jwt;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Infrastructure\Service\JwtService as JwtInfrastructureService;
use JetBrains\PhpStorm\Pure;
use Lcobucci\JWT\Token;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenJwtAuthenticationService extends AbstractApplicationService implements
    TokenAuthenticationServiceInterface,
    ConfigAwareInterface
{

    use ConfigAwareTrait;

    protected ?Jwt $authenticatedJwt = null;
    protected ?Token $parsedJwt = null;

    public function authenticateWithToken(
        string $token
    ): void {
        /** JwtService $jwtService */
        $jwtService = $this->applicationServicePluginManager->get(JwtServiceInterface::class);

        try {
            /** @var Jwt $jwt */
            $jwt = $jwtService->getOneBy(['token.accessToken' => $token]);
        } catch (EntityNotFoundException $e) {
            throw new TokenNotFoundException('', 0, $e);
        }

        /** @var JwtInfrastructureService $jwtInfrastructureServiceAuthIssuer */
        $jwtInfrastructureServiceAuthIssuer = $this->infrastructureServicePluginManager->get(
            $this->config['authIssuerServiceName']
        );
        $parsedJwt = $jwtInfrastructureServiceAuthIssuer->parse($jwt->getIdToken());

        if (!$jwtInfrastructureServiceAuthIssuer->validate($parsedJwt)) {
            throw new InvalidJwtSignatureException();
        }

        $this->setAuthenticatedJwt($jwt);
        $this->setParsedJwt($parsedJwt);
    }

    #[Pure] public function getAuthenticatedUser(): UserInterface
    {
        return $this->authenticatedJwt->getUser();
    }

    public function getRoleCodeCollection(): ?array
    {
        return $this->authenticatedJwt->getUser()->getRoleCodeCollection();
    }

    public function getAuthenticatedJwt(): ?Jwt
    {
        return $this->authenticatedJwt;
    }

    public function setAuthenticatedJwt(?Jwt $authenticatedJwt): static
    {
        $this->authenticatedJwt = $authenticatedJwt;

        return $this;
    }

    public function getParsedJwt(): ?Token
    {
        return $this->parsedJwt;
    }

    public function setParsedJwt(?Token $parsedJwt): static
    {
        $this->parsedJwt = $parsedJwt;

        return $this;
    }
}
