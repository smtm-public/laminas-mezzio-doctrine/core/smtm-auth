<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait AuthenticationServicePluginManagerAwareTrait
{
    protected AuthenticationServicePluginManager $authenticationServicePluginManager;

    public function getAuthenticationServicePluginManager(): AuthenticationServicePluginManager
    {
        return $this->authenticationServicePluginManager;
    }

    public function setAuthenticationServicePluginManager(
        AuthenticationServicePluginManager $authenticationServicePluginManager
    ): static {
        $this->authenticationServicePluginManager = $authenticationServicePluginManager;

        return $this;
    }
}
