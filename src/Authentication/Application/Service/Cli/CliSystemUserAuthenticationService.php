<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Cli;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\User\Exception\UserNotFoundException;
use Smtm\Auth\Context\UserClient\Application\Service\UserClientService;
use Smtm\Auth\Context\UserClient\Domain\UserClientInterface;
use Smtm\Base\Application\Service\AbstractApplicationService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CliSystemUserAuthenticationService extends AbstractApplicationService implements CliAuthenticationServiceInterface
{
    protected UserClientInterface $authenticatedUserClient;

    public function authenticateUserClient(?int $id = 1): void
    {
        /** @var UserClientService $userClientService */
        $userClientService = $this->applicationServicePluginManager->get(UserClientService::class);

        /** @var UserClientInterface $userClient */
        if (($userClient = $userClientService->getById($id)) === null) {
            throw new UserNotFoundException();
        }

        $this->setAuthenticatedUserClient($userClient);
    }

    public function getRoleCodeCollection(): ?array
    {
        return $this->authenticatedUserClient->getRoleCodeCollection();
    }

    public function getRoleCodeCollectionForClient(ClientInterface $client): ?array
    {
        $userClientForClient = null;

        foreach ($this->authenticatedUserClient->getUser()->getUserClientCollection() as $userClient) {
            if ($userClient->getClient()->getClientId() === $client->getClientId()) {
                $userClientForClient = $userClient;

                break;
            }
        }

        return $userClientForClient->getRoleCodeCollection();
    }

    public function getAuthenticatedUser(): UserInterface
    {
        return $this->authenticatedUserClient->getUser();
    }

    public function getAuthenticatedUserClient(): UserClientInterface
    {
        return $this->authenticatedUserClient;
    }

    public function setAuthenticatedUserClient(UserClientInterface $authenticatedUserClient): static
    {
        $this->authenticatedUserClient = $authenticatedUserClient;

        return $this;
    }
}
