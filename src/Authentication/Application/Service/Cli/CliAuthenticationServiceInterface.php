<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Cli;

use Smtm\Auth\Authentication\Application\Service\AuthenticationServiceInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface CliAuthenticationServiceInterface extends AuthenticationServiceInterface
{
    public function authenticateUserClient(?int $id = 1): void;
}
