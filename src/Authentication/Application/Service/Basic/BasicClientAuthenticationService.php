<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Basic;

use Smtm\Auth\Authentication\Application\Service\Basic\Exception\InvalidPasswordException;
use Smtm\Auth\Context\Client\Application\Service\ClientServiceInterface;
use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\User\Application\Service\UserServiceInterface;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\User\Exception\UserNotFoundException;
use Smtm\Base\Application\Service\AbstractApplicationService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class BasicClientAuthenticationService extends AbstractApplicationService implements BasicAuthenticationServiceInterface
{
    protected ?ClientInterface $authenticatedClient = null;
    protected UserInterface $authenticatedUser;

    public function authenticateWithUsernameAndPassword(
        string $username,
        string $password
    ): void {
        /** @var ClientServiceInterface $clientService */
        $clientService = $this->applicationServicePluginManager->get(ClientServiceInterface::class);

        /** @var ClientInterface $client */
        if (($client = $clientService->getOneOrNullByClientId($username)) === null) {
            throw new UserNotFoundException();
        }

        if (!$client->verifyClientSecret($password)) {
            throw new InvalidPasswordException();
        }

        $this->setAuthenticatedClient($client);

        /** UserService $userService */
        $userService = $this->applicationServicePluginManager->get(UserServiceInterface::class);
        /** @var UserInterface $systemUser */
        $systemUser = $userService->getById(1);
        $this->setAuthenticatedUser($systemUser);
    }

    public function getAuthenticatedUser(): UserInterface
    {
        return $this->authenticatedUser;
    }

    public function setAuthenticatedUser(UserInterface $authenticatedUser): static
    {
        $this->authenticatedUser = $authenticatedUser;

        return $this;
    }

    public function getRoleCodeCollection(): ?array
    {
        return $this->authenticatedUser->getRoleCodeCollection();
    }

    public function getAuthenticatedClient(): ?ClientInterface
    {
        return $this->authenticatedClient;
    }

    public function setAuthenticatedClient(?ClientInterface $authenticatedClient): static
    {
        $this->authenticatedClient = $authenticatedClient;

        return $this;
    }
}
