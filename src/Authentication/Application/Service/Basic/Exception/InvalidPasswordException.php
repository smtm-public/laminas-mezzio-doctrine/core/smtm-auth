<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Basic\Exception;

use Smtm\Base\Application\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidPasswordException extends RuntimeException
{
    protected $message = 'Invalid password';
}
