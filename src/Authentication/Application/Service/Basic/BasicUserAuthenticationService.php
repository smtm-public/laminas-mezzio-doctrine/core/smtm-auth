<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Basic;

use Smtm\Auth\Authentication\Application\Service\Basic\Exception\InvalidPasswordException;
use Smtm\Auth\Authentication\Context\User\Application\Service\UserService;
use Smtm\Auth\Authentication\Context\User\Application\Service\UserServiceInterface;
use Smtm\Auth\Authentication\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\User\Exception\UserNotFoundException;
use Smtm\Base\Application\Service\AbstractApplicationService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class BasicUserAuthenticationService extends AbstractApplicationService implements BasicAuthenticationServiceInterface
{
    protected UserInterface $authenticatedUser;

    public function authenticateWithUsernameAndPassword(
        string $username,
        string $password
    ): void {
        /** UserService $userService */
        $userService = $this->applicationServicePluginManager->get(UserServiceInterface::class);

        /** @var UserInterface $user */
        if (($user = $userService->getOneOrNullByUsername($username)) === null) {
            throw new UserNotFoundException();
        }

        if (!$user->verifyPassword($password)) {
            throw new InvalidPasswordException();
        }

        $this->setAuthenticatedUser($user);
    }

    public function getRoleCodeCollection(): ?array
    {
        return $this->authenticatedUser->getRoleCodeCollection();
    }

    public function getAuthenticatedUser(): UserInterface
    {
        return $this->authenticatedUser;
    }

    public function setAuthenticatedUser(UserInterface $authenticatedUser): static
    {
        $this->authenticatedUser = $authenticatedUser;

        return $this;
    }
}
