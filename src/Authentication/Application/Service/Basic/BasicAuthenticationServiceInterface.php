<?php

declare(strict_types=1);

namespace Smtm\Auth\Authentication\Application\Service\Basic;

use Smtm\Auth\Authentication\Application\Service\AuthenticationServiceInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface BasicAuthenticationServiceInterface extends AuthenticationServiceInterface
{
    public function authenticateWithUsernameAndPassword(
        string $username,
        string $password
    ): void;
}
