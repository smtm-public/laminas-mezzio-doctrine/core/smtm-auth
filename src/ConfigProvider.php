<?php

declare(strict_types=1);

namespace Smtm\Auth;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'auth' => 'array',
        'commands' => 'array',
        'dependencies' => 'array',
        'doctrine' => 'array',
        'middleware_pipeline' => 'array',
        'routes' => 'array',
        'templates' => 'array',
    ])] public function __invoke(): array
    {
        return [
            'auth' => include __DIR__ . '/../config/auth.php',
            'commands' => include __DIR__ . '/../config/commands.php',
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'doctrine' => include __DIR__ . '/../config/doctrine.php',
            'middleware_pipeline' => include __DIR__ . '/../config/middlewares.php',
            'routes' => include __DIR__ . '/../config/routes.php',
            'templates' => include __DIR__ . '/../config/templates.php',
        ];
    }
}
