<?php

declare(strict_types=1);

namespace Smtm\Auth\Authorization\Application\Service\Options;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RequestAttributeValue
{
    public function __construct(
        protected string $attributeName
    ) {
    }

    public function getAttributeName(): string
    {
        return $this->attributeName;
    }

    public function setAttributeName(string $attributeName): static
    {
        return $this->__setProperty('attributeName', $attributeName);
    }
}
