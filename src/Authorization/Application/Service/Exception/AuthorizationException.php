<?php

declare(strict_types=1);

namespace Smtm\Auth\Authorization\Application\Service\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthorizationException extends RuntimeException
{

}
