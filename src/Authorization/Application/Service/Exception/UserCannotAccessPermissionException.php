<?php

declare(strict_types=1);

namespace Smtm\Auth\Authorization\Application\Service\Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserCannotAccessPermissionException extends AuthorizationException
{
    protected $message = 'The user roles do not have access to the required permission';
}
