<?php

declare(strict_types=1);

namespace Smtm\Auth\Authorization\Application\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface AuthorizationServiceInterface
{
    public function authorizeForPermissionName(string $permissionName): void;
    public function authorizeForOptions(array $options, array $request): bool;
}
