<?php

declare(strict_types=1);

namespace Smtm\Auth\Authorization\Application\Service;

use Smtm\Auth\Authentication\Application\Service\AuthenticationService;
use Smtm\Auth\Authentication\Application\Service\AuthenticationServiceInterface;
use Smtm\Auth\Authorization\Application\Service\Exception\UserCannotAccessPermissionException;
use Smtm\Auth\Authorization\Application\Service\Options\RequestAttributeValue;
use Smtm\Auth\Context\RoleCodePermission\Application\Service\RoleCodePermissionService;
use Smtm\Auth\Context\RoleCodePermission\Application\Service\RoleCodePermissionServiceInterface;
use Smtm\Auth\Context\User\Application\Extractor\UserExtractor;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthorizationService extends AbstractApplicationService implements AuthorizationServiceInterface
{
    public function authorizeForPermissionName(string $permissionName): void
    {
        /** @var AuthenticationService $authenticationService */
        $rootAuthenticationService = $this->applicationServicePluginManager->get(AuthenticationService::class);
        /** @var AuthenticationServiceInterface $authenticationService */
        $authenticationService = $rootAuthenticationService->getAuthenticationService();

        /** @var RoleCodePermissionService $roleCodePermissionService */
        $roleCodePermissionService = $this->applicationServicePluginManager->get(RoleCodePermissionServiceInterface::class);
        $roleCodePermission = $roleCodePermissionService->getAll(
            [
                'and',
                ['in', 'roleCode', $authenticationService->getRoleCodeCollection()],
                ['=', 'permission.name', $permissionName],
            ]
        );

        if (empty($roleCodePermission->getValues())) {
            throw new UserCannotAccessPermissionException();
        }
    }

    public function authorizeForOptions(array $options, array $request): bool
    {
        /** @var AuthenticationService $authenticationService */
        $rootAuthenticationService = $this->applicationServicePluginManager->get(AuthenticationService::class);
        /** @var AuthenticationServiceInterface $authenticationService */
        $authenticationService = $rootAuthenticationService->getAuthenticationService();
        $authenticatedUser = $authenticationService->getAuthenticatedUser();
        /** @var UserExtractor $userExtractor */
        $userExtractor = $this->extractorPluginManager->get(UserExtractor::class);
        $authenticatedUserExtracted = $userExtractor->extract($authenticatedUser);
        $result = true;

        if (array_key_exists('entityRules', $options)) {
            if (array_key_exists('affectedUsers', $options['entityRules'])) {
                $affected = true;

                foreach ($options['entityRules']['affectedUsers'] as $propertyName => $value) {
                    if ($authenticatedUserExtracted[$propertyName] !== $value) {
                        $affected = false;

                        break;
                    }
                }

                if (!$affected) {
                    return true;
                }
            }

            try {
                /** @var AbstractDbService $service */
                $service = $this->applicationServicePluginManager->get($options['entityRules']['service']);
                $criteria = [];

                if (array_key_exists('identifier', $options['entityRules'])) {
                    $identifierProperty = $options['entityRules']['identifier']['property'];

                    $value = match (true) {
                        $options['entityRules']['identifier']['value'] instanceof RequestAttributeValue =>
                            $request['attributes'][$options['entityRules']['identifier']['value']->getAttributeName()]
                    };

                    $criteria[] = ['=', $identifierProperty, $value];
                }

                if (array_key_exists('criteria', $options['entityRules'])) {
                    $criteria[] = $criteria;
                }

                $result = $service->{$options['entityRules']['method']}($criteria);
            } catch (\Throwable $t) {
                throw $t;
            }
        }

        return (bool)$result;
    }
}
