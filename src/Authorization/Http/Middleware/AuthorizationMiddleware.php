<?php

declare(strict_types=1);

namespace Smtm\Auth\Authorization\Http\Middleware;

use Smtm\Auth\Authorization\Application\Service\AuthorizationServiceInterface;
use Smtm\Auth\Authorization\Application\Service\Exception\UserCannotAccessPermissionException;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Http\Exception\ForbiddenException;
use Smtm\Base\Http\Middleware\Exception\InvalidArgumentException;
use Mezzio\Router\RouteResult;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthorizationMiddleware implements MiddlewareInterface, ConfigAwareInterface
{

    use ConfigAwareTrait;

    public function __construct(
        protected ApplicationServicePluginManager $applicationServicePluginManager
    ) {

    }

    /**
     * @inheritdoc
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->config['enableAuthenticationAndAuthorization'] && $this->config['enableAuthentication'] && $this->config['enableAuthorization']) {
            /** @var RouteResult $routeResult */
            $routeResult = $request->getAttribute(RouteResult::class);

            if (!$routeResult || !$routeResult->isSuccess()) {
                return $handler->handle($request);
            }

            $matchedRoute = $routeResult->getMatchedRoute();
            $routeOptions = $matchedRoute->getOptions();

            if (!array_key_exists('authorization', $routeOptions)) {
                throw new InvalidArgumentException('Missing \'authorization\' route option.');
            }

            if (!isset($routeOptions['authorization']) || !isset($routeOptions['authorization']['services'])) {
                return $handler->handle($request);
            }

            foreach ($routeOptions['authorization']['services'] as $authorizationServiceConfig) {
                /** @var AuthorizationServiceInterface $authorizationService */
                $authorizationService = $this->applicationServicePluginManager->get(
                    $authorizationServiceConfig['name'],
                    $authorizationServiceConfig['options']
                );

                try {
                    $authorizationService->authorizeForPermissionName($matchedRoute->getName());

                    if (array_key_exists('options', $authorizationServiceConfig)) {
                        if (!$authorizationService->authorizeForOptions(
                            $authorizationServiceConfig['options'],
                            [
                                'request' => [
                                    'attributes' => $request->getAttributes(),
                                ],
                            ]
                        )) {

                        }
                    }
                } catch (UserCannotAccessPermissionException $e) {
                    throw new ForbiddenException('', 0, $e);
                } catch (\Throwable $t) {
                    throw new ForbiddenException('', 0, $t);
                }
            }
        }

        return $handler->handle($request);
    }
}
