<?php

declare(strict_types=1);

namespace Smtm\Auth\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = 'auth';
}
