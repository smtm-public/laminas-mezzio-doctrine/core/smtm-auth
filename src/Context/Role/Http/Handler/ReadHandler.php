<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Http\Handler;

use Smtm\Auth\Context\Role\Application\Extractor\RoleExtractor;
use Smtm\Auth\Context\Role\Application\Service\RoleServiceInterface;
use Smtm\Base\Http\Handler\DbServiceEntity\UuidAware\AbstractReadHandler;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ReadHandler extends AbstractReadHandler
{
    public ?string $applicationServiceName = RoleServiceInterface::class;
    public ?string $domainObjectExtractorName = RoleExtractor::class;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        return parent::handle($request);
    }
}
