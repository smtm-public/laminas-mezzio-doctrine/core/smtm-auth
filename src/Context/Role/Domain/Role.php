<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Domain;

use Smtm\Auth\Context\Permission\Domain\Permission;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\IdAwareEntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Base\Domain\UuidAwareEntityTrait;
use Smtm\Base\MetaProgramming\ArrayOf;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Role implements RoleInterface, MarkedForUpdateInterface
{
    use IdAwareEntityTrait;
    use UuidAwareEntityTrait {
        __construct as traitConstructor;
    }
    use NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait,
        MarkedForUpdateTrait;

    protected ?string $description = null;
    protected string $code;
    protected Collection $permissionCollection;

    public function __construct()
    {
        $this->traitConstructor();

        $this->permissionCollection = new ArrayCollection();
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description = null): static
    {
        return $this->__setProperty('description', $description);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        return $this->__setProperty('code', $code);
    }

    /**
     * @return Permission[]
     */
    public function getPermissionCollection(): array
    {
        return $this->permissionCollection->getValues();
    }

    /**
     * @param Permission[] $permissionArray
     */
    public function setPermissionCollection(
        #[ArrayOf(Permission::class)] array $permissionArray
    ): self {
        $permissionCollection = new ArrayCollection($permissionArray);

        /** @var Permission $permission */
        foreach ($this->permissionCollection as $permission) {
            if (!$permissionCollection->contains($permission)) {
                $oldCollectionValues = $this->permissionCollection->getValues();
                $this->permissionCollection->removeElement($permission);
                $newCollectionValues = $this->permissionCollection->getValues();
                $this->__setMarkedForUpdate(true, 'permissionCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var Permission $permission */
        foreach ($permissionCollection as $permission) {
            if (!$this->permissionCollection->contains($permission)) {
                $oldCollectionValues = $this->permissionCollection->getValues();
                $this->permissionCollection->add($permission);
                $newCollectionValues = $this->permissionCollection->getValues();
                $this->__setMarkedForUpdate(true, 'permissionCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function hasPermission(Permission $permission): bool
    {
        $found = false;

        foreach ($this->permissionCollection as $ownPermission) {
            if ($ownPermission->getName() === $permission->getName()) {
                $found = true;

                break;
            }
        }

        return $found;
    }
}
