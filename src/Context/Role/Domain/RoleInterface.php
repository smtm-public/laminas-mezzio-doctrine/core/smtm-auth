<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Domain;

use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\UuidAwareEntityInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RoleInterface extends
    UuidAwareEntityInterface,
    CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface,
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface
{

    public function getDescription(): ?string;

    public function setDescription(?string $description = null): static;

    public function getCode(): string;

    public function setCode(string $code): static;
}
