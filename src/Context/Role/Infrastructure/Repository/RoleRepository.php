<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleRepository extends AbstractRepository implements RoleRepositoryInterface
{

}
