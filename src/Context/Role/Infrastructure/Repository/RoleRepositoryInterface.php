<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RoleRepositoryInterface extends RepositoryInterface
{

}
