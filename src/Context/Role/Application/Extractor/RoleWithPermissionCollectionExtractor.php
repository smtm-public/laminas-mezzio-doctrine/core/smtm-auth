<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Application\Extractor;

use Smtm\Auth\Context\Permission\Application\Extractor\PermissionExtractor;
use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Application\Extractor\Strategy\CollectionExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\DateTimeExtractionStrategy;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleWithPermissionCollectionExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'id' => null,
        'uuid' => null,
        'code' => null,
        'description' => null,
        'permissionCollection' => [
            'strategy' => [
                'name' => CollectionExtractionStrategy::class,
                'options' => [
                    CollectionExtractionStrategy::OPTION_KEY_EXTRACTOR_NAME => PermissionExtractor::class,
                ],
            ],
        ],
        'created' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
        'modified' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
    ];
}
