<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Application\Extractor;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Application\Extractor\Strategy\DateTimeExtractionStrategy;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'id' => null,
        'uuid' => null,
        'code' => null,
        'description' => null,
        'created' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
        'modified' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
    ];
}
