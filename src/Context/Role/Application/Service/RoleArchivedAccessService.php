<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleArchivedAccessService extends RoleService implements
    RoleArchivedAccessServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_ARCHIVED_ACCESS_MANAGER_NAME;
}
