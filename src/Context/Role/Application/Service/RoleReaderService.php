<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleReaderService extends RoleService implements
    RoleServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_READER_MANAGER_NAME;
}
