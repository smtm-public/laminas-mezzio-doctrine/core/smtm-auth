<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Application\Service;

use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Auth\Context\Role\Domain\Role;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleService extends AbstractDbService implements UuidAwareEntityDbServiceInterface
{

    use ArchivableUuidAwareEntityDbServiceTrait, UuidAwareEntityDbServiceTrait;

    protected ?string $domainObjectName = Role::class;
}
