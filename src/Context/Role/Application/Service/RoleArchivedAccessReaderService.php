<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Role\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleArchivedAccessReaderService extends RoleService implements
    RoleArchivedAccessReaderServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_ARCHIVED_ACCESS_READER_MANAGER_NAME;
}
