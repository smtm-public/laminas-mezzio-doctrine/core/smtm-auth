<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClient\Domain;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\UserClientRole\Domain\UserClientRoleInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\UuidAwareEntityInterface;
use Smtm\Base\MetaProgramming\ArrayOf;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UserClientInterface extends
    UuidAwareEntityInterface,
    CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface,
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface
{
    public function getUser(): UserInterface;

    public function setUser(UserInterface $user): static;

    public function getClient(): ClientInterface;

    public function setClient(ClientInterface $client): static;

    /**
     * @return UserClientRoleInterface[]
     */
    #[ArrayOf(UserClientRoleInterface::class)] public function getUserClientRoleCollection(): array;

    /**
     * @param UserClientRoleInterface[] $userClientRoleArray
     */
    public function setUserClientRoleCollection(
        #[ArrayOf(UserClientRoleInterface::class)] array $userClientRoleArray
    ): static;

    public function addUserClientRole(UserClientRoleInterface $userClientRole): static;

    public function removeUserClientRole(UserClientRoleInterface $userClientRole): static;

    public function hasUserClientRole(UserClientRoleInterface $userClientRole): bool;

    /**
     * @return string[]
     */
    #[ArrayOf('string')] public function getRoleCodeCollection(): array;
}
