<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClient\Domain;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\Role\Domain\RoleInterface;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\UserClientRole\Domain\UserClientRole;
use Smtm\Auth\Context\UserClientRole\Domain\UserClientRoleInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\IdAwareEntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Base\Domain\UuidAwareEntityTrait;
use Smtm\Base\MetaProgramming\ArrayOf;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserClient implements UserClientInterface, MarkedForUpdateInterface
{

    use IdAwareEntityTrait;
    use UuidAwareEntityTrait {
        __construct as traitConstructor;
    }
    use CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait,
        NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        MarkedForUpdateTrait;

    protected UserInterface $user;
    protected ClientInterface $client;
    protected Collection $userClientRoleCollection;

    public function __construct()
    {
        $this->traitConstructor();

        $this->userClientRoleCollection = new ArrayCollection();
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): static
    {
        return $this->__setProperty('user', $user);
    }

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function setClient(ClientInterface $client): static
    {
        return $this->__setProperty('client', $client);
    }

    /**
     * @return UserClientRoleInterface[]
     */
    #[ArrayOf(UserClientRoleInterface::class)] public function getUserClientRoleCollection(): array
    {
        return $this->userClientRoleCollection->getValues();
    }

    /**
     * @param UserClientRoleInterface[] $userClientRoleArray
     */
    public function setUserClientRoleCollection(
        #[ArrayOf(UserClientRoleInterface::class)] array $userClientRoleArray
    ): static {
        $userClientRoleCollection = new ArrayCollection($userClientRoleArray);

        /** @var UserClientRoleInterface $userClientRole */
        foreach ($this->userClientRoleCollection as $userClientRole) {
            if (!$userClientRoleCollection->contains($userClientRole)) {
                $oldCollectionValues = $this->userClientRoleCollection->getValues();
                $this->userClientRoleCollection->removeElement($userClientRole);
                $newCollectionValues = $this->userClientRoleCollection->getValues();
                $this->__setMarkedForUpdate(true, 'userClientRoleCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var UserClientRoleInterface $userClientRole */
        foreach ($userClientRoleCollection as $userClientRole) {
            if (!$this->userClientRoleCollection->contains($userClientRole)) {
                $oldCollectionValues = $this->userClientRoleCollection->getValues();
                $this->userClientRoleCollection->add($userClientRole);
                $newCollectionValues = $this->userClientRoleCollection->getValues();
                $this->__setMarkedForUpdate(true, 'userClientRoleCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function addUserClientRole(UserClientRoleInterface $userClientRole): static
    {
        if (!$this->userClientRoleCollection->contains($userClientRole)) {
            $oldCollectionValues = $this->userClientRoleCollection->getValues();
            $this->userClientRoleCollection->add($userClientRole);
            $newCollectionValues = $this->userClientRoleCollection->getValues();
            $this->__setMarkedForUpdate(true, 'userClientRoleCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function removeUserClientRole(UserClientRoleInterface $userClientRole): static
    {
        if ($this->userClientRoleCollection->contains($userClientRole)) {
            $oldCollectionValues = $this->userClientRoleCollection->getValues();
            $this->userClientRoleCollection->removeElement($userClientRole);
            $newCollectionValues = $this->userClientRoleCollection->getValues();
            $this->__setMarkedForUpdate(true, 'userClientRoleCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function hasUserClientRole(UserClientRoleInterface $userClientRole): bool
    {
        return $this->userClientRoleCollection->contains($userClientRole);
    }

    public function getUserClientRoleByUserClientAndRole(UserClientInterface $userClient, RoleInterface $role): UserClientRole|null
    {
        /** @var UserClientRoleInterface $userClientRole */
        foreach ($this->userClientRoleCollection as $userClientRole) {
            if ($userClientRole->getUserClient()->getId() === $userClient->getId()
                && $userClientRole->getRole()->getId() === $role->getId()
            ) {
                return $userClientRole;
            }
        }

        return null;
    }

    /**
     * @return string[]
     */
    #[ArrayOf('string')] public function getRoleCodeCollection(): array
    {
        $roleCodeCollection = [];

        /** @var UserClientRoleInterface $userClientRole */
        foreach ($this->userClientRoleCollection as $userClientRole) {
            $roleCodeCollection[] = $userClientRole->getRole()->getCode();
        }

        return $roleCodeCollection;
    }
}
