<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClient\Application\Hydrator;

use Smtm\Auth\Context\Client\Application\Service\ClientService;
use Smtm\Auth\Context\Client\Application\Service\ClientServiceInterface;
use Smtm\Auth\Context\Role\Application\Service\RoleService;
use Smtm\Auth\Context\User\Application\Service\UserService;
use Smtm\Auth\Context\User\Application\Service\UserServiceInterface;
use Smtm\Auth\Context\UserClient\Application\Service\UserClientService;
use Smtm\Auth\Context\UserClientRole\Application\Service\UserClientRoleService;
use Smtm\Auth\Context\UserClientRole\Domain\UserClientRole;
use Smtm\Base\Application\Hydrator\DomainObjectHydrator;
use Smtm\Base\Domain\DomainObjectInterface;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserClientHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'user' => 'You must specify a user for the UserClient.',
        'client' => 'You must specify a client for the UserClient.',
    ];

    public function hydrate(array $data, object $object): DomainObjectInterface
    {
        if (!empty($data)) {
            /** @var UserClientService $userClientService */
            $userClientService = $this->applicationServicePluginManager->get(UserClientService::class);

            if (isset($data['id'])) {
                $object = $userClientService->getById($data['id']);
                unset($data['id']);
            } elseif (isset($data['uuid'])) {
                $result = $userClientService->getOneOrNullByUuid($data['uuid']);

                if ($result !== null) {
                    $object = $result;
                    unset($data['uuid']);
                }
            }

            unset($data['created']);
            unset($data['modified']);

            if (!empty($data)) {
                if (array_key_exists('user', $data)) {
                    /** @var UserService $userService */
                    $userService = $this->applicationServicePluginManager->get(UserServiceInterface::class);

                    if (is_string($data['user'])) {
                        $data['user'] = $userService->getOneByUuid($data['user']);
                    } elseif (is_int($data['user'])) {
                        $data['user'] = $userService->getById($data['user']);
                    }
                }

                if (array_key_exists('client', $data)) {
                    /** @var ClientService $clientService */
                    $clientService = $this->applicationServicePluginManager->get(ClientServiceInterface::class);

                    if (is_string($data['client'])) {
                        $data['client'] = $clientService->getOneBy(['name' => $data['client']]);;
                    } elseif (is_int($data['client'])) {
                        $data['client'] = $clientService->getById($data['client']);
                    }
                }

                if (
                    isset($data['user'])
                    && ReflectionHelper::isPropertyInitialized('id', $data['user'])
                    && isset($data['client'])
                    && $userClientCollection = $userClientService->getAll([
                        'user' => $data['user'],
                        'client' => $data['client']
                    ])->getValues()
                ) {
                    $object = $userClientCollection[array_key_last($userClientCollection)];
                    unset($data['user']);
                    unset($data['client']);
                }

                if (!empty($data)) {
                    /** @var UserClientRoleService $userClientRoleService */
                    $userClientRoleService = $this->applicationServicePluginManager->get(UserClientRoleService::class);
                    /** @var RoleService $roleService */
                    $roleService = $this->applicationServicePluginManager->get(RoleService::class);

                    if (isset($data['userClientRoleCollection'])) {
                        $userClientRoleCollection = [];

                        foreach ($data['userClientRoleCollection'] as $userClientRoleData) {
                            $userClientRole = null;

                            if (is_string($userClientRoleData)) {
                                $userClientRole = $userClientRoleService->getOneByUuid($userClientRoleData);
                            } elseif (is_int($userClientRoleData)) {
                                $userClientRole = $userClientRoleService->getById($userClientRoleData);
                            } elseif (is_array($userClientRoleData)) {
                                $userClientRole = $userClientRoleService->hydrateDomainObject($userClientRoleData);
                            }

                            $userClientRoleCollection[] = $userClientRole;
                        }

                        $data['userClientRoleCollection'] = $userClientRoleCollection;
                    } elseif (isset($data['roleCollection'])) {
                        $userClientRoleCollection = [];

                        foreach ($data['roleCollection'] as $roleData) {
                            $role = null;

                            if (is_string($roleData)) {
                                $role = $roleService->getOneByUuid($roleData);
                            } elseif (is_int($roleData)) {
                                $role = $roleService->getById($roleData);
                            }

                            /** @var UserClientRole $userClientRole */
                            $userClientRole = $userClientRoleService->hydrateDomainObject([
                                'userClient' => $object,
                                'role' => $role,
                            ]);

                            $userClientRoleCollection[] = $userClientRole;
                        }

                        $data['userClientRoleCollection'] = $userClientRoleCollection;
                    } elseif (isset($data['roleCodeCollection'])) {
                        $userClientRoleCollection = [];

                        foreach ($data['roleCodeCollection'] as $roleCode) {
                            $role = $roleService->getOneBy(['code' => $roleCode]);

                            /** @var UserClientRole $userClientRole */
                            $userClientRole = $userClientRoleService->hydrateDomainObject([
                                'userClient' => $object,
                                'role' => $role,
                            ]);

                            $userClientRoleCollection[] = $userClientRole;
                        }

                        $data['userClientRoleCollection'] = $userClientRoleCollection;
                    }

                    return parent::hydrate($data, $object);
                }

                return $object;
            }

            return $object;
        }

        return $object;
    }
}
