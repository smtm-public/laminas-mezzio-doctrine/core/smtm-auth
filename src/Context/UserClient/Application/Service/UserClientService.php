<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClient\Application\Service;

use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Auth\Context\UserClient\Application\Hydrator\UserClientHydrator;
use Smtm\Auth\Context\UserClient\Domain\UserClient;
use Smtm\Auth\Context\UserClientRole\Application\Service\UserClientRoleService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Domain\EntityInterface;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserClientService extends AbstractDbService implements UuidAwareEntityDbServiceInterface
{

    use ArchivableUuidAwareEntityDbServiceTrait,
        UuidAwareEntityDbServiceTrait;

    public ?string $domainObjectName = UserClient::class;
    protected ?string $hydratorName = UserClientHydrator::class;

    public function saveFunc(
        ?EntityInterface $entity = null,
        array $data = [],
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        /** @var UserClientRoleService $userClientRoleService */
        $userClientRoleService = $this->applicationServicePluginManager->get(UserClientRoleService::class);

        $oldCollections = [];
        $oldCollections['userClientRole'] = [];

        /** @var UserClient $entity */
        if ($entity !== null) {
            $oldCollections['userClientRole'] = $entity->getUserClientRoleCollection();
        }

        /** @var UserClient $userClient */
        $userClient = $this->hydrateDomainObject($data, $entity);

        $newCollections['userClientRole'] = $userClient->getUserClientRoleCollection();
        $userClient->setUserClientRoleCollection([]);

        /** @var UserClient $userClient */
        $userClient = parent::saveFunc(
            $userClient,
            [],
            $options
        );

        $userClient->setUserClientRoleCollection($newCollections['userClientRole']);

        foreach ($oldCollections['userClientRole'] as $userClientRole) {
            if (!$userClient->hasUserClientRole($userClientRole)) {
                $userClientRoleService->deleteFunc($userClientRole, $options);
            }
        }

        foreach ($newCollections['userClientRole'] as $userClientRole) {
            $userClientRoleService->saveFunc(
                $userClientRole,
                [],
                $options
            );
        }

        return $userClient;
    }

    public function deleteFunc(
        EntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        $this->archiveFunc($entity, $options);
    }
}
