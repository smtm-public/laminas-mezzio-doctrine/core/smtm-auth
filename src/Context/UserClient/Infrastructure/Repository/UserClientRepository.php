<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClient\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserClientRepository extends AbstractRepository implements UserClientRepositoryInterface
{

}
