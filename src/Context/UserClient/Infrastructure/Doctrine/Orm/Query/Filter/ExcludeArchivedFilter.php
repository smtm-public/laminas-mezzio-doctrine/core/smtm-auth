<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClient\Infrastructure\Doctrine\Orm\Query\Filter;

use Smtm\Auth\Context\UserClient\Domain\UserClient;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExcludeArchivedFilter extends SQLFilter
{
    public const NAME = 'excludeSmtmAuthUserClientArchived';

    /**
     * @inheritDoc
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, string $targetTableAlias): string
    {
        if ($targetEntity->getReflectionClass()->name === UserClient::class) {
            return sprintf(
                <<< EOT
                EXISTS(
                    SELECT
                        id
                    FROM
                        auth_user
                    WHERE
                        auth_user.id=%s.auth_user_id
                        AND auth_user.not_archived = 1
                )
                EOT,
                $targetTableAlias
            );
        }

        return '';
    }
}
