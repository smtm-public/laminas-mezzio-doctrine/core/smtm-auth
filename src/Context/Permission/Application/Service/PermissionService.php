<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Permission\Application\Service;

use Smtm\Auth\Context\Permission\Domain\Permission;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\AbstractEntityManagerConfigAwareAndUuidAwareEntityDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class PermissionService extends AbstractEntityManagerConfigAwareAndUuidAwareEntityDbService implements
    PermissionServiceInterface
{

    use ArchivableUuidAwareEntityDbServiceTrait;

    protected ?string $domainObjectName = Permission::class;

    public function getOneByName(string $name): Permission
    {
        return $this->getOneBy(['name' => $name]);
    }
}
