<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Permission\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\DbReaderServiceInterface;
use Smtm\Base\Application\Service\ApplicationServiceInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface PermissionReaderServiceInterface extends ApplicationServiceInterface, DbReaderServiceInterface
{

}
