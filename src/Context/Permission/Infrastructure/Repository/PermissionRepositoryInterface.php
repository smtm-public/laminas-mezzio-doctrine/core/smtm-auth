<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Permission\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface PermissionRepositoryInterface extends RepositoryInterface
{

}
