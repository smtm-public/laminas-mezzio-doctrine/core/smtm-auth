<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClientRole\Domain;

use Smtm\Auth\Context\Role\Domain\RoleInterface;
use Smtm\Auth\Context\UserClient\Domain\UserClientInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\IdAwareEntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Base\Domain\UuidAwareEntityTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserClientRole implements UserClientRoleInterface, MarkedForUpdateInterface
{

    use IdAwareEntityTrait,
        UuidAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait,
        NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        MarkedForUpdateTrait;

    protected UserClientInterface $userClient;
    protected RoleInterface $role;

    public function getUserClient(): UserClientInterface
    {
        return $this->userClient;
    }

    public function setUserClient(UserClientInterface $userClient): static
    {
        return $this->__setProperty('userClient', $userClient);
    }

    public function getRole(): RoleInterface
    {
        return $this->role;
    }

    public function setRole(RoleInterface $role): static
    {
        return $this->__setProperty('role', $role);
    }
}
