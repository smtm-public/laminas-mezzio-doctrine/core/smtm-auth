<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClientRole\Domain;

use Smtm\Auth\Context\Role\Domain\RoleInterface;
use Smtm\Auth\Context\UserClient\Domain\UserClientInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\UuidAwareEntityInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UserClientRoleInterface extends
    UuidAwareEntityInterface,
    CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface,
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface
{
    public function getUserClient(): UserClientInterface;

    public function setUserClient(UserClientInterface $userClient): static;

    public function getRole(): RoleInterface;

    public function setRole(RoleInterface $role): static;
}
