<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClientRole\Infrastructure\Doctrine\Orm\Query\Filter;

use Smtm\Auth\Context\UserClientRole\Domain\UserClientRole;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ExcludeArchivedFilter extends SQLFilter
{
    public const NAME = 'excludeSmtmAuthUserClientRoleArchived';

    /**
     * @inheritDoc
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, string $targetTableAlias): string
    {
        if ($targetEntity->getReflectionClass()->name === UserClientRole::class) {
            return sprintf(
                <<< EOT
                EXISTS(
                    SELECT
                        id
                    FROM
                        auth_user_client
                    WHERE
                        auth_user_client.id=%s.auth_user_client_id
                        AND auth_user_client.not_archived = 1
                ) AND EXISTS(
                    SELECT
                        id
                    FROM
                        auth_role
                    WHERE
                        auth_role.id=%s.auth_role_id
                        AND auth_role.not_archived = 1
                )
                EOT,
                $targetTableAlias,
                $targetTableAlias,
                $targetTableAlias
            );
        }

        return '';
    }
}
