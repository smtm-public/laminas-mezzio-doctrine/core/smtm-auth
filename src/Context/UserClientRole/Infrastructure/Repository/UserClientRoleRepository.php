<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClientRole\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserClientRoleRepository extends AbstractRepository implements UserClientRoleRepositoryInterface
{

}
