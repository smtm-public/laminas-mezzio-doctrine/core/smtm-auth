<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClientRole\Application\Extractor;

use Smtm\Auth\Context\Role\Application\Extractor\RoleWithPermissionCollectionExtractor;
use Smtm\Auth\Context\UserClient\Application\Extractor\UserClientExtractor;
use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Application\Extractor\Strategy\DateTimeExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\DomainObjectExtractionStrategy;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserClientRoleWithPermissionExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'id' => null,
        'uuid' => null,
        'userClient' => [
            'include' => true,
            'strategy' => [
                'name' => DomainObjectExtractionStrategy::class,
                'options' => [
                    DomainObjectExtractionStrategy::OPTION_KEY_EXTRACTOR_NAME => UserClientExtractor::class,
                ],
            ],
        ],
        'role' => [
            'strategy' => [
                'name' => DomainObjectExtractionStrategy::class,
                'options' => [
                    DomainObjectExtractionStrategy::OPTION_KEY_EXTRACTOR_NAME => RoleWithPermissionCollectionExtractor::class,
                ],
            ],
        ],
        'created' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
        'modified' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
    ];
}
