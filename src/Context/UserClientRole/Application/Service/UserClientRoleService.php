<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClientRole\Application\Service;

use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Auth\Context\UserClientRole\Application\Hydrator\UserClientRoleHydrator;
use Smtm\Auth\Context\UserClientRole\Domain\UserClientRole;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\Domain\EntityInterface;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserClientRoleService extends AbstractDbService implements UuidAwareEntityDbServiceInterface
{

    use ArchivableUuidAwareEntityDbServiceTrait,
        UuidAwareEntityDbServiceTrait;

    public ?string $domainObjectName = UserClientRole::class;
    protected ?string $hydratorName = UserClientRoleHydrator::class;

    public function deleteFunc(
        EntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        $this->archiveFunc($entity, $options);
    }
}
