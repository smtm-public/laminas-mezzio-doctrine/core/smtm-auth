<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\UserClientRole\Application\Hydrator;

use Smtm\Auth\Context\Role\Application\Service\RoleService;
use Smtm\Auth\Context\UserClient\Application\Service\UserClientService;
use Smtm\Auth\Context\UserClientRole\Application\Service\UserClientRoleService;
use Smtm\Base\Application\Hydrator\DomainObjectHydrator;
use Smtm\Base\Domain\DomainObjectInterface;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserClientRoleHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'userClient' => 'You must specify a userClient for the UserClientRole.',
        'role' => 'You must specify a role for the UserClientRole.',
    ];

    public function hydrate(array $data, object $object): DomainObjectInterface
    {
        if (!empty($data)) {
            /** @var UserClientRoleService $userClientRoleService */
            $userClientRoleService = $this->applicationServicePluginManager->get(UserClientRoleService::class);

            if (isset($data['id'])) {
                $object = $userClientRoleService->getById($data['id']);
                unset($data['id']);
            } elseif (isset($data['uuid'])) {
                $result = $userClientRoleService->getOneOrNullByUuid($data['uuid']);

                if ($result !== null) {
                    $object = $result;
                    unset($data['uuid']);
                }
            }

            unset($data['created']);
            unset($data['modified']);

            if (!empty($data)) {
                /** @var UserClientService $userClientService */
                $userClientService = $this->applicationServicePluginManager->get(UserClientService::class);

                if (is_string($data['userClient'])) {
                    $data['userClient'] = $userClientService->getOneByUuid($data['userClient']);;
                } elseif (is_int($data['userClient'])) {
                    $data['userClient'] = $userClientService->getById($data['userClient']);
                }

                /** @var RoleService $roleService */
                $roleService = $this->applicationServicePluginManager->get(RoleService::class);

                if (is_string($data['role'])) {
                    $data['role'] = $roleService->getOneByUuid($data['role']);;
                } elseif (is_int($data['role'])) {
                    $data['role'] = $roleService->getById($data['role']);
                }

                if (
                    isset($data['userClient'])
                    && ReflectionHelper::isPropertyInitialized('id', $data['userClient'])
                    && isset($data['role'])
                    && ($userClientRoleCollection = $userClientRoleService->getAll([
                        'userClient' => $data['userClient'],
                        'role' => $data['role'],
                    ])->getValues())
                ) {
                    $object = $userClientRoleCollection[array_key_last($userClientRoleCollection)];
                    unset($data['userClient']);
                    unset($data['role']);
                }

                if (!empty($data)) {
                    return parent::hydrate($data, $object);
                }
            }

            return $object;
        }

        return $object;
    }
}
