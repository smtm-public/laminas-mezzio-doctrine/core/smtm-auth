<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Context\RedirectUri\Domain;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Domain\EntityTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RedirectUriInterface extends
    EntityInterface,
    CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface,
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface
{
    public function getClientId(): int;

    public function getClient(): ClientInterface;

    public function setClient(ClientInterface $client): static;

    public function getRedirectUri(): string;

    public function setRedirectUri(string $redirectUri): static;
}
