<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Context\RedirectUri\Domain;

use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\EntityTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RedirectUri implements RedirectUriInterface
{

    use EntityTrait,
        NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;

    protected int $clientId;
    protected ClientInterface $client;
    protected string $redirectUri;

    public function getClientId(): int
    {
        return $this->client->getId();
    }

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function setClient(ClientInterface $client): static
    {
        $this->client = $client;
        $this->clientId = $client->getId();

        return $this;
    }

    public function getRedirectUri(): string
    {
        return $this->redirectUri;
    }

    public function setRedirectUri(string $redirectUri): static
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }
}
