<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Context\RedirectUri\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RedirectUriRepository extends AbstractRepository implements RedirectUriRepositoryInterface
{

}
