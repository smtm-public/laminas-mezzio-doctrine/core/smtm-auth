<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Context\RedirectUri\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RedirectUriRepositoryInterface extends RepositoryInterface
{

}
