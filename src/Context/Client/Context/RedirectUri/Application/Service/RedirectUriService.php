<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Context\RedirectUri\Application\Service;

use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\ArchivableEntityDbServiceTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RedirectUriService extends AbstractDbService
{

    use ArchivableEntityDbServiceTrait;
}
