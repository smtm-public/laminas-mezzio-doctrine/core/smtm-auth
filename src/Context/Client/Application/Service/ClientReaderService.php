<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ClientReaderService extends ClientService implements ClientReaderServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_READER_MANAGER_NAME;
}
