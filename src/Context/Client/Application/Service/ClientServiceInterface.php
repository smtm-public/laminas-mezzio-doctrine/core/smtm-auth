<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Application\Service;

use Smtm\Base\Application\Service\ApplicationServiceInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ClientServiceInterface extends ApplicationServiceInterface
{

}
