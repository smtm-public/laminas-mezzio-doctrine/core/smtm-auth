<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Application\Service;

use Smtm\Auth\Context\Client\Domain\Client;
use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\AbstractEntityManagerConfigAwareAndUuidAwareEntityDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ClientService extends AbstractEntityManagerConfigAwareAndUuidAwareEntityDbService implements
    ClientServiceInterface
{

    use ArchivableUuidAwareEntityDbServiceTrait;

    protected ?string $domainObjectName = Client::class;

    public function getOneOrNullByClientId(string $clientId): ?ClientInterface
    {
        try {
            return $this->getOneBy(['clientId' => $clientId]);
        } catch (EntityNotFoundException $e) {
            return null;
        }
    }

    public function getOneByClientId(string $clientId): ClientInterface
    {
        return $this->getOneBy(['clientId' => $clientId]);
    }
}
