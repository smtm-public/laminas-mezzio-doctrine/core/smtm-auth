<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ClientArchivedAccessReaderService extends ClientService implements ClientArchivedAccessReaderServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_ARCHIVED_ACCESS_READER_MANAGER_NAME;
}
