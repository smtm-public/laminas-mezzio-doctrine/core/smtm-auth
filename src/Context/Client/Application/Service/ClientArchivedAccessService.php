<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ClientArchivedAccessService extends ClientService implements ClientArchivedAccessServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_ARCHIVED_ACCESS_MANAGER_NAME;
}
