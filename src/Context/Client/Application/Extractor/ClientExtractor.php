<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Application\Extractor;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;


/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ClientExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'uuid' => null,
        'name' => null,
    ];
}
