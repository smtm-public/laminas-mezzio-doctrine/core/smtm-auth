<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Application\Extractor;

use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;


/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ClientIdentityExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'id' => null,
        'uuid' => null,
    ];
}
