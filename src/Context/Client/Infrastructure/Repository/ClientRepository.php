<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ClientRepository extends AbstractRepository implements ClientRepositoryInterface
{

}
