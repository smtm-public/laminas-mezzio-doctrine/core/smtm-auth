<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Domain;

use Smtm\Auth\Context\Client\Context\RedirectUri\Domain\RedirectUriInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\UuidAwareEntityInterface;
use Smtm\Base\MetaProgramming\ArrayOf;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface ClientInterface extends
    UuidAwareEntityInterface,
    CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface,
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface
{
    public function getName(): string;

    public function setName(string $name): static;

    public function getClientId(): string;

    public function setClientId(string $clientId): static;

    public function getClientSecret(): string;

    public function setClientSecret(string $clientSecret): static;

    public function verifyClientSecret(string $clientSecret): bool;

    public function getReturnUriError(): string;

    public function setReturnUriError(string $returnUriError): static;

    /**
     * @return RedirectUriInterface[]
     */
    #[ArrayOf(RedirectUriInterface::class)] public function getRedirectUriCollection(): array;

    /**
     * @param RedirectUriInterface[] $redirectUriArray
     */
    public function setRedirectUriCollection(
        #[ArrayOf(RedirectUriInterface::class)] array $redirectUriArray
    ): static;

    public function hasRedirectUriWithRedirectUri(string $redirectUriValue): bool;
}
