<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Client\Domain;

use Smtm\Auth\Context\Client\Context\RedirectUri\Domain\RedirectUriInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\IdAwareEntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Base\Domain\UuidAwareEntityTrait;
use Smtm\Base\MetaProgramming\ArrayOf;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Client implements ClientInterface, MarkedForUpdateInterface
{

    use IdAwareEntityTrait;
    use UuidAwareEntityTrait {
        __construct as traitConstructor;
    }
    use NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait,
        MarkedForUpdateTrait;


    protected string $name;
    protected string $clientId;
    protected string $clientSecret;
    protected string $returnUriError;
    protected Collection $redirectUriCollection;

    public function __construct()
    {
        $this->traitConstructor();

        $this->redirectUriCollection = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        return $this->__setProperty('name', $name);
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function setClientId(string $clientId): static
    {
        return $this->__setProperty('clientId', $clientId);
    }

    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public function setClientSecret(string $clientSecret): static
    {
        return $this->__setProperty('clientSecret', $clientSecret);
    }

    public function verifyClientSecret(string $clientSecret): bool
    {
        return password_verify($clientSecret, $this->clientSecret);
    }

    public function getReturnUriError(): string
    {
        return $this->returnUriError;
    }

    public function setReturnUriError(string $returnUriError): static
    {
        return $this->__setProperty('returnUriError', $returnUriError);
    }

    /**
     * @return RedirectUriInterface[]
     */
    #[ArrayOf(RedirectUriInterface::class)] public function getRedirectUriCollection(): array
    {
        return $this->redirectUriCollection->getValues();
    }

    /**
     * @param RedirectUriInterface[] $redirectUriArray
     */
    public function setRedirectUriCollection(
        #[ArrayOf(RedirectUriInterface::class)] array $redirectUriArray
    ): static {
        $redirectUriCollection = new ArrayCollection($redirectUriArray);

        /** @var RedirectUriInterface $redirectUri */
        foreach ($this->redirectUriCollection as $redirectUri) {
            if (!$redirectUriCollection->contains($redirectUri)) {
                $oldCollectionValues = $this->redirectUriCollection->getValues();
                $this->redirectUriCollection->removeElement($redirectUri);
                $newCollectionValues = $this->redirectUriCollection->getValues();
                $this->__setMarkedForUpdate(true, 'redirectUriCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var RedirectUriInterface $redirectUri */
        foreach ($redirectUriCollection as $redirectUri) {
            if (!$this->redirectUriCollection->contains($redirectUri)) {
                $oldCollectionValues = $this->redirectUriCollection->getValues();
                $this->redirectUriCollection->add($redirectUri);
                $newCollectionValues = $this->redirectUriCollection->getValues();
                $this->__setMarkedForUpdate(true, 'redirectUriCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function hasRedirectUriWithRedirectUri(string $redirectUriValue): bool
    {
        $return = false;

        $this->redirectUriCollection->forAll(
            function (int $key, RedirectUriInterface $redirectUri) use (&$return, $redirectUriValue) {
                if ($redirectUri->getRedirectUri() === $redirectUriValue) {
                    $return = true;
                }
            }
        );

        return $return;
    }

}
