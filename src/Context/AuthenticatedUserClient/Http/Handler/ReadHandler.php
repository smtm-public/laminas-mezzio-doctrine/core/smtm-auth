<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\AuthenticatedUserClient\Http\Handler;

use Smtm\Auth\Authentication\Application\Service\Bearer\BearerAuthenticationServiceInterface;
use Smtm\Auth\Authentication\Http\Middleware\AuthenticationMiddleware;
use Smtm\Auth\Context\AuthenticatedUserClient\Application\Extractor\AuthenticatedUserClientExtractor;
use Smtm\Auth\Context\UserClient\Application\Service\UserClientService;
use Smtm\Base\Http\Handler\DbServiceEntity\UuidAware\AbstractReadHandler;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ReadHandler extends AbstractReadHandler
{
    public ?string $applicationServiceName = UserClientService::class;
    public ?string $domainObjectExtractorName = AuthenticatedUserClientExtractor::class;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        /** @var BearerAuthenticationServiceInterface $authenticationService */
        $authenticationService = $request->getAttribute(AuthenticationMiddleware::REQUEST_ATTRIBUTE_NAME_AUTHENTICATION_SERVICE);
        $entity = $authenticationService->getAuthenticatedUserClient();

        $parsedQueryParams = static::parseQueryParams($request->getQueryParams());

        return $this->prepareResponse($entity, $parsedQueryParams);
    }
}
