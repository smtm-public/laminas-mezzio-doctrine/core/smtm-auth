<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\AuthenticatedUserClient\Application\Extractor;

use Smtm\Auth\Context\Client\Application\Extractor\ClientExtractor;
use Smtm\Auth\Context\User\Application\Extractor\UserExtractor;
use Smtm\Auth\Context\UserClientRole\Application\Extractor\UserClientRoleWithPermissionExtractor;
use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Application\Extractor\Strategy\CollectionExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\DateTimeExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\DomainObjectExtractionStrategy;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AuthenticatedUserClientExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'id' => null,
        'uuid' => null,
        'user' => [
            'strategy' => [
                'name' => DomainObjectExtractionStrategy::class,
                'options' => [
                    DomainObjectExtractionStrategy::OPTION_KEY_EXTRACTOR_NAME => UserExtractor::class,
                ],
            ],
        ],
        'client' => [
            'strategy' => [
                'name' => DomainObjectExtractionStrategy::class,
                'options' => [
                    DomainObjectExtractionStrategy::OPTION_KEY_EXTRACTOR_NAME => ClientExtractor::class,
                ],
            ],
        ],
        'userClientRoleCollection' => [
            'strategy' => [
                'name' => CollectionExtractionStrategy::class,
                'options' => [
                    CollectionExtractionStrategy::OPTION_KEY_EXTRACTOR_NAME => UserClientRoleWithPermissionExtractor::class,
                ],
            ],
        ],
        'created' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
        'modified' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
    ];
}
