<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Exception;

use Smtm\Base\Application\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserNotFoundException extends RuntimeException
{
    protected $message = 'The user could not be found';
}
