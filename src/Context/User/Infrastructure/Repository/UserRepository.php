<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Infrastructure\Repository;

use DateTime;
use Smtm\Auth\Context\User\Domain\User;
use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    /**
     * @inheritdoc
     *
     * @throws EntitySaveException
     */
    public function storeUser(array $userData, array $roles = []): UserInterface
    {
        /** @var ?User $user */
        $user = null;

        try {
            $user = $this->findOneByUsername($userData['username']);
        } catch (EntityNotFoundException $e) { // phpcs:ignore
            // The User has not been found so we need to create it
        }

        $newLocale = false;

        if ($user === null) {
            // new User
            $user = new User();
            $user->setCreated(new DateTime());

            $newLocale = true;
        } else {
            // existing User
            if ($user->getCountry() !== $userData['country']) {
                $newLocale = true;
            }

            if ($this->isUserModified($user, $userData, $roles)) {
                $user->setModified(new DateTime());
            }
        }

        $user->setUsername($userData['username']);
        $user->setAuthType($userData['authType']);
        $user->setFirstName($userData['firstName']);
        $user->setLastName($userData['lastName']);
        $user->setEmail($userData['email']);
        $user->setCountry($userData['country']);

        if ($newLocale) {
            $user->setLocale(Locale::getDefaultLocaleByCountry($userData['country']));
        }

        $user->setEnvironment($userData['environment']);
        $user->setGender($userData['gender_iso5218']);
        $user->setB2b($userData['b2b']);
        $user->setB2bType($userData['b2b_type']);
        $user->setRoleCollection($roles);

        $this->save($user);

        return $user;
    }

    /**
     * @inheritDoc
     *
     * @throws NonUniqueResultException
     */
    public function findUserByToken(string $accessToken): ?UserInterface
    {
        $queryBuilder = $this->createQueryBuilder('t', OAuthToken::class);
        $queryBuilder->select('t')
            ->where($queryBuilder->expr()->eq('t.accessToken', ':accessToken'))
            ->setParameter('accessToken', $accessToken);

        $token = $queryBuilder->getQuery()->getOneOrNullResult();

        return $token ? $token->getUser() : null;
    }

    /**
     * @inheritdoc
     *
     * @throws EntityNotFoundException
     */
    public function findOneByUsername(string $username): ?User
    {
        return $this->findOneBy(['username' => $username]);
    }

    /**
     * @param User $user
     * @param array $userData
     * @param array $roles
     *
     * @return bool
     */
    private function isUserModified(User $user, array $userData, array $roles): bool
    {
        $isModified = false;

        if ($user->getFirstName() !== $userData['firstName']
            || $user->getLastName() !== $userData['lastName']
            || $user->getEmail() !== $userData['email']
            || $user->getCountry() !== $userData['country']
            || $user->getEnvironment() !== $userData['environment']
            || $user->getGender() !== $userData['gender_iso5218']
            || $user->getB2b() !== $userData['b2b']
            || $user->getB2bType() !== $userData['b2b_type']
        ) {
            $isModified = true;
        } else {
            if (count($user->getRoleCollection()) !== count($roles)) {
                $isModified = true;
            } else {
                foreach ($roles as $role) {
                    if (!$user->hasRole($role)) {
                        $isModified = true;

                        break;
                    }
                }
            }
        }

        return $isModified;
    }
}
