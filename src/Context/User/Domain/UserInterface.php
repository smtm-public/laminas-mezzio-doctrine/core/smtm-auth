<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Domain;

use DateTimeImmutable;
use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\UserClient\Domain\UserClientInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\IdAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\UuidAwareEntityInterface;
use Smtm\Base\MetaProgramming\ArrayOf;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface UserInterface extends
    IdAwareEntityInterface,
    UuidAwareEntityInterface,
    CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface,
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface
{
    public function getUsername(): ?string;

    public function setUsername(?string $username): static;

    public function getPassword(): ?string;

    public function setPassword(?string $password): static;

    public function verifyPassword(string $password): bool;

    public function getFirstName(): string;

    public function setFirstName(string $firstName): static;

    public function getLastName(): string;

    public function setLastName(string $lastName): static;

    public function getFullName(): string;

    public function getEmail(): ?string;

    public function setEmail(?string $email): static;

    public function getI18nTerritorialDivisionCodeIso3166Alpha2(): ?string;

    public function setI18nTerritorialDivisionCodeIso3166Alpha2(
        ?string $i18nTerritorialDivisionCodeIso3166Alpha2
    ): static;

    public function getGenderIso5218(): int;

    public function setGenderIso5218(int $genderIso5218): static;

    public function getLocale(): ?string;

    public function setLocale(?string $locale): static;

    /**
     * @return UserClientInterface[]
     */
    #[ArrayOf(UserClientInterface::class)] public function getUserClientCollection(): array;

    /**
     * @param UserClientInterface[] $userClientArray
     */
    public function setUserClientCollection(
        #[ArrayOf(UserClientInterface::class)] array $userClientArray
    ): static;

    public function addUserClient(UserClientInterface $userClient): static;

    public function removeUserClient(UserClientInterface $userClient): static;

    public function hasUserClient(UserClientInterface $userClient): bool;

    /**
     * @return ClientInterface[]
     */
    #[ArrayOf(ClientInterface::class)] public function getClientCollection(): array;

    public function hasClient(ClientInterface $searchClient): bool;

    /**
     * @return string[]
     */
    #[ArrayOf('string')] public function getRoleCodeCollectionForClient(ClientInterface $client): array;

    public function getExpires(): ?DateTimeImmutable;

    public function setExpires(?DateTimeImmutable $expires): static;

    public function isExpired(): bool;

    public function getIsSystem(): bool;

    public function setIsSystem(bool $isSystem): static;

    public function getStatus(): int;

    public function setStatus(int $status): static;

    public function getRegistrationCode(): ?string;

    public function setRegistrationCode(?string $registrationCode): static;

    public function getResetPasswordCode(): ?string;

    public function setResetPasswordCode(?string $resetPasswordCode): static;

    public function isInitialized(): bool;

    public function setInitialized(bool $initialized): static;

    public function isBlocked(): bool;

    public function setBlocked(bool $blocked): static;

    public function getValidUntil(): ?\DateTime;

    public function setValidUntil(?\DateTime $validUntil): static;
}
