<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Domain;

use DateTime;
use DateTimeImmutable;
use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\Permission\Domain\Permission;
use Smtm\Auth\Context\Title\Domain\Title;
use Smtm\Auth\Context\UserClient\Domain\UserClient;
use Smtm\Auth\Context\UserClient\Domain\UserClientInterface;
use Smtm\Auth\Domain\CreatedByAuthUserAndModifiedByAuthUserAwareEntityTrait;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\IdAwareEntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;
use Smtm\Base\Domain\UuidAwareEntityTrait;
use Smtm\Base\MetaProgramming\ArrayOf;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class User implements
    UserInterface, MarkedForUpdateInterface
{

    use IdAwareEntityTrait;
    use UuidAwareEntityTrait {
        __construct as traitConstructor;
    }
    use NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        CreatedByAuthUserAndModifiedByAuthUserAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait,
        MarkedForUpdateTrait;

//    public const TYPE_SYSTEM = 1;
//    public const TYPE_NON_SYSTEM = 2;
//    public const DEFAULT_TYPE = self::TYPE_NON_SYSTEM;
//    public const TYPES = [
//        self::TYPE_SYSTEM,
//        self::TYPE_NON_SYSTEM,
//    ];

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;
    public const DEFAULT_STATUS = self::STATUS_ACTIVE;
    public const STATUSES = [
        self::STATUS_INACTIVE,
        self::STATUS_ACTIVE,
    ];

    protected ?string $remoteUuid = null;
    protected ?string $username = null;
    protected ?string $password = null;
    protected ?string $email = null;
    protected ?string $emailUsername = null;
    protected ?string $emailDomain = null;
    protected ?Title $title = null;
    protected string $firstName;
    protected string $lastName;
    protected string $fullName;
    protected int $genderIso5218;
    protected ?string $i18nTerritorialDivisionCodeIso3166Alpha2 = null;
    protected ?string $locale = null;
    protected ?string $lastAuthProviderUuid = null;
    protected ?DateTime $lastLoginTime = null;
    protected Collection $userClientCollection;
    protected ?DateTimeImmutable $expires = null;
    protected bool $isSystem = false;
    protected int $status = self::STATUS_ACTIVE;
    protected ?string $registrationCode = null;
    protected ?string $resetPasswordCode = null;
    protected bool $initialized = false;
    protected bool $blocked = false;
    protected ?\DateTime $validUntil = null;


    public function __construct()
    {
        $this->traitConstructor();

        $this->userClientCollection = new ArrayCollection();
    }

    public function getRemoteUuid(): ?string
    {
        return $this->remoteUuid;
    }

    public function setRemoteUuid(?string $remoteUuid): static
    {
        return $this->__setProperty('remoteUuid', $remoteUuid);
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): static
    {
        return $this->__setProperty('username', $username);
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): static
    {
        return $this->__setProperty('password', $password);
    }

    public function verifyPassword(string $password): bool
    {
        return password_verify($password, $this->getPassword());
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        return $this->__setProperty('email', $email);
    }

    public function getEmailUsername(): ?string
    {
        return $this->emailUsername;
    }

    public function getEmailDomain(): ?string
    {
        return $this->emailDomain;
    }

    public function getTitle(): ?Title
    {
        return $this->title;
    }

    public function setTitle(?Title $title): static
    {
        return $this->__setProperty('title', $title);
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): static
    {
        return $this->__setProperty('firstName', $firstName);
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): static
    {
        return $this->__setProperty('lastName', $lastName);
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function getGenderIso5218(): int
    {
        return $this->genderIso5218;
    }

    public function setGenderIso5218(int $genderIso5218): static
    {
        return $this->__setProperty('genderIso5218', $genderIso5218);
    }

    public function getI18nTerritorialDivisionCodeIso3166Alpha2(): ?string
    {
        return $this->i18nTerritorialDivisionCodeIso3166Alpha2;
    }

    public function setI18nTerritorialDivisionCodeIso3166Alpha2(?string $i18nTerritorialDivisionCodeIso3166Alpha2
    ): static {
        return $this->__setProperty('i18nTerritorialDivisionCodeIso3166Alpha2',
            $i18nTerritorialDivisionCodeIso3166Alpha2);
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): static
    {
        return $this->__setProperty('locale', $locale);
    }

    public function getLastAuthProviderUuid(): ?string
    {
        return $this->lastAuthProviderUuid;
    }

    public function setLastAuthProviderUuid(?string $lastAuthProviderUuid): static
    {
        return $this->__setProperty('lastAuthProviderUuid', $lastAuthProviderUuid);
    }

    public function getLastLoginTime(): ?DateTime
    {
        return $this->lastLoginTime;
    }

    public function setLastLoginTime(?DateTime $lastLoginTime): static
    {
        return $this->__setProperty('lastLoginTime', $lastLoginTime);
    }

    public function hasClientAccessToPermission(ClientInterface $client, Permission $permission): bool
    {
        $return = false;

        /** @var UserClientInterface $userClient */
        foreach ($this->userClientCollection->getValues() as $userClient) {
            if ($userClient->getClient() === $client && $userClient->getRole()->hasPermission($permission)) {
                $return = true;

                break;
            }
        }

        return $return;
    }

    public function getExpires(): ?DateTimeImmutable
    {
        return $this->expires;
    }

    public function setExpires(?DateTimeImmutable $expires): static
    {
        return $this->__setProperty('expires', $expires);
    }

    public function isExpired(): bool
    {
        return $this->expires->getTimestamp() - time() <= 0;
    }

    public function getIsSystem(): bool
    {
        return $this->isSystem;
    }

    public function setIsSystem(bool $isSystem): static
    {
        return $this->__setProperty('isSystem', $isSystem);
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): static
    {
        return $this->__setProperty('status', $status);
    }

    /**
     * @return UserClientInterface[]
     */
    #[ArrayOf(UserClientInterface::class)] public function getUserClientCollection(): array
    {
        return $this->userClientCollection->getValues();
    }

    /**
     * @param UserClientInterface[] $userClientArray
     */
    public function setUserClientCollection(
        #[ArrayOf(UserClientInterface::class)] array $userClientArray
    ): static {
        $userClientCollection = new ArrayCollection($userClientArray);

        /** @var UserClientInterface $userClient */
        foreach ($this->userClientCollection as $userClient) {
            if (!$userClientCollection->contains($userClient)) {
                $oldCollectionValues = $this->userClientCollection->getValues();
                $this->userClientCollection->removeElement($userClient);
                $newCollectionValues = $this->userClientCollection->getValues();
                $this->__setMarkedForUpdate(true, 'userClientCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        /** @var UserClientInterface $userClient */
        foreach ($userClientCollection as $userClient) {
            if (!$this->userClientCollection->contains($userClient)) {
                $oldCollectionValues = $this->userClientCollection->getValues();
                $this->userClientCollection->add($userClient);
                $newCollectionValues = $this->userClientCollection->getValues();
                $this->__setMarkedForUpdate(true, 'userClientCollection', $oldCollectionValues, $newCollectionValues);
            }
        }

        return $this;
    }

    public function addUserClient(UserClientInterface $userClient): static
    {
        if (!$this->userClientCollection->contains($userClient)) {
            $oldCollectionValues = $this->userClientCollection->getValues();
            $this->userClientCollection->add($userClient);
            $newCollectionValues = $this->userClientCollection->getValues();
            $this->__setMarkedForUpdate(true, 'userClientCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function removeUserClient(UserClientInterface $userClient): static
    {
        if ($this->userClientCollection->contains($userClient)) {
            $oldCollectionValues = $this->userClientCollection->getValues();
            $this->userClientCollection->removeElement($userClient);
            $newCollectionValues = $this->userClientCollection->getValues();
            $this->__setMarkedForUpdate(true, 'userClientCollection', $oldCollectionValues, $newCollectionValues);
        }

        return $this;
    }

    public function hasUserClient(UserClientInterface $userClient): bool
    {
        return $this->userClientCollection->contains($userClient);
    }

    public function getUserClientByUserAndClient(UserInterface $user, ClientInterface $client): UserClient|null
    {
        /** @var UserClientInterface $userClient */
        foreach ($this->userClientCollection as $userClient) {
            if ($userClient->getUser()->getId() === $user->getId()
                && $userClient->getClient()->getId() === $client->getId()
            ) {
                return $userClient;
            }
        }

        return null;
    }


    /**
     * @return ClientInterface[]
     */
    #[ArrayOf(ClientInterface::class)] public function getClientCollection(): array
    {
        $clientCollection = [];

        /** @var UserClientInterface $userClient */
        foreach ($this->userClientCollection as $userClient) {
            if (!array_key_exists($userClient->getClient()->getId(), $clientCollection)) {
                $clientCollection[$userClient->getClient()->getId()] =
                    $userClient->getClient();
            }
        }

        return $clientCollection;
    }

    public function hasClient(ClientInterface $searchClient): bool
    {
        return in_array(
            $searchClient->getId(),
            array_map(
                fn (ClientInterface $client) => $client->getId(),
                $this->getClientCollection()
            )
        );
    }

    /**
     * @return string[]
     */
    #[ArrayOf('string')] public function getRoleCodeCollectionForClient(ClientInterface $client): array
    {
        /** @var UserClientInterface $userClient */
        foreach ($this->userClientCollection as $userClient) {
            if ($userClient->getClient()->getId() === $client->getId()) {
                return $userClient->getRoleCodeCollection();
            }
        }

        return [];
    }

    public function getRegistrationCode(): ?string
    {
        return $this->registrationCode;
    }

    public function setRegistrationCode(?string $registrationCode): static
    {
        return $this->__setProperty('registrationCode', $registrationCode);
    }

    public function getResetPasswordCode(): ?string
    {
        return $this->resetPasswordCode;
    }

    public function setResetPasswordCode(?string $resetPasswordCode): static
    {
        return $this->__setProperty('resetPasswordCode', $resetPasswordCode);
    }

    public function getInitialized(): bool
    {
        return $this->initialized;
    }

    public function setInitialized(bool $initialized): static
    {
        return $this->__setProperty('initialized', $initialized);
    }

    public function isInitialized(): bool
    {
        return $this->initialized;
    }

    public function getBlocked(): bool
    {
        return $this->blocked;
    }

    public function setBlocked(bool $blocked): static
    {
        return $this->__setProperty('blocked', $blocked);
    }

    public function isBlocked(): bool
    {
        return $this->blocked;
    }

    public function getValidUntil(): ?DateTime
    {
        return $this->validUntil;
    }

    public function setValidUntil(?DateTime $validUntil): static
    {
        return $this->__setProperty('validUntil', $validUntil);
    }
}
