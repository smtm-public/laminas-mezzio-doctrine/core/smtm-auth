<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Application\Extractor;

use Smtm\Auth\Context\Title\Application\Extractor\TitleExtractor;
use Smtm\Base\Application\Extractor\AbstractDomainObjectExtractor;
use Smtm\Base\Application\Extractor\Strategy\DateTimeExtractionStrategy;
use Smtm\Base\Application\Extractor\Strategy\DomainObjectExtractionStrategy;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserExtractor extends AbstractDomainObjectExtractor
{
    protected array $properties = [
        'id' => null,
        'uuid' => null,
        'title' => [
            'strategy' => [
                'name' => DomainObjectExtractionStrategy::class,
                'options' => [
                    DomainObjectExtractionStrategy::OPTION_KEY_EXTRACTOR_NAME => TitleExtractor::class,
                ],
            ],
        ],
        'firstName' => null,
        'lastName' => null,
        'username' => null,
        'email' => null,
        'lastAuthProviderUuid' => null,
        'lastLoginTime' => null,
//        'roleCollection' => [
//            'include' => true,
//            'strategy' => [
//                'name' => CollectionExtractionStrategy::class,
//                'options' => [
//                    DomainObjectExtractionStrategy::OPTION_KEY_EXTRACTOR_NAME => RoleExtractor::class,
//                ],
//            ],
//        ],
        'isSystem' => null,
        'status' => null,
        'created' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
        'modified' => [
            'strategy' => [
                'name' => DateTimeExtractionStrategy::class,
                'options' => [
                    DateTimeExtractionStrategy::OPTION_KEY_FORMAT => DateTimeHelper::DEFAULT_FORMAT,
                ],
            ],
        ],
    ];
}
