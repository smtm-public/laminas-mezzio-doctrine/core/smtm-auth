<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Application\Service;

use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Auth\Application\Service\ApplicationService\DbService\ArchivedByAuthUserAwareEntityDbServiceInterface;
use Smtm\Auth\Application\Service\ApplicationService\DbService\CreatedByAuthUserAwareEntityDbServiceInterface;
use Smtm\Auth\Application\Service\ApplicationService\DbService\ModifiedByAuthUserAwareEntityDbServiceInterface;
use Smtm\Auth\Authentication\Application\Service\AuthenticationService;
use Smtm\Auth\Authentication\Application\Service\Bearer\BearerAuthenticationServiceInterface;
use Smtm\Auth\Context\Client\Domain\ClientInterface;
use Smtm\Auth\Context\Role\Application\Service\RoleService;
use Smtm\Auth\Context\Token\Domain\TokenInterface;
use Smtm\Auth\Context\User\Application\Extractor\UserExtractor;
use Smtm\Auth\Context\User\Application\Hydrator\UserHydrator;
use Smtm\Auth\Context\User\Application\Service\Exception\InvalidEmailVerificationException;
use Smtm\Auth\Context\User\Domain\User;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Auth\Context\UserClient\Application\Service\UserClientService;
use Smtm\Auth\Context\UserClient\Domain\UserClient;
use Smtm\Auth\Context\UserClientRole\Application\Service\UserClientRoleService;
use Smtm\Auth\Context\UserClientRole\Domain\UserClientRole;
use Smtm\AuthProvider\Context\AuthCode\Application\Service\Exception\InvalidLoginCredentialsException;
use Smtm\AuthProvider\Context\Token\Application\Service\TokenService;
use Smtm\AuthProvider\Context\Token\Domain\Token;
use Smtm\Base\Application\Service\AbstractApplicationService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityCreateNonUniqueException;
use Smtm\Base\Application\Service\ApplicationService\Exception\EntityNotFoundException;
use Smtm\Base\Application\Service\ApplicationServiceInterface;
use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\Base\Infrastructure\Helper\GenderIso5218Helper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Helper\ReflectionHelper;
use Smtm\Base\Infrastructure\Repository\Exception\NotFoundRecordException;
use Smtm\Base\Infrastructure\Service\CryptoService;
use Smtm\Base\Infrastructure\Service\RemoteServiceConnector\RemoteServiceConnectorAuth;
use Smtm\Smtm\Context\User\Context\OAuthToken\Application\Service\SmtmService;
use Smtm\Email\Context\Message\Application\Service\MessageService;
use Smtm\Email\Context\Message\Domain\Message;
use JetBrains\PhpStorm\ArrayShape;
use Laminas\Mime\Mime as LaminasMime;
use Ramsey\Uuid\Uuid;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserService extends AbstractDbService implements
    ConfigAwareInterface,
    UserServiceInterface,
    CreatedByAuthUserAwareEntityDbServiceInterface,
    ModifiedByAuthUserAwareEntityDbServiceInterface,
    ArchivedByAuthUserAwareEntityDbServiceInterface,
    UuidAwareEntityDbServiceInterface
{

    use ConfigAwareTrait,
        ArchivableUuidAwareEntityDbServiceTrait {
            archiveFunc as traitArchiveFunc;
        }
    use UuidAwareEntityDbServiceTrait;

    protected ?string $domainObjectName = User::class;
    protected ?string $hydratorName = UserHydrator::class;

    public function getOneOrNullByUsername(string $username): ?User
    {
        try {
            return $this->getOneBy(['username' => $username]);
        } catch (EntityNotFoundException $e) {
            if ($e->getPrevious() instanceof NotFoundRecordException && !$e->getPrevious()->getPrevious()) {
                return null;
            }

            throw $e;
        }
    }

    public function getOneOrNullByEmail(string $email): ?User
    {
        try {
            return $this->getOneBy(['email' => strtolower($email)]);
        } catch (EntityNotFoundException $e) {
            if ($e->getPrevious() instanceof NotFoundRecordException && !$e->getPrevious()->getPrevious()) {
                return null;
            }

            throw $e;
        }
    }

    public function getOneByEmail(string $email): User
    {
        return $this->getOneBy(['email' => strtolower($email)]);
    }

    public function getOneOrNullByRemoteUuid(string $remoteUuid): ?User
    {
        try {
            return $this->getOneBy(['remoteUuid' => $remoteUuid]);
        } catch (EntityNotFoundException $e) {
            if ($e->getPrevious() instanceof NotFoundRecordException && !$e->getPrevious()->getPrevious()) {
                return null;
            }

            throw $e;
        }
    }

    public function getOneByRemoteUuid(string $remoteUuid): User
    {
        return $this->getOneBy(['remoteUuid' => $remoteUuid]);
    }

    public function deleteFunc(
        EntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        $this->archiveFunc($entity, $options);
    }

    public function archiveFunc(
        NotArchivedAwareEntityInterface $entity,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): void {
        $data = $this->extractorPluginManager->get(UserExtractor::class)->extract($entity);

        $authenticatedToken = null;
        $data['syncRemote'] = $options[ApplicationServiceInterface::OPTION_KEY_PARAMS]['syncRemote'] ?? false;
        $data['syncAllClients'] = $options[ApplicationServiceInterface::OPTION_KEY_PARAMS]['syncAllClients'] ?? false;

        if (!empty($data['syncRemote'])) {
            if (!isset($data['targetUserClientCollection'])) {
                $authenticatedToken = $this->getAuthenticatedToken($data);
                $authenticatedUserClient = $authenticatedToken->getUserClient();

                $data['targetUserClientCollection'] = [$authenticatedUserClient];

                if (isset($data['syncAllClients']) && $data['syncAllClients']) {
                    $data['targetUserClientCollection'] = $authenticatedUserClient->getUser()->getUserClientCollection();
                }
            }

            if (!isset($data['remoteUserData'])) {
                $authenticatedToken ??= $this->getAuthenticatedToken($data);

                $data['remoteUserData'] = $this->getRemoteUserData(
                    $entity,
                    $data,
                    $authenticatedToken
                );
            }

            if (!isset($data['remoteUuid'])) {
                $data['remoteUuid'] = $this->getRemoteUuid($entity, $data, $data['remoteUserData']);
            }
        } else {
            $data['remoteUserData'] = null;
            $data['remoteUuid'] = null;
        }

        $this->traitArchiveFunc($entity, $options);

        $this->deleteRemoteUser(
            $entity,
            $data,
            $data['remoteUserData'],
            $options,
            $authenticatedToken
        );
    }

    public function saveFunc(
        ?EntityInterface $entity = null,
        array $data = [],
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): EntityInterface {
        /** @var UserClientService $userClientService */
        $userClientService = $this->applicationServicePluginManager->get(UserClientService::class);
        $authenticatedToken = null;
        $data['syncRemote'] = $options[ApplicationServiceInterface::OPTION_KEY_PARAMS]['syncRemote'] ?? false;
        $data['syncAllClients'] = $options[ApplicationServiceInterface::OPTION_KEY_PARAMS]['syncAllClients'] ?? false;

        if (!empty($data['syncRemote'])) {
            if (!isset($data['targetUserClientCollection'])) {
                $authenticatedToken = $this->getAuthenticatedToken($data);
                $authenticatedUserClient = $authenticatedToken->getUserClient();

                $data['targetUserClientCollection'] = [$authenticatedUserClient];

                if (!empty($data['syncAllClients'])) {
                    $data['targetUserClientCollection'] = $authenticatedUserClient->getUser()->getUserClientCollection();
                }
            }

            if (!isset($data['remoteUserData'])) {
                $authenticatedToken ??= $this->getAuthenticatedToken($data);

                $data['remoteUserData'] = $this->getRemoteUserData(
                    $entity,
                    $data,
                    $authenticatedToken
                );
            }

            if (($entity === null || ($entity instanceof UserInterface && !ReflectionHelper::isPropertyInitialized('id', $entity)))
                && $data['remoteUserData']
            ) {
                $criteria = [
                    'or',
                ];

                if (isset($data['username'])) {
                    $criteria['username'] = $data['username'];
                }

                if (isset($data['email'])) {
                    $criteria['email'] = $data['email'];
                }

                if (!($existingUser = $this->getOneOrNullBy($criteria))) {
                    $data['remoteUuid'] = $data['remoteUserData']['uuid'];
                } else {
                    $exceptionMessage = 'The entity is not unique.';

                    if ($data['username'] === $data['remoteUserData']['username']) {
                        $exceptionMessage .= ' Username `' . $data['username'] . '` matches and existing user\'s username.';
                    }

                    if ($data['email'] === $data['remoteUserData']['email']) {
                        $exceptionMessage .= ' Email `' . $data['email'] . '` matches an existing user\'s email.';
                    }

                    throw new EntityCreateNonUniqueException($exceptionMessage);
                }
            }

            if (!isset($data['remoteUuid'])) {
                $data['remoteUuid'] = $this->getRemoteUuid($entity, $data, $data['remoteUserData']);
            }
        } else {
            $data['remoteUserData'] = null;
            $data['remoteUuid'] = null;
        }

        $oldCollections = [];
        $oldCollections['userClient'] = [];

        /** @var User $entity */
        if ($entity !== null) {
            $oldCollections['userClient'] = $entity->getUserClientCollection();

            foreach ($oldCollections['userClient'] as $userClient) {
                $userClientService->saveFunc(
                    $userClient,
                    array_diff_key($data, ['id' => null, 'uuid' => null]),
                    $options
                );
            }
        }

        /** @var User $user */
        $user = $this->hydrateDomainObject($data, $entity);

        $newCollections['userClient'] = $user->getUserClientCollection();
        $user->setUserClientCollection([]);

        $shortData = [
            'remoteUserData' => $data['remoteUserData'],
            'remoteUuid' => $data['remoteUuid'],
        ];

        if (isset($data['createdBy'])) {
            $shortData['createdBy'] = $data['createdBy'];
        }

        if (isset($data['modifiedBy'])) {
            $shortData['modifiedBy'] = $data['modifiedBy'];
        }

        /** @var User $user */
        $user = parent::saveFunc(
            $user,
            $shortData,
            $options
        );

        $user->setUserClientCollection($newCollections['userClient']);

        if ($entity !== null) {
            foreach ($oldCollections['userClient'] as $userClient) {
                if (!$user->hasUserClient($userClient)) {
                    $userClientService->deleteFunc($userClient, $options);
                }
            }
        }

        foreach ($newCollections['userClient'] as $userClient) {
            $userClientService->saveFunc(
                $userClient,
                [],
                $options
            );
        }

        $this->syncRemoteUser(
            $user,
            $data,
            $data['remoteUserData'],
            $options,
            $authenticatedToken
        );

        return $user;
    }

    public function getAuthenticatedToken(array $data): Token
    {
        if (isset($data['authenticatedToken'])) {
            /** @var TokenService $tokenService */
            $tokenService = $this->applicationServicePluginManager->get(TokenService::class);
            $authenticatedToken = $tokenService->getOneBy(['accessToken' => $data['authenticatedToken']]);

            return $authenticatedToken;
        }

        return $this->applicationServicePluginManager
            ->get(AuthenticationService::class)
            ->getAuthenticationService()
            ->getAuthenticatedToken();
    }

    public function getRemoteUserData(
        ?UserInterface $user,
        array $data,
        TokenInterface $authenticatedToken
    ): array|false|null {
        $remoteUserData = null;

        if (isset($data['syncRemote']) && $data['syncRemote']) {
            /** @var SmtmService $smtmService */
            $smtmService = $this->applicationServicePluginManager->get(
                SmtmService::class
            );
            $criteria = [
                'or',
            ];

            if ($user === null || $user->getRemoteUuid() === null) {
                if (isset($data['username'])) {
                    $criteria['username'] = $data['username'];
                } elseif ($user !== null && $user->getUsername() !== null) {
                    $criteria['username'] = $user->getUsername();
                }

                if (isset($data['email'])) {
                    $criteria['email'] = $data['email'];
                } elseif ($user !== null && $user->getEmail() !== null) {
                    $criteria['email'] = $user->getEmail();
                }
            } else {
                $criteria['uuid'] = $user->getRemoteUuid();
            }

            $remoteUserData = $smtmService->authUserIndex(
                RemoteServiceConnectorAuth::create(
                    RemoteServiceConnectorAuth::TYPE_BEARER,
                    [
                        'token' => $authenticatedToken->getToken()->getAccessToken(),
                    ]
                ),
                ['criteria' => $criteria],
                [
                    AbstractApplicationService::OPTION_NAME_LOG_EXTRA_DATA =>
                        [
                            'script' => __FILE__,
                            'service_name' => 'smtm-auth-provider',
                        ],
                ]
            );
            $remoteUserData = reset($remoteUserData);
        }

        return $remoteUserData;
    }

    public function getRemoteUuid(
        ?UserInterface $user,
        array $data,
        array|false|null $remoteUserData
    ): string|null {
        if (isset($data['syncRemote']) && $data['syncRemote']) {
            return ($user === null || $user->getRemoteUuid() === null)
                ? (
                    $remoteUserData === false
                        ? $this->prepareDomainObject()->getUuid()
                        : $remoteUserData['uuid']
                )
                : $user->getRemoteUuid();
        }

        return null;
    }

    public function deleteRemoteUser(
        UserInterface $user,
        array $data,
        array|false|null $remoteUserData,
        array $options,
        ?TokenInterface $authenticatedToken
    ): void {
        $remoteUserDataPersisted = null;

        if (isset($data['syncRemote']) && $data['syncRemote']) {
            /** @var SmtmService $smtmService */
            $smtmService = $this->applicationServicePluginManager->get(
                SmtmService::class
            );
            $remotePersistData = $this->getRemotePersistData($data);
            $remotePersistParams = $this->getRemotePersistParams($options);

            if ($authenticatedToken === null) {
                $authenticatedToken = $this->getAuthenticatedToken($data);
            }

            $smtmService->authUserDelete(
                RemoteServiceConnectorAuth::create(
                    RemoteServiceConnectorAuth::TYPE_BEARER,
                    [
                        'token' => $authenticatedToken->getToken()->getAccessToken(),
                    ]
                ),
                $remotePersistData['uuid'],
                $remotePersistData,
                $remotePersistParams,
                [
                    AbstractApplicationService::OPTION_NAME_LOG_EXTRA_DATA =>
                        [
                            'script' => __FILE__,
                            'service_name' => 'smtm-auth-provider',
                        ],
                ]
            );
        }
    }

    public function syncRemoteUser(
        UserInterface $user,
        array $data,
        array|false|null $remoteUserData,
        array $options,
        ?TokenInterface $authenticatedToken
    ): array|null {
        $remoteUserDataPersisted = null;

        if (isset($data['syncRemote']) && $data['syncRemote']) {
            /** @var SmtmService $smtmService */
            $smtmService = $this->applicationServicePluginManager->get(
                SmtmService::class
            );
            $remotePersistData = $this->getRemotePersistData($data);
            $remotePersistParams = $this->getRemotePersistParams($options);

            if ($authenticatedToken === null) {
                $authenticatedToken = $this->getAuthenticatedToken($data);
            }

            if ($remoteUserData === false) {
                $remoteUserDataPersisted = $smtmService->authUserCreate(
                    RemoteServiceConnectorAuth::create(
                        RemoteServiceConnectorAuth::TYPE_BEARER,
                        [
                            'token' => $authenticatedToken->getToken()->getAccessToken(),
                        ]
                    ),
                    $remotePersistData,
                    $remotePersistParams,
                    [
                        AbstractApplicationService::OPTION_NAME_LOG_EXTRA_DATA =>
                            [
                                'script' => __FILE__,
                                'service_name' => 'smtm-auth-provider',
                            ],
                    ]
                );
            } else {
                $bearerAuthentication = RemoteServiceConnectorAuth::create(
                    RemoteServiceConnectorAuth::TYPE_BEARER,
                    [
                        'token' => $authenticatedToken->getToken()->getAccessToken(),
                    ]
                );
                $remoteCallOptions = [
                    AbstractApplicationService::OPTION_NAME_LOG_EXTRA_DATA =>
                        [
                            'script' => __FILE__,
                            'service_name' => 'smtm-auth-provider',
                        ],
                ];

                $remoteUserDataPersisted = match ($options[ApplicationServiceInterface::OPTION_KEY_METHOD] ?? null) {
                    HttpHelper::METHOD_POST, HttpHelper::METHOD_PUT => $smtmService->authUserPut(
                        $bearerAuthentication,
                        $remotePersistData['uuid'],
                        $remotePersistData,
                        $remotePersistParams,
                        $remoteCallOptions
                    ),
                    default => $smtmService->authUserPatch(
                        $bearerAuthentication,
                        $remotePersistData['uuid'],
                        $remotePersistData,
                        $remotePersistParams,
                        $remoteCallOptions
                    ),
                };
            }
        }

        return $remoteUserDataPersisted;
    }

    public function getRemotePersistData(array $data): array
    {
        $remotePersistData = $data;
        $remotePersistData['uuid'] = $data['remoteUuid'];
        unset($remotePersistData['syncRemote']);
        unset($remotePersistData['syncAllClients']);
        unset($remotePersistData['remoteUserData']);
        unset($remotePersistData['targetUserClientCollection']);
        unset($remotePersistData['id']);
        unset($remotePersistData['remoteUuid']);
        unset($remotePersistData['created']);
        unset($remotePersistData['modified']);
        unset($remotePersistData['createdBy']);
        unset($remotePersistData['modifiedBy']);

        return $remotePersistData;
    }

    public function getRemotePersistParams(array $options): array
    {
        $params = $options[ApplicationServiceInterface::OPTION_KEY_PARAMS] ?? [];
        unset($params['syncRemote']);

        return $params;
    }

    public function verifyEmail(
        array $emailVerification,
        #[ArrayShape([
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => 'string | null',
            ApplicationServiceInterface::OPTION_KEY_PARAMS => 'array',
            ApplicationServiceInterface::OPTION_KEY_METHOD => 'string | null',
        ])] array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
            ApplicationServiceInterface::OPTION_KEY_PARAMS => [],
            ApplicationServiceInterface::OPTION_KEY_METHOD => null,
        ]
    ): User {
        /** CryptoService $cryptoService */
        $cryptoService = $this->infrastructureServicePluginManager->get(CryptoService::class);

        try {
            $emailVerificationDecrypted = $cryptoService->decrypt($emailVerification);
            $emailVerificationData = json_decode(
                $emailVerificationDecrypted,
                true,
                JSON_THROW_ON_ERROR
            );
        } catch (\Exception $e) {
            throw new InvalidEmailVerificationException('', 0, $e);
        }

        /** @var User $user */
        $user = $this->getOneByUuid($emailVerificationData['uuid']);

        return parent::update(
            $user,
            [
                'emailVerification' => null,
            ],
            $options
        );
    }

    public function getByClientAndUsernameOrEmailAndPassword(
        ClientInterface $client,
        string $usernameOrEmail,
        string $password
    ): User {

        /** @var User $user */
        $user = $this->getByClientAndUsernameOrEmail($client, $usernameOrEmail);

        if (!$user->verifyPassword($password) || $user->isBlocked()) {
            throw new InvalidLoginCredentialsException(
                'Incorrect username or password',
                HttpHelper::STATUS_CODE_UNAUTHORIZED
            );
        }

        return $user;
    }

    public function getByClientAndUsernameOrEmail(ClientInterface $client, string $usernameOrEmail): UserInterface
    {
        return $this->getOneBy([
            'and',
            ['=', 'userClientCollection.client', $client],
            [
                'or',
                ['=', 'username', $usernameOrEmail],
                ['=', 'email', $usernameOrEmail],
            ],
            ['=', 'blocked', 0]
        ]);
    }

    public function sendEmailCreate(
        ClientInterface $client,
        User $user
    ): void {
        /** @var MessageService $messageService */
        $messageService = $this->applicationServicePluginManager->get(MessageService::class);
        /** @var Message $message */
        $message = $messageService->prepareDomainObject();
        $message = $messageService->setSubjectTemplate(
            $message,
            $this->config['clientOptions'][$client->getClientId()]['email']['message']['userCreated']['templateSubject'],
            $subjectTemplateParams = $this->getEmailTemplateParams($user)
        );

        /** @var CryptoService $cryptoService */
        $cryptoService = $this->infrastructureServicePluginManager->get(CryptoService::class);
        $rCode = $cryptoService->encrypt(
            json_encode([
                'uuid' => $user->getUuid(),
                'email' => $user->getEmail(),
                'created' => $user->getCreated(),
                'nonce' => Uuid::uuid4() . '_' . time(),
            ])
        );
        $returnUrl = HttpHelper::urlAddQueryParams(
            $this->config['clientOptions'][$client->getClientId()]['email']['message']['userCreated']['returnUri'],
            ['rCode' => $rCode]
        );
        $this->update($user, ['registrationCode' => $rCode]);
        $contentTemplateParamsHtml = $subjectTemplateParams + [
                'layout' => $this->config['clientOptions'][$client->getClientId()]['email']['templatesLayout'][LaminasMime::TYPE_HTML],
                'link' => $returnUrl,
            ];
        $message = $messageService->addContentWithTemplate(
            $message,
            $this->config['clientOptions'][$client->getClientId()]['email']['message']['userCreated']['templateContent'],
            LaminasMime::TYPE_HTML,
            $contentTemplateParamsHtml
        );
        $contentTemplateParamsText = $subjectTemplateParams + [
                'layout' => $this->config['clientOptions'][$client->getClientId()]['email']['templatesLayout'][LaminasMime::TYPE_TEXT],
                'link' => $returnUrl,
            ];
        $message = $messageService->addContentWithTemplate(
            $message,
            $this->config['clientOptions'][$client->getClientId()]['email']['message']['userCreated']['templateContent'],
            LaminasMime::TYPE_TEXT,
            $contentTemplateParamsText
        );
        $message = $messageService->addFrom(
            $message,
            $this->config['clientOptions'][$client->getClientId()]['email']['message']['userCreated']['from']
        );
        $message = $messageService->addTo(
            $message,
            $user->getEmail(),
            $user->getFullName()
        );

        $messageService->send($message);
    }

    public function getResetPasswordUrl(
        ClientInterface $client,
        UserInterface $user,
        string $reason = '',
        array $options = [
            ApplicationServiceInterface::OPTION_KEY_REMOTE_ADDRESS => null,
        ]
    ): string {
        /** @var CryptoService $cryptoService */
        $cryptoService = $this->infrastructureServicePluginManager->get(CryptoService::class);
        $resetPasswordCode = $cryptoService->encrypt(
            json_encode([
                'uuid' => $user->getUuid(),
                'email' => $user->getEmail(),
                'created' => $user->getCreated(),
                'nonce' => Uuid::uuid4() . '_' . time(),
                'expires' => time() + 3600*24,
            ])
        );

        $this->saveFunc(
            $user,
            [
                'resetPasswordCode' => $resetPasswordCode,
                'modifiedBy' => $this->getById($this->config['defaultSystemUserId']),
            ],
            $options
        );

        $url = $this->config['clientOptions'][$client->getClientId()]['email']['message']['passwordReset']['returnUri'];
        $queryParams = ['rpCode' => $resetPasswordCode, 'reason' => $reason];

        $returnUrl = HttpHelper::urlAddQueryParams($url, $queryParams);

        return $returnUrl;
    }

    public function sendEmailPasswordReset(
        ClientInterface $client,
        User $user
    ): void {
        /** @var MessageService $messageService */
        $messageService = $this->applicationServicePluginManager->get(MessageService::class);
        /** @var Message $message */
        $message = $messageService->prepareDomainObject();
        $message = $messageService->setSubjectTemplate(
            $message,
            $this->config['clientOptions'][$client->getClientId()]['email']['message']['passwordReset']['templateSubject'],
            $subjectTemplateParams = $this->getEmailTemplateParams($user)
        );

        $returnUrl = $this->getResetPasswordUrl($client, $user);
        $contentTemplateParamsHtml = $subjectTemplateParams + [
                'layout' => $this->config['clientOptions'][$client->getClientId()]['email']['templatesLayout'][LaminasMime::TYPE_HTML],
                'link' => $returnUrl,
            ];
        $message = $messageService->addContentWithTemplate(
            $message,
            $this->config['clientOptions'][$client->getClientId()]['email']['message']['passwordReset']['templateContent'],
            LaminasMime::TYPE_HTML,
            $contentTemplateParamsHtml
        );
        $contentTemplateParamsText = $subjectTemplateParams + [
                'layout' => $this->config['clientOptions'][$client->getClientId()]['email']['templatesLayout'][LaminasMime::TYPE_TEXT],
                'link' => $returnUrl,
            ];
        $message = $messageService->addContentWithTemplate(
            $message,
            $this->config['clientOptions'][$client->getClientId()]['email']['message']['passwordReset']['templateContent'],
            LaminasMime::TYPE_TEXT,
            $contentTemplateParamsText
        );
        $message = $messageService->addFrom(
            $message,
            $this->config['clientOptions'][$client->getClientId()]['email']['message']['passwordReset']['from']
        );
        $message = $messageService->addTo(
            $message,
            $user->getEmail(),
            $user->getFullName()
        );

        $messageService->send($message);
    }

    #[ArrayShape([
        'firstName' => 'string',
        'lastName' => 'string',
        'fullName' => 'string',
        'email' => 'string',
        'gender' => 'string',
        'created' => 'string'
    ])] public function getEmailTemplateParams(User $user): array
    {
        return [
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'fullName' => $user->getFullname(),
            'email' => $user->getEmail(),
            'gender' => $user->getGenderIso5218() !== null
                ? GenderIso5218Helper::GENDER_NAME[$user->getGenderIso5218()]
                : GenderIso5218Helper::GENDER_NAME[GenderIso5218Helper::NOT_SPECIFIED],
            'created' => DateTimeHelper::format(
                $user->getCreated(),
                DateTimeHelper::DEFAULT_FORMAT
            ),
        ];
    }
}
