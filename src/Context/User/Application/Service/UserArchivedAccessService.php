<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserArchivedAccessService extends UserService implements
    UserArchivedAccessServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_ARCHIVED_ACCESS_MANAGER_NAME;
}
