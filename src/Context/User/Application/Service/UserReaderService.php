<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserReaderService extends UserService implements UserReaderServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_READER_MANAGER_NAME;
}
