<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Application\Service\Exception;

use RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InvalidEmailVerificationException extends RuntimeException
{

}

