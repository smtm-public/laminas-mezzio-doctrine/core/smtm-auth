<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Application\Hydrator;

use Smtm\Auth\Authentication\Application\Service\AuthenticationService;
use Smtm\Auth\Authentication\Application\Service\Bearer\BearerAuthenticationServiceInterface;
use Smtm\Auth\Context\Role\Application\Service\RoleService;
use Smtm\Auth\Context\Title\Application\Service\TitleService;
use Smtm\Auth\Context\User\Application\Service\UserService;
use Smtm\Auth\Context\User\Application\Service\UserServiceInterface;
use Smtm\Auth\Context\UserClient\Application\Service\UserClientService;
use Smtm\Auth\Context\UserClient\Domain\UserClient;
use Smtm\Auth\Context\UserClientRole\Application\Service\UserClientRoleService;
use Smtm\Auth\Context\UserClientRole\Domain\UserClientRole;
use Smtm\AuthProvider\Context\Token\Domain\Token;
use Smtm\Base\Application\Hydrator\DomainObjectHydrator;
use Smtm\Base\Domain\DomainObjectInterface;
use Smtm\Base\Infrastructure\Helper\GenderIso5218Helper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class UserHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'firstName' => 'You must specify firstName for the User',
        'lastName' => 'You must specify lastName for the User',
        'genderIso5218' => 'You must specify genderIso5218 for the User',
        'initialized' => 'You must specify initialized for the User',
        'blocked' => 'You must specify blocked for the User',
        'status' => 'You must specify status for the User',
    ];

    public function hydrate(array $data, object $object): DomainObjectInterface
    {
        if (!empty($data)) {
            /** @var UserService $userService */
            $userService = $this->applicationServicePluginManager->get(UserService::class);

            if (isset($data['id'])) {
                $object = $userService->getById($data['id']);
                unset($data['id']);
            } elseif (isset($data['uuid'])) {
                $result = $userService->getOneOrNullByUuid($data['uuid']);

                if ($result !== null) {
                    $object = $result;
                    unset($data['uuid']);
                }
            }

            unset($data['created']);
            unset($data['modified']);

            if (!empty($data)) {
                $authenticatedToken = null;

                if (!array_key_exists('remoteUserData', $data)) {
                    $authenticatedToken ??= $userService->getAuthenticatedToken($data);

                    $data['remoteUserData'] = $userService->getRemoteUserData(
                        $object,
                        $data,
                        $authenticatedToken
                    );
                }

                if (!array_key_exists('remoteUuid', $data)) {
                    $data['remoteUuid'] = $userService->getRemoteUuid($object, $data, $data['remoteUserData']);
                }

                /** @var UserClientService $userClientService */
                $userClientService = $this->applicationServicePluginManager->get(UserClientService::class);

                if (array_key_exists('userClientCollection', $data)) {
                    $userClientCollection = [];

                    foreach ($data['userClientCollection'] as $userClientData) {
                        if (is_array($userClientData)) {
                            $userClientCollection[] = $userClientService->hydrateDomainObject(
                                array_replace(
                                    $userClientData,
                                    [
                                        'user' => $object,
                                    ]
                                )
                            );
                        } elseif (is_int($userClientData)) {
                            $userClientCollection[] = $userClientService->getById($userClientData);
                        } elseif (is_string($userClientData)) {
                            $userClientCollection[] = $userClientService->getOneByUuid($userClientData);
                        } else {
                            $userClientCollection[] = $userClientData;
                        }
                    }

                    $data['userClientCollection'] = $userClientCollection;
                } elseif (isset($data['roleCollection']) || isset($data['roleCodeCollection'])) {
                    if (!isset($data['targetUserClientCollection'])) {
                        /** @var Token $authenticatedToken */
                        $authenticatedToken = $this->applicationServicePluginManager
                            ->get(AuthenticationService::class)
                            ->getAuthenticationService()
                            ->getAuthenticatedToken();
                        $authenticatedUserClient = $authenticatedToken->getUserClient();

                        $data['targetUserClientCollection'] = [$authenticatedUserClient];

                        if (isset($data['syncAllClients']) && $data['syncAllClients']) {
                            $data['targetUserClientCollection'] = $authenticatedUserClient->getUser()->getUserClientCollection();
                        }
                    }

                    $userClientCollection = [];

                    foreach ($data['targetUserClientCollection'] as $targetUserClient) {
                        /** @var UserClient $userClient */
                        $userClientCollection[] = $userClientService->hydrateDomainObject(
                            array_merge(
                                [
                                    'user' => $object,
                                    'client' => $targetUserClient->getClient(),
                                ],
                                isset($data['roleCollection'])
                                    ? [
                                        'roleCollection' => $data['roleCollection'],
                                    ]
                                    : [
                                        'roleCodeCollection' => $data['roleCodeCollection'],
                                    ]
                            ),
                            $object->getUserClientByUserAndClient(
                                $object,
                                $targetUserClient->getClient()
                            )
                        );
                    }

                    $data['userClientCollection'] = $userClientCollection;
                }

                if (array_key_exists('title', $data)) {
                    /** @var TitleService $titleService */
                    $titleService = $this->applicationServicePluginManager->get(TitleService::class);

                    if (is_string($data['title'])) {
                        $data['title'] = $titleService->getOneBy(['name' => $data['title']]);;
                    } elseif (is_int($data['title'])) {
                        $data['title'] = $titleService->getById($data['title']);
                    }
                }

                $data['genderIso5218'] = GenderIso5218Helper::NOT_SPECIFIED;

                if (array_key_exists('username', $data)) {
                    $data['username'] = strtolower($data['username']);
                }

                if (array_key_exists('email', $data)) {
                    $data['email'] = strtolower($data['email']);
                }

                if (isset($data['password']) && $data['password'] !== '') {
                    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                }

                return parent::hydrate($data, $object);
            }

            return $object;
        }

        return $object;
    }
}
