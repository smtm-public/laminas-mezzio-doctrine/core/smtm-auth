<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Http\Handler;

use Smtm\Auth\Context\User\Application\Extractor\UserExtractor;
use Smtm\Auth\Context\User\Application\Service\UserServiceInterface;
use Smtm\Base\Http\Handler\DbServiceEntity\UuidAware\AbstractIndexHandler;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class IndexHandler extends AbstractIndexHandler
{
    public ?string $applicationServiceName = UserServiceInterface::class;
    public ?string $domainObjectExtractorName = UserExtractor::class;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        $queryParams = $request->getQueryParams();
        $criteria = [];

        if (array_key_exists('criteria', $queryParams)) {
            $criteria = $queryParams['criteria'];
        }

        if (array_key_exists('role', $queryParams)) {
            $roleCriteria = [];

            if (is_array($queryParams['role'])) {
                $roleCriteria = ['or'];

                foreach ($queryParams['role'] as $roleCode) {
                    $roleCriteria[] = ['=', 'roleCollection.code', $roleCode];
                }
            } elseif (is_string($queryParams['role'])) {
                $roleCriteria[] = ['=', 'roleCollection.code', $queryParams['role']];
            }

            $criteria[] = $roleCriteria;
        }

        $this->setCriteria($criteria);

        return parent::handle($request);
    }
}
