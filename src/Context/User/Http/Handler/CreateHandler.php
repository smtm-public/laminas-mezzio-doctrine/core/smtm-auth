<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Http\Handler;

use Smtm\Auth\Context\User\Application\Extractor\UserExtractor;
use Smtm\Auth\Context\User\Application\Service\UserServiceInterface;
use Smtm\Base\Http\Handler\DbServiceEntity\UuidAware\AbstractCreateHandler;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CreateHandler extends AbstractCreateHandler
{
    public ?string $applicationServiceName = UserServiceInterface::class;
    public ?string $domainObjectExtractorName = UserExtractor::class;

    /**
     * @SWG\Post(path="/experimental/sso/sso/token",
     *   tags={"Token"},
     *   summary="Creates an SSO Token from auth code and return it back for further authentication",
     *   description="Creates access token by given authCode OR refreshToken",
     *   consumes={"application/json"},
     *   produces={"application/json"},
     *   @SWG\Response(
     *     response=201,
     *     description="Successfully saved data",
     *     @SWG\Schema(
     *       @SWG\Property(
     *              property="accessToken",
     *              type="string"
     *       ),
     *       @SWG\Property(
     *              property="refreshToken",
     *              type="string"
     *       ),
     *     )
     *   ),
     *   @SWG\Response(response=409, description="Invalid data"),
     *   security={{"Bearer":{}}}
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function handle(ServerRequestInterface $request): JsonResponse
    {
        return parent::handle($request);
    }

    protected static function parseQueryParams(array $queryParams = null): array
    {
        $parsedQueryParams = parent::parseQueryParams($queryParams);

        if (array_key_exists('syncRemote', $queryParams)) {
            $parsedQueryParams['syncRemote'] = $queryParams['syncRemote'];
        }

        if (array_key_exists('syncAllClients', $queryParams)) {
            $parsedQueryParams['syncAllClients'] = $queryParams['syncAllClients'];
        }

        return $parsedQueryParams;
    }
}
