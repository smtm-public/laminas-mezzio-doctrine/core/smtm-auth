<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Http\InputFilter;

use Smtm\Base\Http\InputFilter\AbstractRequestValidatingInputFilterCollection;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterSpecificationInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CreateHandlerRequestValidatingInputFilterCollection extends AbstractRequestValidatingInputFilterCollection
{
    protected array $parsedBodyInputFilterSpecification = [
        InputFilterSpecificationInterface::INPUT_COLLECTION => [
            [
                'name' => 'password',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'validator-password-auth',
                    ],
                ],
            ],
        ],
    ];
}
