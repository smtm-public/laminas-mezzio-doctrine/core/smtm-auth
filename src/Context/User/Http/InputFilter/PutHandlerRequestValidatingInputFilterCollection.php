<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\User\Http\InputFilter;

use Smtm\Base\Http\InputFilter\AbstractRequestValidatingInputFilterCollection;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterSpecificationInterface;
use Laminas\Validator\Uuid;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class PutHandlerRequestValidatingInputFilterCollection extends AbstractRequestValidatingInputFilterCollection
{
    protected array $pathParamsInputFilterSpecification = [
        InputFilterSpecificationInterface::INPUT_COLLECTION => [
            [
                'name' => 'uuid',
                'required' => true,
                'validators' => [
                    [
                        'name' => Uuid::class,
                    ],
                ],
            ],
        ],
    ];
    protected array $parsedBodyInputFilterSpecification = [
        InputFilterSpecificationInterface::INPUT_COLLECTION => [
            [
                'name' => 'password',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'validator-password-auth',
                    ],
                ],
            ],
        ],
    ];
}
