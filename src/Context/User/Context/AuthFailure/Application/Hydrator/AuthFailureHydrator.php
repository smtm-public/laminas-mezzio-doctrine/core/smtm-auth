<?php

namespace Smtm\Auth\Context\User\Context\AuthFailure\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class AuthFailureHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'user' => 'You must specify a User for the Auth failure.',
    ];
}
