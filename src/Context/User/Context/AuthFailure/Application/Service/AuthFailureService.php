<?php

namespace Smtm\Auth\Context\User\Context\AuthFailure\Application\Service;

use Smtm\Auth\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Auth\Context\User\Context\AuthFailure\Application\Hydrator\AuthFailureHydrator;
use Smtm\Auth\Context\User\Context\AuthFailure\Domain\AuthFailure;
use Smtm\Auth\Context\User\Domain\UserInterface;
use Smtm\Base\ConfigAwareTrait;

class AuthFailureService extends AbstractDbService
{
    use ConfigAwareTrait;

    protected ?string $domainObjectName = AuthFailure::class;
    protected ?string $hydratorName = AuthFailureHydrator::class;

    public function canRequestAuthentication(UserInterface $user): bool
    {
        if (!$this->config['userLockout']['enabled']) {
            return true;
        }

        $lockoutValidationTime = (new \DateTime())
            ->sub(new \DateInterval('PT' . $this->config['userLockout']['interval'] . 'M'));

        return $this->getCount([
                'user' => $user,
                ['>', 'created', $lockoutValidationTime]
            ]) < $this->config['userLockout']['maxFailures'];
    }

    public function clearFailures(UserInterface $user): void
    {
        $this->getRepository()->removeByUserId($user->getId());
    }
}
