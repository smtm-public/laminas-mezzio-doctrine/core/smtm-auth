<?php

namespace Smtm\Auth\Context\User\Context\AuthFailure\Domain;

use Smtm\Auth\Context\User\Domain\User;
use Smtm\Base\Domain\AbstractIdAwareEntity;
use Smtm\Base\Domain\CreatedDateTimeImmutableAwareEntityTrait;

class AuthFailure extends AbstractIdAwareEntity
{
    use CreatedDateTimeImmutableAwareEntityTrait;

    protected User $user;

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
