<?php

namespace Smtm\Auth\Context\User\Context\AuthFailure\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

class AuthFailureRepository extends AbstractRepository implements AuthFailureRepositoryInterface
{
    public function removeByUserId(int $userId): void
    {
        $this->getEntityManager()->getConnection()->delete(
            'auth_auth_failure',
            ['auth_user_id' => $userId]
        );
    }
}
