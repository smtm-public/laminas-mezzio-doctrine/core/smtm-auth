<?php

namespace Smtm\Auth\Context\User\Context\AuthFailure\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

interface AuthFailureRepositoryInterface extends RepositoryInterface
{

}
