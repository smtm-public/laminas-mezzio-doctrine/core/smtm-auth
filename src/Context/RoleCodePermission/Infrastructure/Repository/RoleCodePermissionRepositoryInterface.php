<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\RoleCodePermission\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RoleCodePermissionRepositoryInterface extends RepositoryInterface
{

}
