<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\RoleCodePermission\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleCodePermissionRepository extends AbstractRepository implements
    RoleCodePermissionRepositoryInterface
{

}
