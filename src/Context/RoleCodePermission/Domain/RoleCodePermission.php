<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\RoleCodePermission\Domain;

use Smtm\Auth\Context\Permission\Domain\Permission;
use Smtm\Base\Domain\EntityInterface;
use Smtm\Base\Domain\EntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use JetBrains\PhpStorm\Pure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleCodePermission implements EntityInterface, MarkedForUpdateInterface
{

    use EntityTrait, MarkedForUpdateTrait;

    protected string $roleCode;
    protected int $permissionId;
    protected Permission $permission;

    public function getRoleCode(): string
    {
        return $this->roleCode;
    }

    public function setRoleCode(string $roleCode): static
    {
        return $this->__setProperty('roleCode', $roleCode);
    }

    #[Pure] public function getPermissionId(): int
    {
        return $this->permission->getId();
    }

    public function getPermission(): Permission
    {
        return $this->permission;
    }

    public function setPermission(Permission $permission): static
    {
        $this->__setProperty('permission', $permission);

        return $this->__setProperty('permissionId', $permission->getId());
    }
}
