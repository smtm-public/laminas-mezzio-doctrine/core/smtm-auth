<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\RoleCodePermission\Application\Service;

use Smtm\Base\Application\Service\ApplicationServiceInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RoleCodePermissionServiceInterface extends ApplicationServiceInterface
{

}
