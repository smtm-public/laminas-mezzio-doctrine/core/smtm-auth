<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\RoleCodePermission\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleCodePermissionReaderService extends RoleCodePermissionService implements
    RoleCodePermissionReaderServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_READER_MANAGER_NAME;
}
