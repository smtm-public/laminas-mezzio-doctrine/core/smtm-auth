<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\RoleCodePermission\Application\Service;

use Smtm\Auth\Context\RoleCodePermission\Domain\RoleCodePermission;
use Smtm\Base\Application\Service\ApplicationService\DbService\AbstractEntityManagerConfigAwareDbService;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RoleCodePermissionService extends AbstractEntityManagerConfigAwareDbService implements
    RoleCodePermissionServiceInterface
{
    protected ?string $domainObjectName = RoleCodePermission::class;
}
