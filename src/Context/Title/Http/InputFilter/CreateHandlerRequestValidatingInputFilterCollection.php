<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Title\Http\InputFilter;

use Smtm\Base\Http\InputFilter\AbstractRequestValidatingInputFilterCollection;
use Smtm\Base\Infrastructure\Laminas\InputFilter\InputFilterSpecificationInterface;
use Smtm\Base\Infrastructure\Laminas\Validator\IsInteger;
use Smtm\Base\Infrastructure\Laminas\Validator\IsString;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class CreateHandlerRequestValidatingInputFilterCollection extends AbstractRequestValidatingInputFilterCollection
{
    protected array $parsedBodyInputFilterSpecification = [
        InputFilterSpecificationInterface::INPUT_COLLECTION => [
            [
                'name' => 'r_name',
                'required' => true,
                'validators' => [
                    [
                        'name' => IsString::class,
                        'options' => [
                            'minLength' => 1,
                            'maxLength' => 255,
                        ]
                    ],
                ],
            ],
            [
                'name' => 'r_order',
                'required' => true,
                'validators' => [
                    [
                        'name' => IsInteger::class,
                    ],
                ],
            ],
        ],
    ];
}
