<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Title\Domain;

use Smtm\Base\Domain\AbstractUuidAwareEntity;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait;
use Smtm\Base\Domain\MarkedForUpdateInterface;
use Smtm\Base\Domain\MarkedForUpdateTrait;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Title extends AbstractUuidAwareEntity implements
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface,
    MarkedForUpdateInterface
{

    use NotArchivedAwareEntityTrait,
        ArchivedDateTimeAwareEntityTrait,
        CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityTrait,
        MarkedForUpdateTrait;

    protected string $name;
    protected int $order;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        return $this->__setProperty('name', $name);
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function setOrder(int $order): static
    {
        return $this->__setProperty('order', $order);
    }
}
