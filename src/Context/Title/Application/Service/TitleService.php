<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Title\Application\Service;

use Smtm\Auth\Context\Title\Application\Hydrator\TitleHydrator;
use Smtm\Auth\Context\Title\Domain\Title;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\AbstractEntityManagerConfigAwareAndUuidAwareEntityDbService;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\ArchivableUuidAwareEntityDbServiceTrait;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TitleService extends AbstractEntityManagerConfigAwareAndUuidAwareEntityDbService
{

    use ArchivableUuidAwareEntityDbServiceTrait;

    protected ?string $domainObjectName = Title::class;
    protected ?string $hydratorName = TitleHydrator::class;
}
