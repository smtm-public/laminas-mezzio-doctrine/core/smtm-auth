<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Title\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\DbReaderServiceInterface;
use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TitleReaderService extends TitleService implements DbReaderServiceInterface
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_READER_MANAGER_NAME;
}
