<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Title\Application\Service;

use Smtm\Base\Infrastructure\Doctrine\Persistence\ManagerRegistryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TitleArchivedAccessService extends TitleService
{
    protected ?string $entityManagerName = ManagerRegistryInterface::DEFAULT_ARCHIVED_ACCESS_MANAGER_NAME;
}
