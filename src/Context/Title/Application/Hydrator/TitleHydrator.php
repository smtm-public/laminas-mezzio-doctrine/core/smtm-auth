<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Title\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TitleHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'name' => 'You must specify a name for the Title.',
        'order' => 'You must specify the order of the Title.',
    ];
}
