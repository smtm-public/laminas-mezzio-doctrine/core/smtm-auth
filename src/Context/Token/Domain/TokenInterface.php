<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Token\Domain;

use DateTimeImmutable;
use Smtm\Auth\Context\UserClient\Domain\UserClient;
use Smtm\Base\Domain\ArchivedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface;
use Smtm\Base\Domain\IdAwareEntityInterface;
use Smtm\Base\Domain\NotArchivedAwareEntityInterface;
use Smtm\Base\Domain\UuidAwareEntityInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface TokenInterface extends
    IdAwareEntityInterface,
    UuidAwareEntityInterface,
    CreatedDateTimeImmutableAndModifiedDateTimeAwareEntityInterface,
    NotArchivedAwareEntityInterface,
    ArchivedDateTimeAwareEntityInterface
{
    public function getProviderUuid(): string;

    public function setProviderUuid(string $providerUuid): static;

    public function getUserClient(): UserClient;

    public function setUserClient(UserClient $userClient): static;

    public function getIdToken(): ?string;

    public function setIdToken(?string $idToken): static;

    public function getAccessToken(): string;

    public function setAccessToken(string $accessToken): static;

    public function getRefreshToken(): ?string;

    public function setRefreshToken(?string $refreshToken): static;

    public function getExpires(): DateTimeImmutable;

    public function setExpires(DateTimeImmutable $expires): static;

    public function getRefreshTokenExpires(): ?DateTimeImmutable;

    public function setRefreshTokenExpires(?DateTimeImmutable $refreshTokenExpires): static;

    public function getTokenType(): ?string;

    public function setTokenType(?string $tokenType): static;

    public function getScope(): ?string;

    public function setScope(?string $scope): static;

    public function getUsed(): bool;

    public function setUsed(bool $used): static;

    public function getRevoked(): bool;

    public function setRevoked(bool $revoked): static;
}
