<?php

declare(strict_types=1);

namespace Smtm\Auth\Context\Token\Exception;

use Smtm\Base\Application\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TokenNotFoundException extends RuntimeException
{
    protected $message = 'Token not found';
}
