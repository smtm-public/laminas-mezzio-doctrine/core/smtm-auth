<?php

declare(strict_types=1);

namespace Smtm\Auth\Infrastructure\Doctrine\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait AuthRoleTableAndAuthPermissionTableAndAuthRoleCodePermissionTablePopulatingMigrationTrait
{
    protected array $role = [];
    protected array $permission = [];
    protected array $rolePermission = [];

    public function populateAuthRoleTable(Connection $connection, Schema $schema): array
    {
        $role = [];

        foreach ($this->role as $roleCode => $roleData) {
            $connection->insert('auth_role', $roleData);
            $role[$roleCode]['id'] = $connection->lastInsertId();
        }

        return $role;
    }

    public function populateAuthPermissionTable(Connection $connection, Schema $schema): array
    {
        $permission = [];

        foreach ($this->permission as $permissionName => $permissionData) {
            $connection->insert('auth_permission', $permissionData);
            $permission[$permissionName]['id'] = $connection->lastInsertId();
        }

        return $permission;
    }

    public function populateAuthRoleCodePermissionTable(
        Connection $connection,
        Schema $schema
    ): void {
        foreach ($this->rolePermission as $roleCode => $permissionNames) {
            foreach ($permissionNames as $permissionName) {
                $sql = 'SELECT id FROM auth_permission WHERE r_name=' . $connection->quote($permissionName);
                $permissionId = $this->connection->fetchOne($sql);
                $this->abortIf(!$permissionId, 'Permission with name \'' . $permissionName . '\' not found.');

                $connection->insert('auth_role_code_permission', [
                    'auth_role_code' => $roleCode,
                    'auth_permission_id' => $permissionId,
                ]);
            }
        }
    }
}
