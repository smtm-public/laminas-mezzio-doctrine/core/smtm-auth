<?php

declare(strict_types=1);

namespace Smtm\Auth;

return [
    'em' => 'default',

    'table_storage' => [
        'table_name' => 'doctrine_migrations_smtm_auth',
    ],

    'migrations_paths' => [
        'Smtm\Auth\Migration' => __DIR__ . '/../data/db/migrations',
    ],
];
