<?php

declare(strict_types=1);

namespace Smtm\Auth;

return [
    Command\GenerateClientCredentials::class,
    Command\GeneratePkce::class,
    Command\HelloWorld::class,
];
