<?php

declare(strict_types=1);

namespace Smtm\AuthProvider;

use Smtm\Auth\Application\Service\ApplicationService\DbService\Factory\AuthEntityManagerConfigAwareDbServiceFactory;
use Smtm\Auth\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\Factory\AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory;
use Smtm\Auth\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\Factory\AuthEntityManagerConfigAwareAndUuidAwareEntityDbReaderServiceFactory;
use Smtm\Auth\Application\Service\ApplicationService\DbService\UuidAwareEntityDbService\Factory\AuthEntityManagerConfigAwareAndUuidAwareEntityDbServiceFactory;
use Smtm\Auth\Authentication\Application\Service\AuthenticationServicePluginManager;
use Smtm\Auth\Authentication\Application\Service\Cli\CliAuthenticationServiceInterface;
use Smtm\Auth\Authentication\Application\Service\Cli\CliSystemUserAuthenticationService;
use Smtm\Auth\Authentication\Application\Service\Factory\AuthenticationServiceAbstractFactory;
use Smtm\Auth\Authentication\Http\Middleware\AuthenticationMiddleware;
use Smtm\Auth\Authorization\Http\Middleware\AuthorizationMiddleware;
use Smtm\Auth\Command\Factory\CommandAbstractFactory;
use Smtm\Auth\Context\Client\Application\Service\ClientArchivedAccessReaderService;
use Smtm\Auth\Context\Client\Application\Service\ClientArchivedAccessReaderServiceInterface;
use Smtm\Auth\Context\Client\Application\Service\ClientArchivedAccessService;
use Smtm\Auth\Context\Client\Application\Service\ClientArchivedAccessServiceInterface;
use Smtm\Auth\Context\Client\Application\Service\ClientReaderService;
use Smtm\Auth\Context\Client\Application\Service\ClientReaderServiceInterface;
use Smtm\Auth\Context\Client\Application\Service\ClientService;
use Smtm\Auth\Context\Client\Application\Service\ClientServiceInterface;
use Smtm\Auth\Context\Permission\Application\Service\PermissionArchivedAccessReaderService;
use Smtm\Auth\Context\Permission\Application\Service\PermissionArchivedAccessReaderServiceInterface;
use Smtm\Auth\Context\Permission\Application\Service\PermissionArchivedAccessService;
use Smtm\Auth\Context\Permission\Application\Service\PermissionArchivedAccessServiceInterface;
use Smtm\Auth\Context\Permission\Application\Service\PermissionReaderService;
use Smtm\Auth\Context\Permission\Application\Service\PermissionReaderServiceInterface;
use Smtm\Auth\Context\Permission\Application\Service\PermissionService;
use Smtm\Auth\Context\Permission\Application\Service\PermissionServiceInterface;
use Smtm\Auth\Context\Role\Application\Service\RoleArchivedAccessReaderService;
use Smtm\Auth\Context\Role\Application\Service\RoleArchivedAccessReaderServiceInterface;
use Smtm\Auth\Context\Role\Application\Service\RoleArchivedAccessService;
use Smtm\Auth\Context\Role\Application\Service\RoleArchivedAccessServiceInterface;
use Smtm\Auth\Context\Role\Application\Service\RoleReaderService;
use Smtm\Auth\Context\Role\Application\Service\RoleReaderServiceInterface;
use Smtm\Auth\Context\Role\Application\Service\RoleService;
use Smtm\Auth\Context\Role\Application\Service\RoleServiceInterface;
use Smtm\Auth\Context\RoleCodePermission\Application\Service\RoleCodePermissionReaderService;
use Smtm\Auth\Context\RoleCodePermission\Application\Service\RoleCodePermissionReaderServiceInterface;
use Smtm\Auth\Context\RoleCodePermission\Application\Service\RoleCodePermissionService;
use Smtm\Auth\Context\RoleCodePermission\Application\Service\RoleCodePermissionServiceInterface;
use Smtm\Auth\Context\Title\Application\Service\TitleArchivedAccessService;
use Smtm\Auth\Context\Title\Application\Service\TitleReaderService;
use Smtm\Auth\Context\Title\Application\Service\TitleService;
use Smtm\Auth\Context\User\Application\Service\UserArchivedAccessReaderService;
use Smtm\Auth\Context\User\Application\Service\UserArchivedAccessReaderServiceInterface;
use Smtm\Auth\Context\User\Application\Service\UserArchivedAccessService;
use Smtm\Auth\Context\User\Application\Service\UserArchivedAccessServiceInterface;
use Smtm\Auth\Context\User\Application\Service\UserReaderService;
use Smtm\Auth\Context\User\Application\Service\UserReaderServiceInterface;
use Smtm\Auth\Context\User\Application\Service\UserService;
use Smtm\Auth\Context\User\Application\Service\UserServiceInterface;
use Smtm\Auth\Context\User\Context\AuthFailure\Application\Service\AuthFailureService;
use Smtm\Auth\Factory\AuthConfigAwareDelegator;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Laminas\Validator\PasswordValidator;
use Smtm\Base\Infrastructure\Service\Log\Factory\LoggerAwareDelegator;
use Laminas\Validator\ValidatorPluginManager;
use Psr\Container\ContainerInterface;

return [
    'abstract_factories' => [
        CommandAbstractFactory::class,
    ],

    'factories' => [
        AuthenticationServicePluginManager::class => function (ContainerInterface $container, $requestedName) {
            return new AuthenticationServicePluginManager(
                $container,
                [
                    'abstract_factories' => [
                        AuthenticationServiceAbstractFactory::class,
                    ],
                    'aliases' => [
                        CliAuthenticationServiceInterface::class => CliSystemUserAuthenticationService::class,
                    ],
                ]
            );
        }
    ],

    'delegators' => [
        \Smtm\Auth\Command\GenerateClientCredentials::class => [
            (new LoggerAwareDelegator())->setLoggerName('logger-console'),
        ],
        \Smtm\Auth\Command\GeneratePkce::class => [
            (new LoggerAwareDelegator())->setLoggerName('logger-console'),
        ],

        AuthenticationMiddleware::class => [
            AuthConfigAwareDelegator::class,
        ],
        AuthorizationMiddleware::class => [
            AuthConfigAwareDelegator::class,
        ],


        ApplicationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var ApplicationServicePluginManager $applicationServicePluginManager */
                $applicationServicePluginManager = $callback();

                // Write/Read
                $applicationServicePluginManager->setFactory(
                    ClientService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    ClientServiceInterface::class,
                    ClientService::class
                );
                $applicationServicePluginManager->setFactory(
                    TitleService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbServiceFactory::class
                );
//                $applicationServicePluginManager->setFactory(
//                    UserService::class,
//                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbServiceFactory::class
//                );
                $applicationServicePluginManager->addDelegator(
                    UserService::class,
                    AuthConfigAwareDelegator::class
                );
                $applicationServicePluginManager->setAlias(
                    UserServiceInterface::class,
                    UserService::class
                );
                $applicationServicePluginManager->addDelegator(
                    AuthFailureService::class,
                    AuthConfigAwareDelegator::class
                );


                $applicationServicePluginManager->setFactory(
                    PermissionService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    PermissionServiceInterface::class,
                    PermissionService::class
                );
//                $applicationServicePluginManager->setFactory(
//                    RoleService::class,
//                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbServiceFactory::class
//                );
                $applicationServicePluginManager->setAlias(
                    RoleServiceInterface::class,
                    RoleService::class
                );
                $applicationServicePluginManager->setFactory(
                    RoleCodePermissionService::class,
                    AuthEntityManagerConfigAwareDbServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    RoleCodePermissionServiceInterface::class,
                    RoleCodePermissionService::class
                );

                // Reader service
                $applicationServicePluginManager->setFactory(
                    ClientReaderService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbReaderServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    ClientReaderServiceInterface::class,
                    ClientReaderService::class
                );
                $applicationServicePluginManager->setFactory(
                    TitleReaderService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbReaderServiceFactory::class
                );
//                $applicationServicePluginManager->setFactory(
//                    UserReaderService::class,
//                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbReaderServiceFactory::class
//                );
                $applicationServicePluginManager->setAlias(
                    UserReaderServiceInterface::class,
                    UserReaderService::class
                );
                $applicationServicePluginManager->setFactory(
                    PermissionReaderService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbReaderServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    PermissionReaderServiceInterface::class,
                    PermissionReaderService::class
                );
//                $applicationServicePluginManager->setFactory(
//                    RoleReaderService::class,
//                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbReaderServiceFactory::class
//                );
                $applicationServicePluginManager->setAlias(
                    RoleReaderServiceInterface::class,
                    RoleReaderService::class
                );
                $applicationServicePluginManager->setFactory(
                    RoleCodePermissionReaderService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbReaderServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    RoleCodePermissionReaderServiceInterface::class,
                    RoleCodePermissionReaderService::class
                );

                // Archived access service
                $applicationServicePluginManager->setFactory(
                    ClientArchivedAccessService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    ClientArchivedAccessServiceInterface::class,
                    ClientArchivedAccessService::class
                );
                $applicationServicePluginManager->setFactory(
                    TitleArchivedAccessService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory::class
                );
//                $applicationServicePluginManager->setFactory(
//                    UserArchivedAccessService::class,
//                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory::class
//                );
                $applicationServicePluginManager->setAlias(
                    UserArchivedAccessServiceInterface::class,
                    UserArchivedAccessService::class
                );
                $applicationServicePluginManager->setFactory(
                    PermissionArchivedAccessService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    PermissionArchivedAccessServiceInterface::class,
                    PermissionArchivedAccessService::class
                );
//                $applicationServicePluginManager->setFactory(
//                    RoleArchivedAccessService::class,
//                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory::class
//                );
                $applicationServicePluginManager->setAlias(
                    RoleArchivedAccessServiceInterface::class,
                    RoleArchivedAccessService::class
                );

                $applicationServicePluginManager->setFactory(
                    ClientArchivedAccessReaderService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    ClientArchivedAccessReaderServiceInterface::class,
                    ClientArchivedAccessReaderService::class
                );
//                $applicationServicePluginManager->setFactory(
//                    UserArchivedAccessReaderService::class,
//                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory::class
//                );
                $applicationServicePluginManager->setAlias(
                    UserArchivedAccessReaderServiceInterface::class,
                    UserArchivedAccessReaderService::class
                );
                $applicationServicePluginManager->setFactory(
                    PermissionArchivedAccessReaderService::class,
                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory::class
                );
                $applicationServicePluginManager->setAlias(
                    PermissionArchivedAccessReaderServiceInterface::class,
                    PermissionArchivedAccessReaderService::class
                );
//                $applicationServicePluginManager->setFactory(
//                    RoleArchivedAccessReaderService::class,
//                    AuthEntityManagerConfigAwareAndUuidAwareEntityDbArchivedAccessServiceFactory::class
//                );
                $applicationServicePluginManager->setAlias(
                    RoleArchivedAccessReaderServiceInterface::class,
                    RoleArchivedAccessReaderService::class
                );

                return $applicationServicePluginManager;
            }
        ],

        ValidatorPluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var ValidatorPluginManager $validatorPluginManager */
                $validatorPluginManager = $callback();
                $config = $container->get('config')['auth'];

                $validatorPluginManager->setFactory(
                    'validator-password-auth',
                    function (ContainerInterface $container, $requestedName, ?array $options = null) use ($validatorPluginManager, $config) {
                        return $validatorPluginManager->build(
                            PasswordValidator::class,
                            $config['passwordValidationRules']
                        );
                    }
                );

                return $validatorPluginManager;
            }
        ],
    ],
];
