<?php

declare(strict_types=1);

namespace Smtm\Auth;

use Smtm\Auth\Authentication\Http\Middleware\AuthenticationMiddleware;
use Smtm\Auth\Authorization\Http\Middleware\AuthorizationMiddleware;

return [
    [
        'priority' => 820,
        'middleware' => AuthenticationMiddleware::class,
    ],
    [
        'priority' => 840,
        'middleware' => AuthorizationMiddleware::class,
    ],
];
