<?php

declare(strict_types=1);

namespace Smtm\Auth;

return [
    'orm' => [
        'query' => [
            'filters' => [
            ],
        ],
        'mapping' => [
            'paths' => [
                \Smtm\Auth\Context\Client\Domain\Client::class =>
                    __DIR__ . '/../src/Context/Client/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\Auth\Context\Client\Context\RedirectUri\Domain\RedirectUri::class =>
                    __DIR__ . '/../src/Context/Client/Context/RedirectUri/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\Auth\Context\Permission\Domain\Permission::class =>
                    __DIR__ . '/../src/Context/Permission/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\Auth\Context\Role\Domain\Role::class =>
                    __DIR__ . '/../src/Context/Role/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\Auth\Context\RoleCodePermission\Domain\RoleCodePermission::class =>
                    __DIR__ . '/../src/Context/RoleCodePermission/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\Auth\Context\Title\Domain\Title::class =>
                    __DIR__ . '/../src/Context/Title/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\Auth\Context\User\Domain\User::class =>
                    __DIR__ . '/../src/Context/User/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\Auth\Context\User\Context\AuthFailure\Domain\AuthFailure::class =>
                    __DIR__ . '/../src/Context/User/Context/AuthFailure/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\Auth\Context\UserClient\Domain\UserClient::class =>
                    __DIR__ . '/../src/Context/UserClient/Infrastructure/Doctrine/Orm/Mapping',
                \Smtm\Auth\Context\UserClientRole\Domain\UserClientRole::class =>
                    __DIR__ . '/../src/Context/UserClientRole/Infrastructure/Doctrine/Orm/Mapping',
            ],
        ],
    ],
];
