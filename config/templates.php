<?php

declare(strict_types=1);

namespace Smtm\Auth;

return [
    'paths' => [
        'smtm-auth-email'  => [__DIR__ . '/../template/email'],
        'smtm-auth-error'  => [__DIR__ . '/../template/error'],
        'smtm-auth-layout' => [__DIR__ . '/../template/layout'],
        'smtm-auth-page'    => [__DIR__ . '/../template/page'],
    ],
];
