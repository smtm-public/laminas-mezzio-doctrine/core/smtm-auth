<?php

declare(strict_types=1);

namespace Smtm\AuthProvider;

use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Laminas\Mime\Mime;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-auth')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-auth');
    $dotenv->load();
}

!defined('AUTH_DEFAULT_USER_LOCKOUT_INTERVAL') && define('AUTH_DEFAULT_USER_LOCKOUT_INTERVAL', 15);
!defined('AUTH_DEFAULT_USER_LOCKOUT_MAX_FAILURES') && define('AUTH_DEFAULT_USER_LOCKOUT_MAX_FAILURES', 5);

$passwordValidationRules = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_USER_PASSWORD_VALIDATION_RULES'),
    true,
    flags: JSON_THROW_ON_ERROR
);

return [
    'defaultSystemUserId' => (int) EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_DEFAULT_SYSTEM_USER_ID'),
    'entityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_ENTITY_MANAGER_NAME'),
    'readerEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_READER_ENTITY_MANAGER_NAME'),
    'archivedAccessEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_ARCHIVED_ACCESS_ENTITY_MANAGER_NAME'),
    'archivedAccessReaderEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_ARCHIVED_ACCESS_READER_ENTITY_MANAGER_NAME'),
    'enableAuthenticationAndAuthorization' => filter_var(
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_ENABLE_AUTHENTICATION_AND_AUTHORIZATION'),
        FILTER_VALIDATE_BOOLEAN
    ),
    'enableAuthentication' => filter_var(
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_ENABLE_AUTHENTICATION'),
        FILTER_VALIDATE_BOOLEAN
    ),
    'enableAuthorization' => filter_var(
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_ENABLE_AUTHORIZATION'),
        FILTER_VALIDATE_BOOLEAN
    ),
    'clientOptions' => json_decode(
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CLIENT_OPTIONS'),
        true,
        flags: JSON_THROW_ON_ERROR
    ),
    'userLockout' => [
        'enabled' => EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_AUTH_USER_LOCKOUT_ENABLED',
            0
        ),
        'interval' => EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_AUTH_USER_LOCKOUT_INTERVAL',
            AUTH_DEFAULT_USER_LOCKOUT_INTERVAL,
            EnvHelper::TYPE_INT
        ),
        'maxFailures' => EnvHelper::getEnvFromProcessOrSuperGlobal(
            'SMTM_AUTH_USER_LOCKOUT_MAX_FAILURES',
            AUTH_DEFAULT_USER_LOCKOUT_MAX_FAILURES,
            EnvHelper::TYPE_INT
        ),
    ],
    'passwordValidationRules' => $passwordValidationRules,
    'email' => [
        'layoutTemplates' => [
            Mime::TYPE_HTML => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_LAYOUT_TEMPLATE_HTML'),
            Mime::TYPE_TEXT => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_LAYOUT_TEMPLATE_TEXT'),
        ],
        'messages' => [
            'userCreated' => [
                'from' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_USER_CREATED_FROM'),
                'templateSubject' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_USER_CREATED_TEMPLATE_SUBJECT'),
                'templateContent' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_USER_CREATED_TEMPLATE_CONTENT'),
                'returnUri' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_USER_CREATED_RETURN_URI'),
            ],
            'passwordReset' => [
                'from' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_PASSWORD_RESET_FROM'),
                'templateSubject' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_PASSWORD_RESET_TEMPLATE_SUBJECT'),
                'templateContent' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_PASSWORD_RESET_TEMPLATE_CONTENT'),
                'returnUri' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EMAIL_PASSWORD_RESET_RETURN_URI'),
            ],
        ]
    ],
];
