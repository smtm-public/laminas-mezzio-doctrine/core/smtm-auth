<?php

declare(strict_types=1);

namespace Smtm\Auth;

use Smtm\Auth\Authentication\Application\Service\Basic\BasicUserAuthenticationService;
use Smtm\Auth\Authorization\Application\Service\AuthorizationService;
use Smtm\AuthProvider\Authentication\Application\Service\Bearer\BearerAuthenticationService;
use Smtm\Base\Http\InputFilter\UuidRouteParamRequestValidatingInputFilterCollection;
use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-auth')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-auth'
    );
    $dotenv->load();
}

$exposedRoutes = json_decode(EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_EXPOSED_ROUTES'));
$exposedRoutes = array_combine($exposedRoutes, $exposedRoutes);

$routes = [
    'smtm.auth.title.create' => [
        'path' => '/auth/title',
        'method' => 'post',
        'middleware' => Context\Title\Http\Handler\CreateHandler::class,
        'options' => [
            'authentication' => [

            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\Title\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.title.read' => [
        'path' => '/auth/title/{uuid}',
        'method' => 'get',
        'middleware' => Context\Title\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\Title\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.title.put' => [
        'path' => '/auth/title/{uuid}',
        'method' => 'put',
        'middleware' => Context\Title\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\Title\Http\InputFilter\UpdateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.title.patch' => [
        'path' => '/auth/title/{uuid}',
        'method' => 'patch',
        'middleware' => Context\Title\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\Title\Http\InputFilter\UpdateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.title.delete' => [
        'path' => '/auth/title/{uuid}',
        'method' => 'delete',
        'middleware' => Context\Title\Http\Handler\DeleteHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.title.index' => [
        'path' => '/auth/title',
        'method' => 'get',
        'middleware' => Context\Title\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' => null,
            /*
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\Title\Http\InputFilter\IndexHandlerRequestValidatingInputFilterCollection::class,
            */
        ],
    ],

    'smtm.auth.role.create' => [
        'path' => '/auth/role',
        'method' => 'post',
        'middleware' => Context\Role\Http\Handler\CreateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
//            'requestValidatingInputFilterCollectionName' =>
//                Context\User\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.role.read' => [
        'path' => '/auth/role/{uuid}',
        'method' => 'get',
        'middleware' => Context\Role\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.role.put' => [
        'path' => '/auth/role/{uuid}',
        'method' => 'put',
        'middleware' => Context\Role\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
//            'requestValidatingInputFilterCollectionName' =>
//                Context\Role\Http\InputFilter\PutHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.role.patch' => [
        'path' => '/auth/role/{uuid}',
        'method' => 'patch',
        'middleware' => Context\Role\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
//            'requestValidatingInputFilterCollectionName' =>
//                Context\Role\Http\InputFilter\PatchHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.role.delete' => [
        'path' => '/auth/role/{uuid}',
        'method' => 'delete',
        'middleware' => Context\Role\Http\Handler\DeleteHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.role.index' => [
        'path' => '/auth/role',
        'method' => 'get',
        'middleware' => Context\Role\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' => null,
            /*
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\User\Http\InputFilter\IndexHandlerRequestValidatingInputFilterCollection::class,
            */
        ],
    ],

    'smtm.auth.permission.create' => [
        'path' => '/auth/permission',
        'method' => 'post',
        'middleware' => Context\Permission\Http\Handler\CreateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
//            'requestValidatingInputFilterCollectionName' =>
//                Context\User\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.permission.read' => [
        'path' => '/auth/permission/{uuid}',
        'method' => 'get',
        'middleware' => Context\Permission\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.permission.put' => [
        'path' => '/auth/permission/{uuid}',
        'method' => 'put',
        'middleware' => Context\Permission\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
//            'requestValidatingInputFilterCollectionName' =>
//                Context\Permission\Http\InputFilter\PutHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.permission.patch' => [
        'path' => '/auth/permission/{uuid}',
        'method' => 'patch',
        'middleware' => Context\Permission\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
//            'requestValidatingInputFilterCollectionName' =>
//                Context\Permission\Http\InputFilter\PatchHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.permission.delete' => [
        'path' => '/auth/permission/{uuid}',
        'method' => 'delete',
        'middleware' => Context\Permission\Http\Handler\DeleteHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.permission.index' => [
        'path' => '/auth/permission',
        'method' => 'get',
        'middleware' => Context\Permission\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' => null,
            /*
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\User\Http\InputFilter\IndexHandlerRequestValidatingInputFilterCollection::class,
            */
        ],
    ],

    'smtm.auth.user.create' => [
        'path' => '/auth/user',
        'method' => 'post',
        'middleware' => Context\User\Http\Handler\CreateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\User\Http\InputFilter\CreateHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.user.read' => [
        'path' => '/auth/user/{uuid}',
        'method' => 'get',
        'middleware' => Context\User\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.user.put' => [
        'path' => '/auth/user/{uuid}',
        'method' => 'put',
        'middleware' => Context\User\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\User\Http\InputFilter\PutHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.user.patch' => [
        'path' => '/auth/user/{uuid}',
        'method' => 'patch',
        'middleware' => Context\User\Http\Handler\UpdateHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                Context\User\Http\InputFilter\PatchHandlerRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.user.delete' => [
        'path' => '/auth/user/{uuid}',
        'method' => 'delete',
        'middleware' => Context\User\Http\Handler\DeleteHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' =>
                UuidRouteParamRequestValidatingInputFilterCollection::class,
        ],
    ],
    'smtm.auth.user.index' => [
        'path' => '/auth/user',
        'method' => 'get',
        'middleware' => Context\User\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BASIC => [
                        'name' => BasicUserAuthenticationService::class,
                    ],
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' => null,
            /*
            'requestValidatingInputFilterCollectionName' =>
                Context\Authentication\Context\User\Http\InputFilter\IndexHandlerRequestValidatingInputFilterCollection::class,
            */
        ],
    ],
    'smtm.auth.authenticated-user-client.read' => [
        'path' => '/auth/authenticated-user-client',
        'method' => 'get',
        'middleware' => Context\AuthenticatedUserClient\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
    'smtm.auth.client.index' => [
        'path' => '/auth/client',
        'method' => 'get',
        'middleware' => Context\Client\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => [
                'services' => [
                    HttpHelper::AUTHENTICATION_BEARER => [
                        'name' => BearerAuthenticationService::class,
                    ],
                ],
            ],
            'authorization' => [
                'services' => [
                    [
                        'name' => AuthorizationService::class,
                    ],
                ]
            ],
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
];

return array_intersect_key($routes, $exposedRoutes);
