<?php

declare(strict_types=1);

namespace Smtm\Auth\Migration;

use Doctrine\DBAL\Driver\PDO\SQLite\Driver as SQLiteDriver;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201202120001 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add triggers to the `auth_user` table';
    }

    public function up(Schema $schema): void
    {
        if ($this->connection->getDriver() instanceof SQLiteDriver) {
            $this->addSql(
                <<< EOT
                CREATE TRIGGER trg_auth_user_full_name_insert AFTER INSERT ON auth_user
                FOR EACH ROW
                BEGIN
                    UPDATE auth_user SET full_name = NEW.first_name || ' ' || NEW.last_name WHERE id = NEW.id;
                END;
                EOT
            );
            $this->addSql(
                <<< EOT
                CREATE TRIGGER trg_auth_user_full_name_update AFTER UPDATE ON auth_user
                    FOR EACH ROW
                BEGIN
                    UPDATE auth_user SET full_name = NEW.first_name || ' ' || NEW.last_name WHERE id = NEW.id AND (OLD.first_name <> NEW.first_name OR OLD.last_name <> NEW.last_name);
                END;
                EOT
            );
            $this->addSql(
                <<< EOT
                CREATE TRIGGER trg_auth_user_email_username_insert AFTER INSERT ON auth_user
                FOR EACH ROW
                BEGIN
                    UPDATE auth_user SET email_username = SUBSTR(NEW.email, 0, INSTR(NEW.email, '@')) WHERE id = NEW.id AND NEW.email IS NOT NULL;
                    UPDATE auth_user SET email_username = NULL WHERE id = NEW.id AND NEW.email IS NULL;
                END;
                EOT
            );
            $this->addSql(
                <<< EOT
                CREATE TRIGGER trg_auth_user_email_username_update AFTER UPDATE ON auth_user
                FOR EACH ROW
                BEGIN
                    UPDATE auth_user SET email_username = SUBSTR(NEW.email, 0, INSTR(NEW.email, '@')) WHERE id = NEW.id AND OLD.email <> NEW.email AND NEW.email IS NOT NULL;
                    UPDATE auth_user SET email_username = NULL WHERE OLD.email <> NEW.email AND NEW.email IS NULL;
                END;
                EOT
            );
            $this->addSql(
                <<< EOT
                CREATE TRIGGER trg_auth_user_email_domain_insert AFTER INSERT ON auth_user
                FOR EACH ROW
                BEGIN
                    UPDATE auth_user SET email_domain = SUBSTR(NEW.email, INSTR(NEW.email, '@')) WHERE id = NEW.id;
                END;
                EOT
            );
            $this->addSql(
                <<< EOT
                CREATE TRIGGER trg_auth_user_email_domain_update AFTER UPDATE ON auth_user
                FOR EACH ROW
                BEGIN
                    UPDATE auth_user SET email_domain = SUBSTR(NEW.email, INSTR(NEW.email, '@')) WHERE id = NEW.id AND OLD.email <> NEW.email AND NEW.email IS NOT NULL;
                    UPDATE auth_user SET email_domain = NULL WHERE id = NEW.id AND OLD.email <> NEW.email AND NEW.email IS NULL;
                END;
                EOT
            );
            $this->addSql(
                <<< EOT
                UPDATE auth_user SET full_name = first_name || ' ' || last_name;
                EOT
            );
            $this->addSql(
                <<< EOT
                UPDATE auth_user SET email_username = SUBSTR(email, 0, INSTR(email, '@')) WHERE email IS NOT NULL;
                EOT
            );
            $this->addSql(
                <<< EOT
                UPDATE auth_user SET email_domain = SUBSTR(email, INSTR(email, '@')) WHERE email IS NOT NULL;
                EOT
            );
        } else {
            $sql = <<< EOT
            CREATE TRIGGER trg_auth_user_full_name_insert BEFORE INSERT ON auth_user
            FOR EACH ROW
            BEGIN
                SET NEW.full_name = CONCAT(NEW.first_name, ' ', NEW.last_name);
            END;
            CREATE TRIGGER trg_auth_user_full_name_update BEFORE UPDATE ON auth_user
            FOR EACH ROW
            BEGIN
                IF OLD.first_name <> NEW.first_name OR OLD.last_name <> NEW.last_name THEN
                    SET NEW.full_name = CONCAT(NEW.first_name, ' ', NEW.last_name);
                END IF;
            END;
            
            CREATE TRIGGER trg_auth_user_email_username_insert BEFORE INSERT ON auth_user
            FOR EACH ROW
            BEGIN
                IF NEW.email IS NOT NULL THEN
                    SET NEW.email_username = SUBSTRING_INDEX(NEW.email, '@', 1);
                ELSE
                    SET NEW.email_username = NULL;
                END IF;    
            END;
            CREATE TRIGGER trg_auth_user_email_username_update BEFORE UPDATE ON auth_user
            FOR EACH ROW
            BEGIN
                IF OLD.email <> NEW.email THEN
                    IF NEW.email IS NOT NULL THEN
                        SET NEW.email_username = SUBSTRING_INDEX(NEW.email, '@', 1);
                    ELSE
                        SET NEW.email_username = NULL;
                    END IF;
                END IF;
            END;
            
            CREATE TRIGGER trg_auth_user_email_domain_insert BEFORE INSERT ON auth_user
            FOR EACH ROW
            BEGIN
                IF NEW.email IS NOT NULL THEN
                    SET NEW.email_domain = SUBSTRING_INDEX(NEW.email, '@', -1);
                ELSE
                    SET NEW.email_domain = NULL;
                END IF;
            END;
            CREATE TRIGGER trg_auth_user_email_domain_update BEFORE UPDATE ON auth_user
            FOR EACH ROW
            BEGIN
                IF OLD.email <> NEW.email THEN
                    IF NEW.email IS NOT NULL THEN
                        SET NEW.email_domain = SUBSTRING_INDEX(NEW.email, '@', -1);
                    ELSE
                        SET NEW.email_domain = NULL;
                    END IF;
                END IF;
            END;
            EOT;
            $this->addSql($sql);
            $sqlUpdate = <<< EOT
            UPDATE auth_user SET full_name = CONCAT(first_name, ' ', last_name);
            UPDATE auth_user SET email_username = SUBSTRING_INDEX(email, '@', 1) WHERE email IS NOT NULL;
            UPDATE auth_user SET email_domain = SUBSTRING_INDEX(email, '@', -1) WHERE email IS NOT NULL;
            EOT;
            $this->addSql($sqlUpdate);
        }
    }

    public function down(Schema $schema): void
    {
        $sql = <<< EOT
        DROP TRIGGER IF EXISTS trg_auth_user_email_domain_insert;
        DROP TRIGGER IF EXISTS trg_auth_user_email_domain_update;
        
        DROP TRIGGER IF EXISTS trg_auth_user_email_username_insert;
        DROP TRIGGER IF EXISTS trg_auth_user_email_username_update;
        
        DROP TRIGGER IF EXISTS trg_auth_user_full_name_insert;
        DROP TRIGGER IF EXISTS trg_auth_user_full_name_update;
        EOT;
        $this->addSql($sql);
    }
}
