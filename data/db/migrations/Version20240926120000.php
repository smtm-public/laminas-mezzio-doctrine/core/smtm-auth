<?php

declare(strict_types=1);

namespace Smtm\Auth\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20240926120000 extends AbstractMigration
{

    use CommonMigrationTrait;

    public function up(Schema $schema): void
    {
        $authUserTable = $schema->getTable('auth_user');

        if (!$authUserTable->hasColumn('is_system')) {
            $authUserTable->addColumn('is_system', Types::BOOLEAN, ['notNull' => false]);
        }
    }

    public function down(Schema $schema): void
    {

    }
}
