<?php

declare(strict_types=1);

namespace Smtm\Auth\Migration;

use Smtm\Auth\Context\User\Domain\User;
use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Smtm\Base\Infrastructure\Helper\SqlHelper;
use Doctrine\DBAL\Driver\PDO\SQLite\Driver as SQLiteDriver;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120000 extends AbstractMigration
{

    use CommonMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createAuthClientTable($schema);
        $this->createAuthTitleTable($schema);
        $this->createAuthUserTable($schema);
        $this->createAuthAuthFailureTable($schema);
        $this->createAuthClientRedirectUriTable($schema);
        $this->createAuthUserClientTable($schema);
        $this->createAuthRoleTable($schema);
        $this->createAuthUserClientRoleTable($schema);
        $this->createAuthPermissionTable($schema);
        $this->createAuthRoleCodePermissionTable($schema);
    }

    public function createAuthClientTable(Schema $schema): void
    {
        $authClientTable = $this->createTableAndPrimaryKey($schema, 'auth_client');
        $this->addUuidStringColumnAndUniqueIndex($authClientTable);
        $authClientTable->addColumn('r_name', Types::STRING, ['length' => 255, 'notNull' => true]);
        $authClientTable->addColumn('client_id', Types::STRING, ['length' => 32, 'notNull' => true]);
        $authClientTable->addUniqueIndex(
            ['client_id'],
            'idx_unq_' . $authClientTable->getName() . '_client_id'
        );
        $authClientTable->addColumn('client_secret', Types::STRING, ['length' => 255, 'notNull' => true]);
        $authClientTable->addColumn('return_uri_error', Types::STRING, ['length' => 255, 'notNull' => true]);
        $this->addCreatedDatetimeImmutableColumnAndIndex($authClientTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authClientTable);
        $this->addNotArchivedSmallintColumnAndIndex($authClientTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authClientTable);
        $authClientTable->addUniqueIndex(
            ['r_name', 'not_archived'],
            substr(
                'idx_unq_' . $authClientTable->getName() . '_r_name_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $authClientTable->addUniqueIndex(
            ['client_id', 'not_archived'],
            substr(
                'idx_unq_' . $authClientTable->getName() . '_client_id_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
    }

    public function createAuthTitleTable(schema $schema): void
    {
        $authTitleTable = $schema->createTable('auth_title');
        $authTitleTable->addColumn('id', Types::INTEGER)
            ->setAutoincrement(true);
        $authTitleTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($authTitleTable);
        $authTitleTable->addColumn('r_name', Types::STRING, ['length' => 255, 'notNull' => true]);
        $authTitleTable->addColumn(
            'r_order',
            Types::INTEGER,
            ['notNull' => true]
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($authTitleTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authTitleTable);
        $this->addNotArchivedSmallintColumnAndIndex($authTitleTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authTitleTable);
        $authTitleTable->addUniqueIndex(
            ['r_name', 'not_archived'],
            substr(
                'idx_unq_' . $authTitleTable->getName() . '_r_name_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
    }

    public function createAuthUserTable(Schema $schema): void
    {
        $authUserTable = $this->createTableAndPrimaryKey($schema, 'auth_user');
        $this->addUuidStringColumnAndUniqueIndex($authUserTable);
        $authUserTable->addColumn('remote_uuid', Types::STRING, ['notNull' => false, 'length' => 36]);
        $authUserTable->addColumn('username', Types::STRING, ['notNull' => false]);
        $authUserTable->addColumn('password', Types::STRING, ['notNull' => false]);
        $authUserTable->addColumn('email', Types::STRING, ['notNull' => false]);
        $authUserTable->addColumn('email_username', Types::STRING, ['notNull' => false]);
        $authUserTable->addIndex(
            ['email_username'],
            'idx_' . $authUserTable->getName() . '_email_username'
        );
        $authUserTable->addColumn('email_domain', Types::STRING, ['notNull' => false]);
        $authUserTable->addIndex(
            ['email_domain'],
            'idx_' . $authUserTable->getName() . '_email_domain'
        );
        $authUserTable->addColumn(
            'auth_title_id',
            Types::INTEGER,
            ['notNull' => false]
        );
        $authUserTable->addForeignKeyConstraint(
            'auth_title',
            ['auth_title_id'],
            ['id'],
            [],
            'fk_' . $authUserTable->getName() . '_auth_title_id'
        );
        $authUserTable->addIndex(
            ['auth_title_id'],
            'idx_' . $authUserTable->getName() . '_auth_title_id'
        );
        $authUserTable->addColumn('first_name', Types::STRING, ['notNull' => true]);
        $authUserTable->addColumn('last_name', Types::STRING, ['notNull' => true]);
        $authUserTable->addIndex(
            ['first_name', 'last_name'],
            'idx_' . $authUserTable->getName() . '_first_name_last_name'
        );

        if ($this->connection->getDriver() instanceof SQLiteDriver) {
            $authUserTable->addColumn('full_name', Types::STRING, ['notNull' => false, 'default' => '']);
        } else {
            $authUserTable->addColumn('full_name', Types::STRING, ['notNull' => true]);
        }

        $authUserTable->addIndex(
            ['full_name'],
            'idx_' . $authUserTable->getName() . '_full_name'
        );
        $authUserTable->addColumn('gender_iso5218', Types::SMALLINT, ['notNull' => true]);
        $authUserTable->addColumn(
            'i18n_territorial_division_code_iso3166_alpha2',
            Types::STRING,
            ['length' => 2, 'notNull' => false]
        );
        $authUserTable->addForeignKeyConstraint(
            'i18n_territorial_division',
            ['i18n_territorial_division_code_iso3166_alpha2'],
            ['code_iso3166_alpha2'],
            [],
            'fk_' . $authUserTable->getName() . '_i18n_td_code_iso3166_alpha2'
        );
        $authUserTable->addIndex(
            ['i18n_territorial_division_code_iso3166_alpha2'],
            substr(
                'idx_' . $authUserTable->getName() . '_i18n_territorial_division_code_iso3166_alpha2',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
        $authUserTable->addColumn('locale', Types::STRING, ['length' => 6, 'notNull' => false]);
        $authUserTable->addColumn('expires', Types::DATE_MUTABLE, ['notNull' => false]);
        $authUserTable->addColumn('registration_code', Types::TEXT, ['notNull' => false]);
        $authUserTable->addColumn('reset_password_code', Types::TEXT, ['notNull' => false]);
        $authUserTable->addColumn('initialized', Types::BOOLEAN, ['notNull' => true]);
        $authUserTable->addColumn('blocked', Types::BOOLEAN, ['notNull' => true]);
        $authUserTable->addColumn('valid_until', Types::DATETIME_MUTABLE, ['notNull' => false]);
        $authUserTable->addIndex(['valid_until'], 'idx_' . $authUserTable->getName() . '_valid_until');
        $authUserTable->addColumn('is_system', Types::BOOLEAN, ['notNull' => true]);
        $authUserTable->addColumn(
            'r_status',
            Types::SMALLINT,
            ['notNull' => true, 'default' => User::STATUS_ACTIVE]
        );
        $authUserTable->addColumn('last_auth_provider_uuid', Types::STRING, ['notNull' => false, 'length' => 36]);
        $authUserTable->addColumn('last_login_time', Types::DATETIME_MUTABLE, ['notNull' => false]);
        $this->addCreatedByIntColumnAndIndexAndForeignKey($authUserTable, $authUserTable);
        $this->addModifiedByIntColumnAndIndexAndForeignKey($authUserTable, $authUserTable);
        $this->addCreatedDatetimeImmutableColumnAndIndex($authUserTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authUserTable);
        $this->addNotArchivedSmallintColumnAndIndex($authUserTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authUserTable);
        $authUserTable->addUniqueIndex(
            ['remote_uuid', 'not_archived'],
            substr(
                'idx_unq_' . $authUserTable->getName() . '_remote_uuid_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_INDEX
            )
        );
        $authUserTable->addUniqueIndex(
            ['username', 'not_archived'],
            substr(
                'idx_unq_' . $authUserTable->getName() . '_username_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $authUserTable->addUniqueIndex(
            ['email', 'not_archived'],
            substr(
                'idx_unq_' . $authUserTable->getName() . '_email_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
        $authUserTable->addIndex(
            ['first_name', 'last_name', 'not_archived'],
            'idx_' . $authUserTable->getName() . '_first_name_last_name_not_archived'
        );
        $authUserTable->addIndex(
            ['full_name', 'not_archived'],
            'idx_' . $authUserTable->getName() . '_full_name_not_archived'
        );
    }

    public function createAuthAuthFailureTable(Schema $schema): void
    {
        $authAuthFailureTable = $this->createTableAndPrimaryKey($schema, 'auth_auth_failure');
        $authAuthFailureTable->addColumn('auth_user_id', Types::INTEGER, ['notNull' => true]);
        $authAuthFailureTable->addColumn('created', Types::DATETIME_IMMUTABLE, ['notNull' => true]);
        $authAuthFailureTable->addIndex(['created'], 'idx_' . $authAuthFailureTable->getName() . '_created');
        $authAuthFailureTable->addForeignKeyConstraint(
            'auth_user',
            ['auth_user_id'],
            ['id'],
            [],
            'fk_' . $authAuthFailureTable->getName() . '_user'
        );
    }

    public function createAuthClientRedirectUriTable(Schema $schema): void
    {
        $authClientRedirectUriTable = $schema->createTable('auth_client_redirect_uri');
        $authClientRedirectUriTable->addColumn(
            'auth_client_id',
            Types::INTEGER,
            ['notNull' => true]
        );
        $authClientRedirectUriTable->addIndex(['auth_client_id'], 'idx_' . $authClientRedirectUriTable->getName() . '_id');
        $authClientRedirectUriTable->addForeignKeyConstraint(
            'auth_client',
            ['auth_client_id'],
            ['id'],
            [],
            'fk_' . $authClientRedirectUriTable->getName() . '_auth_client_id'
        );
        $authClientRedirectUriTable->addColumn(
            'redirect_uri',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $authClientRedirectUriTable->setPrimaryKey(['auth_client_id', 'redirect_uri']);
        $this->addCreatedDatetimeImmutableColumnAndIndex($authClientRedirectUriTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authClientRedirectUriTable);
        $this->addNotArchivedSmallintColumnAndIndex($authClientRedirectUriTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authClientRedirectUriTable);
        $authClientRedirectUriTable->addUniqueIndex(
            ['auth_client_id', 'redirect_uri', 'not_archived'],
            substr(
                'idx_unq_' . $authClientRedirectUriTable->getName() . '_auth_client_id_redirect_uri_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
    }

    public function createAuthUserClientTable(Schema $schema): void
    {
        $authUserClientTable = $this->createTableAndPrimaryKey($schema, 'auth_user_client');
        $this->addUuidStringColumnAndUniqueIndex($authUserClientTable);
        $authUserClientTable->addColumn('auth_user_id', Types::INTEGER, ['notNull' => true]);
        $authUserClientTable->addColumn('auth_client_id', Types::INTEGER, ['notNull' => true]);
        $authUserClientTable->addForeignKeyConstraint(
            $schema->getTable('auth_user'),
            ['auth_user_id'],
            ['id'],
            [],
            'fk_' . $authUserClientTable->getName() . '_auth_user_id'
        );
        $authUserClientTable->addIndex(
            ['auth_client_id'],
            'idx_' . $authUserClientTable->getName() . '_auth_user_id'
        );
        $authUserClientTable->addForeignKeyConstraint(
            $schema->getTable('auth_client'),
            ['auth_client_id'],
            ['id'],
            [],
            'fk_' . $authUserClientTable->getName() . '_auth_client_id'
        );
        $authUserClientTable->addIndex(
            ['auth_client_id'],
            'idx_' . $authUserClientTable->getName() . '_auth_client_id'
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($authUserClientTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authUserClientTable);
        $this->addNotArchivedSmallintColumnAndIndex($authUserClientTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authUserClientTable);
        $authUserClientTable->addUniqueIndex(
            ['auth_user_id', 'auth_client_id', 'not_archived'],
            substr(
                'idx_unq_' . $authUserClientTable->getName() . '_auth_user_id_auth_client_id_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
    }

    public function createAuthRoleTable(Schema $schema): void
    {
        $authRoleTable = $this->createTableAndPrimaryKey($schema, 'auth_role');
        $this->addUuidStringColumnAndUniqueIndex($authRoleTable);
        $authRoleTable->addColumn('code', Types::STRING, ['length' => 32, 'notNull' => true]);
        $authRoleTable->addColumn('description', Types::TEXT, ['notNull' => false]);
        $this->addCreatedDatetimeImmutableColumnAndIndex($authRoleTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authRoleTable);
        $this->addNotArchivedSmallintColumnAndIndex($authRoleTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authRoleTable);
        $authRoleTable->addUniqueIndex(
            ['code', 'not_archived'],
            'idx_unq_' . $authRoleTable->getName() . '_code_not_archived'
        );
    }

    public function createAuthUserClientRoleTable(Schema $schema): void
    {
        $authUserClientRoleTable = $this->createTableAndPrimaryKey($schema, 'auth_user_client_role');
        $this->addUuidStringColumnAndUniqueIndex($authUserClientRoleTable);
        $authUserClientRoleTable->addColumn('auth_user_client_id', Types::INTEGER, ['notNull' => true]);
        $authUserClientRoleTable->addForeignKeyConstraint(
            $schema->getTable('auth_user_client'),
            ['auth_user_client_id'],
            ['id'],
            [],
            'fk_' . $authUserClientRoleTable->getName() . '_auth_user_client_id'
        );
        $authUserClientRoleTable->addIndex(
            ['auth_user_client_id'],
            'idx_' . $authUserClientRoleTable->getName() . '_auth_user_client_id'
        );
        $authUserClientRoleTable->addColumn('auth_role_id', Types::INTEGER, ['notNull' => true]);
        $authUserClientRoleTable->addForeignKeyConstraint(
            $schema->getTable('auth_role'),
            ['auth_role_id'],
            ['id'],
            [],
            'fk_' . $authUserClientRoleTable->getName() . '_auth_role_id'
        );
        $authUserClientRoleTable->addIndex(
            ['auth_role_id'],
            'idx_' . $authUserClientRoleTable->getName() . '_auth_role_id'
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($authUserClientRoleTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authUserClientRoleTable);
        $this->addNotArchivedSmallintColumnAndIndex($authUserClientRoleTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authUserClientRoleTable);
        $authUserClientRoleTable->addUniqueIndex(
            ['auth_user_client_id', 'auth_role_id', 'not_archived'],
            substr(
                'idx_unq_' . $authUserClientRoleTable->getName() . '_auth_user_client_id_auth_role_id_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
    }

    public function createAuthTokenTable(Schema $schema): void
    {
        $authTokenTable = $this->createTableAndPrimaryKey($schema, 'auth_token');
        $this->addUuidStringColumnAndUniqueIndex($authTokenTable);
        $authTokenTable->addColumn('auth_user_client_id', Types::INTEGER, ['notNull' => true]);
        $authTokenTable->addForeignKeyConstraint(
            $schema->getTable('auth_user_client'),
            ['auth_user_client_id'],
            ['id'],
            [],
            'fk_' . $authTokenTable->getName() . '_auth_user_client_id'
        );
        $authTokenTable->addIndex(['auth_user_client_id'], 'idx_' . $authTokenTable->getName() . '_auth_user_client_id');
        $authTokenTable->addColumn('provider_uuid', Types::STRING, ['length' => 36, 'notNull' => true]);
        $authTokenTable->addColumn('access_token', Types::TEXT, ['notNull' => true]);
        $authTokenTable->addColumn('refresh_token', Types::TEXT, ['notNull' => true]);
        $authTokenTable->addColumn('expires', Types::DATETIME_IMMUTABLE, ['notNull' => true]);
        $authTokenTable->addColumn('used', Types::SMALLINT, ['default' => 0, 'notNull' => true]);
        $authTokenTable->addIndex(['used'], 'idx_' . $authTokenTable->getName() . '_used');
        $authTokenTable->addColumn('revoked', Types::SMALLINT, ['default' => 0, 'notNull' => true]);
        $authTokenTable->addIndex(['revoked'], 'idx_' . $authTokenTable->getName() . '_revoked');
        $authTokenTable->addColumn(
            'refresh_token_expires',
            Types::DATETIME_IMMUTABLE,
            ['notNull' => true]
        );
        $authTokenTable->addColumn('id_token', Types::TEXT, ['notNull' => false]);
        $authTokenTable->addColumn('token_type', Types::STRING, ['length' => 32, 'notNull' => false]);
        $authTokenTable->addColumn('r_scope', Types::STRING, ['length' => 255, 'notNull' => false]);
        $authTokenTable->addIndex(
            ['auth_user_client_id', 'used', 'revoked'],
            'idx_' . $authTokenTable->getName() . '_auth_user_client_id_used_revoked'
        );
        $this->addCreatedDatetimeImmutableColumnAndIndex($authTokenTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authTokenTable);
        $this->addNotArchivedSmallintColumnAndIndex($authTokenTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authTokenTable);
    }

    public function createAuthPermissionTable(Schema $schema): void
    {
        $authPermissionTable = $schema->createTable('auth_permission');
        $authPermissionTable->addColumn(
            'id',
            Types::INTEGER,
            ['notNull' => true, 'autoincrement' => true]
        );
        $authPermissionTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($authPermissionTable);
        $authPermissionTable->addColumn('r_name', Types::STRING, ['notNull' => true]);
        $authPermissionTable->addColumn('description', Types::TEXT, ['notNull' => false]);
        $this->addCreatedDatetimeImmutableColumnAndIndex($authPermissionTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($authPermissionTable);
        $this->addNotArchivedSmallintColumnAndIndex($authPermissionTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($authPermissionTable);
        $authPermissionTable->addUniqueIndex(
            ['r_name', 'not_archived'],
            substr(
                'idx_unq_' . $authPermissionTable->getName() . '_r_name_not_archived',
                0,
                SqlHelper::IDENTIFIER_LENGTH_CONSTRAINT
            )
        );
    }

    public function createAuthRoleCodePermissionTable(Schema $schema): void
    {
        $authRoleCodePermissionTable = $schema->createTable('auth_role_code_permission');
        $authRoleCodePermissionTable->addColumn(
            'auth_role_code',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $authRoleCodePermissionTable->addIndex(
            ['auth_role_code'],
            'idx_' . $authRoleCodePermissionTable->getName() . '_auth_role_code'
        );
        $authRoleCodePermissionTable->addForeignKeyConstraint(
            'auth_role',
            ['auth_role_code'],
            ['code'],
            [],
            'fk_' . $authRoleCodePermissionTable->getName() . '_auth_role_code'
        );
        $authRoleCodePermissionTable->addColumn('auth_permission_id', Types::INTEGER, ['notNull' => true]);
        $authRoleCodePermissionTable->addIndex(
            ['auth_permission_id'],
            'idx_' . $authRoleCodePermissionTable->getName() . '_auth_permission_id'
        );
        $authRoleCodePermissionTable->addForeignKeyConstraint(
            'auth_permission',
            ['auth_permission_id'],
            ['id'],
            [],
            'fk_' . $authRoleCodePermissionTable->getName() . '_auth_permission_id'
        );
        $authRoleCodePermissionTable->setPrimaryKey(
            ['auth_role_code', 'auth_permission_id']
        );
        $authRoleCodePermissionTable->addUniqueIndex(
            ['auth_role_code', 'auth_permission_id'],
            'idx_unq_' . $authRoleCodePermissionTable->getName() . '_role_code_permission_id'
        );
    }

    public function down(Schema $schema): void
    {

    }
}
