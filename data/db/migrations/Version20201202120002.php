<?php

declare(strict_types=1);

namespace Smtm\Auth\Migration;

use Smtm\Auth\Infrastructure\Doctrine\Migration\AuthRoleTableAndAuthPermissionTableAndAuthRoleCodePermissionTablePopulatingMigrationTrait;
use Smtm\Base\Infrastructure\Doctrine\Migration\TablePopulatingMigrationTrait;
use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Psr\Log\LoggerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120002 extends AbstractMigration
{

    use AuthRoleTableAndAuthPermissionTableAndAuthRoleCodePermissionTablePopulatingMigrationTrait,
        TablePopulatingMigrationTrait;

    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        parent::__construct($connection, $logger);

        if (file_exists(__DIR__ . '/../../../../../../.env.smtm.smtm-auth.data')) {
            $dotenv = \Dotenv\Dotenv::createMutable(
                __DIR__ . '/../../../../../../', '.env.smtm.smtm-auth.data'
            );
            $dotenv->load();
        }

        $this->role = [
            [
                'id' => 1,
                'uuid' => '1eece020-713e-4eff-9102-1c9ccf884b96',
                'code' => 'user',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            [
                'id' => 2,
                'uuid' => '64479bfc-c7ba-499e-b9c6-70039361fcc9',
                'code' => 'admin',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
        ];
        $this->permission = [
            'smtm.auth.title.create' => [
                'uuid' => '032c52dc-4d3f-403e-8ba7-5be95f30a2b5',
                'r_name' => 'smtm.auth.title.create',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.title.read' => [
                'uuid' => 'd8272ddf-5138-4952-bab2-f92957f06cce',
                'r_name' => 'smtm.auth.title.read',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.title.put' => [
                'uuid' => '3bca0dac-5596-4f79-b084-5a39770ae703',
                'r_name' => 'smtm.auth.title.put',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.title.patch' => [
                'uuid' => 'df6c0135-d214-4e2b-880e-559042425af8',
                'r_name' => 'smtm.auth.title.patch',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.title.delete' => [
                'uuid' => '0b7e4311-20ce-459f-b919-da4d41616a6c',
                'r_name' => 'smtm.auth.title.delete',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.title.index' => [
                'uuid' => '36e26966-d791-431c-a562-b406883d572e',
                'r_name' => 'smtm.auth.title.index',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],

            'smtm.auth.role.create' => [
                'uuid' => '299b63bd-4311-4743-a141-8928a481ae98',
                'r_name' => 'smtm.auth.role.create',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.role.read' => [
                'uuid' => 'a9872f45-3efa-414f-94c2-97fb0eb41f87',
                'r_name' => 'smtm.auth.role.read',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.role.put' => [
                'uuid' => 'ad2e51a0-0419-4c8e-83ab-45400d5a1605',
                'r_name' => 'smtm.auth.role.put',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.role.patch' => [
                'uuid' => '5d55c6b0-1200-4741-8f18-6ec7767afc1b',
                'r_name' => 'smtm.auth.role.patch',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.role.delete' => [
                'uuid' => 'a78ee450-605f-498d-b6f8-617f70a9a9eb',
                'r_name' => 'smtm.auth.role.delete',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.role.index' => [
                'uuid' => 'd420308c-c96c-4e6a-9ad0-bcea2d402059',
                'r_name' => 'smtm.auth.role.index',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],

            'smtm.auth.permission.create' => [
                'uuid' => 'cad14bd6-65fb-415b-827f-e1dd9c45b63d',
                'r_name' => 'smtm.auth.permission.create',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.permission.read' => [
                'uuid' => '43e1e502-0ec9-4f74-90dc-6b0cbfe3d39e',
                'r_name' => 'smtm.auth.permission.read',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.permission.put' => [
                'uuid' => '77d2af09-4c20-48c0-a089-8958c3ddb856',
                'r_name' => 'smtm.auth.permission.put',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.permission.patch' => [
                'uuid' => 'e7990714-ea5a-4fa3-9554-04b9a47e6cde',
                'r_name' => 'smtm.auth.permission.patch',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.permission.delete' => [
                'uuid' => '15bddca9-9110-4ec4-999c-e9aa5b52ce1a',
                'r_name' => 'smtm.auth.permission.delete',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.permission.index' => [
                'uuid' => 'd8dc7d46-1968-4b39-a369-96b04be8afc9',
                'r_name' => 'smtm.auth.permission.index',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],

            'smtm.auth.user.create' => [
                'uuid' => '60a10438-0292-4b7e-894f-831d9d727da4',
                'r_name' => 'smtm.auth.user.create',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.user.read' => [
                'uuid' => 'a919bf14-2940-4e0f-89af-928da3cad683',
                'r_name' => 'smtm.auth.user.read',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.user.put' => [
                'uuid' => '00bc67ef-6da8-43a4-bb0f-b9d0c4b29400',
                'r_name' => 'smtm.auth.user.put',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.user.patch' => [
                'uuid' => 'a91c9cd6-1bf9-49a4-857d-9ae4388ceb25',
                'r_name' => 'smtm.auth.user.patch',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.user.delete' => [
                'uuid' => '6eea51bb-7ccc-4571-a0a6-fb03fc9dbae9',
                'r_name' => 'smtm.auth.user.delete',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.user.index' => [
                'uuid' => '6839c7fe-149f-48f7-83d3-1cbda7c4174a',
                'r_name' => 'smtm.auth.user.index',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],

            'smtm.auth.authenticated-user-client.read' => [
                'uuid' => 'daec7b5f-58b9-4f24-bde1-426fe8f4e15a',
                'r_name' => 'smtm.auth.authenticated-user-client.read',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],

            'smtm.auth.client.index' => [
                'uuid' => '28a54bbb-3396-439b-9546-fe3fb4c24008',
                'r_name' => 'smtm.auth.client.index',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
            'smtm.auth.client.read' => [
                'uuid' => 'cce936d5-0ad9-42c9-923f-4f144cb7ad8a',
                'r_name' => 'smtm.auth.client.read',
                'created' => date('Y-m-d H:i:s'),
                'not_archived' => 1,
            ],
        ];
        $this->rolePermission = [
            'user' => [
                'smtm.auth.role.read',
                'smtm.auth.role.index',
                'smtm.auth.user.read',
                'smtm.auth.user.put',
                'smtm.auth.user.patch',
                'smtm.auth.authenticated-user-client.read',
                'smtm.auth.client.read',
                'smtm.auth.client.index',
            ],
            'admin' => [
                'smtm.auth.title.create',
                'smtm.auth.title.read',
                'smtm.auth.title.put',
                'smtm.auth.title.patch',
                'smtm.auth.title.delete',
                'smtm.auth.title.index',

                'smtm.auth.role.create',
                'smtm.auth.role.read',
                'smtm.auth.role.put',
                'smtm.auth.role.patch',
                'smtm.auth.role.delete',
                'smtm.auth.role.index',

                'smtm.auth.permission.create',
                'smtm.auth.permission.read',
                'smtm.auth.permission.put',
                'smtm.auth.permission.patch',
                'smtm.auth.permission.delete',
                'smtm.auth.permission.index',

                'smtm.auth.user.create',
                'smtm.auth.user.read',
                'smtm.auth.user.put',
                'smtm.auth.user.patch',
                'smtm.auth.user.delete',
                'smtm.auth.user.index',

                'smtm.auth.authenticated-user-client.read',

                'smtm.auth.client.index',
                'smtm.auth.client.read',
            ],
        ];

        $this->models = [
            'auth_client' => [
                [
                    'id' => 1,
                    'uuid' => '233d9e15-3b78-4acb-b003-54b922aeae55',
                    'r_name' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CLIENT_INITIAL_NAME'),
                    'client_id' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CLIENT_INITIAL_CLIENT_ID'),
                    'client_secret' =>
                        password_hash(
                            EnvHelper::getEnvFromProcessOrSuperGlobal(
                                'SMTM_AUTH_CLIENT_INITIAL_CLIENT_SECRET'
                            ),
                            PASSWORD_DEFAULT
                        ),
                    'return_uri_error' =>
                        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_CLIENT_INITIAL_RETURN_URI_ERROR'),
                    'created' => date('Y-m-d H:i:s'),
                    'not_archived' => 1,
                ],
            ],

            'auth_user' => [
                [
                    'id' => 1,
                    'uuid' => '6eb13b22-7032-4047-955d-2c1c45ecbe58',
                    'username' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_USER_INITIAL_USERNAME'),
                    'password' =>
                        password_hash(
                            EnvHelper::getEnvFromProcessOrSuperGlobal(
                                'SMTM_AUTH_USER_SYSTEM_PASSWORD'
                            ),
                            PASSWORD_DEFAULT
                        ),
                    'email' =>
                        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_USER_INITIAL_EMAIL'),
                    'first_name' =>
                        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_USER_INITIAL_FIRST_NAME'),
                    'last_name' =>
                        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_USER_INITIAL_LAST_NAME'),
                    'gender_iso5218' => 3,
                    'initialized' => 1,
                    'blocked' => 0,
                    'is_system' => 1,
                    'created_by' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'not_archived' => 1,
                ],
            ],

            'auth_user_client' => [
                [
                    'id' => 1,
                    'uuid' => 'e30b7bff-ada7-4118-bdf5-2d00a40f39af',
                    'auth_user_id' => 1,
                    'auth_client_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'not_archived' => 1,
                ],
            ],

            'auth_user_client_role' => [
                [
                    'id' => 1,
                    'uuid' => 'b9e282cc-867f-482f-b5b8-7abe172960ec',
                    'auth_user_client_id' => 1,
                    'auth_role_id' => 2, // admin
                    'created' => date('Y-m-d H:i:s'),
                    'not_archived' => 1,
                ],
            ],

            'email_message_subject_template' => [
                [
                    'uuid' => 'f6131d93-015f-4a95-a07a-e9659d7990bd',
                    'r_name' => 'SMTM_AUTH_USER_CREATED',
                    'r_template' => 'Thank you for registering at Smtm!',
                ],
                [
                    'uuid' => 'ca65eae3-9c6f-4186-a869-a85c560f2a8e',
                    'r_name' => 'SMTM_AUTH_PASSWORD_RESET',
                    'r_template' => 'Smtm - Password reset request',
                ],
            ],
            'email_message_content_template' => [
                [
                    'uuid' => '70a70ebd-bea7-40b1-8dd3-721afa86605f',
                    'r_name' => 'SMTM_AUTH_USER_CREATED',
                    'content_type' => 'text/html',
                    'file_template' => 'smtm-auth-email::user-created-html',
                ],
                [
                    'uuid' => '0930adcd-5981-4723-a277-51cd5bd2fb88',
                    'r_name' => 'SMTM_AUTH_USER_CREATED',
                    'content_type' => 'text/plain',
                    'file_template' => 'smtm-auth-email::user-created-text',
                ],
                [
                    'uuid' => 'f88975e2-74a1-4a22-b528-21e2362a6757',
                    'r_name' => 'SMTM_AUTH_PASSWORD_RESET',
                    'content_type' => 'text/html',
                    'file_template' => 'smtm-auth-email::password-reset-html',
                ],
                [
                    'uuid' => 'aa36492d-815e-49b3-93e9-e00db7aeb1c7',
                    'r_name' => 'SMTM_AUTH_PASSWORD_RESET',
                    'content_type' => 'text/plain',
                    'file_template' => 'smtm-auth-email::password-reset-text',
                ],
            ],
        ];
    }

    public function up(Schema $schema): void
    {
        $this->populateAuthRoleTable($this->connection, $schema);
        $this->populateAuthPermissionTable($this->connection, $schema);
        $this->populateAuthRoleCodePermissionTable($this->connection, $schema);

        $this->populateTables($this->connection, $schema);
    }

    public function down(Schema $schema): void
    {

    }
}
