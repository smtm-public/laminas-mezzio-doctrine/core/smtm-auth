<?php

declare(strict_types=1);

namespace Smtm\Auth\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Smtm\Base\Infrastructure\Helper\EnvHelper;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20240926120001 extends AbstractMigration
{

    use CommonMigrationTrait;

    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        parent::__construct($connection, $logger);

        if (file_exists(__DIR__ . '/../../../../../../.env.smtm.smtm-auth')) {
            $dotenv = \Dotenv\Dotenv::createMutable(
                __DIR__ . '/../../../../../../', '.env.smtm.smtm-auth'
            );
            $dotenv->load();
        }
    }

    public function up(Schema $schema): void
    {
        $authUserTable = $schema->getTable('auth_user');

        if ($authUserTable->hasColumn('is_system')) {
            $systemUserId = EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_AUTH_DEFAULT_SYSTEM_USER_ID');
            $systemUserId = $this->connection->quote($systemUserId);

            $systemUsers = $this->connection->fetchAllAssociative(<<< EOT
                SELECT * FROM auth_user WHERE id=$systemUserId
                EOT
            );

            foreach ($systemUsers as $systemUser) {
                $this->connection->update(
                    'auth_user',
                    [
                        'is_system' => 1,
                    ],
                    ['id' => $systemUser['id']]
                );
            }

            $nonSystemUsers = $this->connection->fetchAllAssociative(<<< EOT
                SELECT * FROM auth_user WHERE id!=$systemUserId
                EOT
            );

            foreach ($nonSystemUsers as $nonSystemUser) {
                $this->connection->update(
                    'auth_user',
                    [
                        'is_system' => 0,
                    ],
                    ['id' => $nonSystemUser['id']]
                );
            }
        }
    }

    public function down(Schema $schema): void
    {

    }
}
