<?php

declare(strict_types=1);

namespace Smtm\Auth\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20240926120002 extends AbstractMigration
{

    use CommonMigrationTrait;

    public function up(Schema $schema): void
    {
        $authUserTable = $schema->getTable('auth_user');

        if ($authUserTable->hasColumn('is_system')) {
            $authUserTable->modifyColumn('is_system', ['notNull' => true]);
        }
    }

    public function down(Schema $schema): void
    {

    }
}
